var editor; // use a global for the submit and return data rendering
var debug;
var debug_dependencies;
var current_user;
var data_script = "commitments_data.php";
var user_roles;
var use_task = "<Use Task>";

function update_selected(table) {
    var selectedRows = table.rows( { search: 'applied', selected: true } );
    var selected_row_count = selectedRows.count();

    count_selected(table, selected_row_count, 2);
}

function update_buttons(table, current_user, user_roles) {
    var enable_new = has_role_admin(current_user, user_roles);
    var enable_edit = has_role_admin(current_user, user_roles);
    var enable_duplicate = has_role_admin(current_user, user_roles);

    var selectedRows = table.rows( { search: 'applied', selected: true } );
    var selected_row_count = selectedRows.count();

    enable_edit = enable_edit && (selected_row_count > 0);
    enable_duplicate = enable_duplicate && (selected_row_count == 1);
    enable_new = enable_new && !enable_edit;

    if (debug) console.log("(De)Select", selectedRows.count(), "rows -> new enabled", enable_new);
    if (debug) console.log("(De)Select", selectedRows.count(), "rows -> edit enabled", enable_edit);
    if (debug) console.log("(De)Select", selectedRows.count(), "rows -> duplicate enabled", enable_duplicate);

    table.buttons( ['.buttons-create'] ).enable(enable_new);
    table.buttons( ['.buttons-edit', '.buttons-remove'] ).enable(enable_edit);
    table.buttons( ['.x-buttons-duplicate'] ).enable(enable_duplicate);
}

function add_can_edit(data, user, user_roles) {
    current_user = user;

    for (var i = 0; i < data.length; i++) {
        var can_edit_and_because = can_edit(user, user_roles, '', '', ''); // FIXME: json.data[i].SYSTEM_ID, json.data[i].WBS_ID);
        data[i].CAN_EDIT = can_edit_and_because.can_edit;
        data[i].CAN_EDIT_BECAUSE = can_edit_and_because.can_edit_because;
    }
}

$(document).ready(function() {
    var queryString = window.location.search;
    let params = new URLSearchParams(queryString);

    // query string ?test
    let is_test = params.get("test") !== null;

    // query string ?debug
    debug = params.get("debug") !== null;
    let fixed_columns = debug ? 4 : 1;

    // query string ?dependencies
    debug_dependencies = params.get("debug_dependencies") !== null;

    var pathname = window.location.pathname;
    let is_prediction = pathname.endsWith('predictions.php');

    data_script += (is_prediction ? "?predictions" : "");

    if (debug || debug_dependencies) {
        console.log(window.location);
        console.log("test = ", is_test);
        console.log("debug = ", debug);
        console.log("debug_dependencies = ", debug_dependencies);
        console.log("is_prediction = ", is_prediction);
        console.log("data_script = ", data_script);
    }

    editor = new DataTable.Editor({
        ajax: {
            url: data_script,
            type: "POST",
            data: function ( d ) {
                if ((d.action == 'create') || (d.action == 'remove')) {
                    return;
                }

                // NOTE below breaks double editing as 'table' changes
                // related test: test_edit_inline_system_twice
                // Send also initial values before change in 'init_data'
                // [row, value] = Object.entries(d.data)[0];
                // [table, value] = Object.entries(value)[0];
                // [field, value] = Object.entries(value)[0];

                // // FIXME check for row, for multi-submit
                // d.init_data = {}
                // d.init_data[row] = {}
                // d.init_data[row][table] = {}
                // d.init_data[row][table][field] = editor.init_data[table][field]

                if (debug) console.log("Data populated for EDIT (currently disabled)");
                if (debug) console.log(d);
            }
        },
        table: "#table",
        fields: [
            // all Editable fields
            {
                label: "Filter on System:",
                name: "SYSTEM_NODE.ID",
                type: "select",
                def: "",
                placeholder: "Select a System",
                placeholderValue: "",
                placeholderDisabled: false
            }, {
                label: "Filter on Activity:",
                name: "ACTIVITY.ID",
                type: "select",
                def: "",
                placeholder: "Select an Activity",
                placeholderValue: "",
                placeholderDisabled: false
            }, {
                label: "Task:",
                name: "TASK.ID",
                type: "datatable",
                // TBD
                config: {
                    paging:         true,
                    scrollY:        150,
                    scrollCollapse: true,
                    deferRender:    true,
                    scroller:       true,   // NOTE enabled as we only use this part top search
                    columns: [
                        {
                            title: '<b>Task ID</b>: Name',
                            data: 'label'
                        }
                    ],
                    order: 0
                }

            }, {
                label: "Selected Task:",
                name: "TASK",
                type: "display",
                data: function ( row, type, val, meta ) {
                     return `<b>${row.TASK_ID}</b>: ${row.TASK_SHORT_TITLE}`;
                },
            }, {
                label: "Selected:",
                name: "TASK_ID",
                type: "hidden"
            }, {
                label: "Selected:",
                name: "TASK_SHORT_TITLE",
                type: "hidden"
            }, {
                label: "Requirement:",
                name: "COMMITMENTS.RES_REQUIREMENT_ID",
                type: "select",
                def: "",
                placeholder: "Select a Requirement",
                placeholderValue: ""
            }, {
                label: "Funding Agency:",
                name: "FUNDING_AGENCY.ID",
                type: "select",
                def: "",
                placeholder: "Select a Funding Agency",
                placeholderValue: ""
            }, {
                label: "Institute:",
                name: "COMMITMENTS.INSTITUTE_ID",
                type: "select",
                def: "",
                placeholder: "Select an Institute",
                placeholderValue: ""
            }, {
                label: "Year:",
                name: "COMMITMENTS.YEAR",
                type: "select",
                def: "",
                placeholder: "Select a Year",
                placeholderValue: "",
                options: [ "2025", "2024", "2023", "2022", "2021", "2020", "2019", "2018", "2017" ]
            }, {
                label: "Value:",
                name: "COMMITMENTS.VALUE",
                def: "0",
                className: 'text-right',
                // type: "mask",
                // mask: "#0.00",
                // maskOptions: {
                //     reverse: true,
                //     placeholder: ""
                // }
            }, {
                label: "Description:",
                name: "COMMITMENTS.DESCRIPTION",
            }
        ]
    } );

    // send also original data
    editor.on('initEdit', function (_e, _node, data, _items, type) {
        // Called just before editing (inline or normal) is done.
        if (debug) console.log('initEdit', type, data);

        // TBD Store initial data before change (as prepared in ajax.data)
        // to enable to detect (and possibly resolve) conflicts (a la git)
        editor.init_data = data;
    });

    editor.on('preOpen', function(e, mode, action) {
        // Called before a form (inline or normal) is opened. Called after initEdit.
        if (debug) console.log('preOpen', mode, action, e);

        // NOTE: editor.field('CAN_EDIT').val() is not filled at first select, so we use the datatable
        var modifier = editor.modifier();
        var data = table.row( modifier ).data();

        // No edits/deletes if CAN_EDIT field is false
        if (data && (typeof data !== "undefined") && !data.CAN_EDIT) {
            if (debug) console.log("No edits if CAN_EDIT field is false");
            return false;
        }

        // For edit window
        if (mode === 'main') {
            // editor.field('CAN_EDIT').hide();
            // editor.field('COMMITMENTS.YEAR').hide();
            if (action === 'create') {
                editor.field('COMMITMENTS.YEAR').enable();
            } else {
                editor.field('COMMITMENTS.YEAR').disable();
            }
        }

        do_validate = false;

        set_change_handler(editor, 'COMMITMENTS.RES_REQUIREMENT_ID', mode, debug);
        set_change_handler(editor, 'COMMITMENTS.INSTITUTE_ID', mode, debug);

        if (debug) console.log('preOpen Done');
    } );

    // re-align columns
    editor.on('open', function (_e, mode, _action) {
        if (mode === 'inline') {
            table.columns.adjust();

            // Select editable text rather than insert. Click again tio insert.
            $('#table div.DTE input').select();
        }
    });

    var do_validate = false;

    function validate() {
        if (do_validate) {
            if (debug) console.log("validate()");
            // var system_id = editor.field('SYSTEM_NODE.ID');
            // system_id.error(system_id.val() == "" ? "Select a System": "");

            // var activity_id = editor.field('ACTIVITY.ID');
            // activity_id.error(activity_id.val() == "" ? "Select an Activity": "");

            // FIXME re-enable for edit mode, not inline
            // var task_id = editor.field('TASK.ID');
            // task_id.error(task_id.val() == "" ? "Select a Task": "");

            var res_requirement_id = editor.field('COMMITMENTS.RES_REQUIREMENT_ID');
            res_requirement_id.error(res_requirement_id.val() == "" ? "Select a Requirement": "");

            var funding_agency_id = editor.field('FUNDING_AGENCY.ID');
            funding_agency_id.error(funding_agency_id.val() == "" ? "Select a Funding Agency": "");

            var institute_id = editor.field('COMMITMENTS.INSTITUTE_ID');
            institute_id.error(institute_id.val() == "" ? "Select an Institute": "");

            var year = editor.field('COMMITMENTS.YEAR');
            var year_value = parseInt(year.val());
            year.error(isNaN(year_value) || year < 2008 ? "Select a Year": "");

            var value = editor.field('COMMITMENTS.VALUE');
            var value_value = parseFloat(value.val());
            value.error(isNaN(value_value) ? "Value must be a number" : value_value < 0 ? "Value must be larger or equal to 0": "");

            if (debug) {
                // console.log("validate() task_id: ", task_id.val());
                // console.log("validate() task_id: ", editor.field('TASK_ID').val());
                console.log("validate() res_requirement_id: ", res_requirement_id.val());
                console.log("validate() funding_agency_id: ", funding_agency_id.val());
                console.log("validate() institute_id: ", institute_id.val());
                console.log("validate() year_value: ", year_value);
                console.log("validate() value_value: ", value_value);
            }
        }
    }

    editor.on('preSubmit', function(e, data, action) {
        if (debug) console.log('preSubmit', e, action, data);

        do_validate = true;
        // Validate fields before submitting to server

        // DELETE is ok
        if (action == "remove") {
            if (debug) console.log("preSubmit DELETE is ok");
            return true;
        }

        validate();

        // Any missing fields, abort submit
        if ( this.inError() ) {
            if (debug) console.log("preSubmit Some error prevented submit");
            return false;
        }

        // 'Use Task' case, set reqID to TaskID
        console.log(data);
        if (action == 'create') {
            if (data.data[0].COMMITMENTS.RES_REQUIREMENT_ID == 0) {
                data.data[0].COMMITMENTS.RES_REQUIREMENT_ID = data.data[0].TASK.ID;
            }
        } else if (action == 'edit') {
            var row_id = e.target.init_data.DT_RowId;
            console.log(row_id);
            if (data.data[row_id].COMMITMENTS.RES_REQUIREMENT_ID == 0) {
                data.data[row_id].COMMITMENTS.RES_REQUIREMENT_ID = e.target.init_data.TASK.ID;
            }
        }

        if (debug) console.log('preSubmit Done', action, data);

        return true;
    });

    // handle server errors
    editor.on('postSubmit', function (_e, json, data, action, xhr) {
        if (debug) console.log("postSubmit", action, json, data, xhr);

        if (json === null) {
            console.log(data);
            console.log('responseText:', xhr.responseText);
            alert("Server error in script: " + data_script + ", look at console log.");
        }

        update_roles(json.data, current_user, user_roles, table);

        if ((action == 'create') || (action == 'edit')) {
            if (json.data[0].COMMITMENTS.RES_REQUIREMENT_ID == json.data[0].TASK.ID) {
                json.data[0].COMMITMENTS.RES_REQUIREMENT_ID = 0;
            }
        }

        //   if ( json.FieldErrors ) {
        //      .... take a look at json.fieldErrors
        //   }

        // console.log(json.data[0].COMMITMENTS.ID)

        // table.page.jumpToData( 22676, 3 );

        if (debug) console.log("postSubmit Done");
    });

    // jump to new or edited record
    editor.on( 'submitComplete', function ( e, json, data, action ) {
        console.log("submitComplete", data, action);
        if (( action === 'create' || action === 'edit' ) && (data)) {
            if (is_test) {
                $(`#${data.DT_RowId}`).addClass("test_commitments_record");
            }
            table.row(`#${data.DT_RowId}`).scrollTo();
        }
        console.log("submitComplete Done");
    });

    editor.dependent( ['SYSTEM_NODE.ID', 'ACTIVITY.ID'], function ( val, data, callback, e) {
        if (debug_dependencies) console.log("Dependency called for SYSTEM_NODE.ID or ACTIVITY.ID ", val, data, e);
        if (debug_dependencies) console.log("Getting task_data");
        $.ajax( {
            url: 'task_data.php',
            type: "POST",
            data: data,
            dataType: 'json',
            success: function ( json ) {
                var options = json.options['TASK.ID'];
                for (var i = 0; i < options.length; i++) {
                    options[i].label = '<b>' + options[i].value + "</b>: " + (options[i].label ? options[i].label : '');
                }
                callback( json );
                var task_id_field = editor.field( 'TASK.ID' );
                if (options.length == 0) {
                    task_id_field.input().attr( 'placeholder', 'No Tasks to Select' );
                    task_id_field.input().children().first().text('No Tasks to Select');
                } else {
                    task_id_field.input().attr( 'placeholder', 'Select a Task' );
                    task_id_field.input().children().first().text('Select a Task');
                }
                if (debug_dependencies) console.log("Done task_data");
            }
        });

        // TBR now used only for filtering
        // if ((data.values['SYSTEM_NODE.ID'] != "") && (data.values['ACTIVITY.ID'] != "")) {
        //     // NOTE Both SYSTEM_ID and ACTIVITY_ID need to be set
        //     if (debug_dependencies) console.log("Showing TASK.ID field");
        //     this.field('TASK.ID').show();
        // } else {
        //     // this.field('TASK.ID').hide();
        // }
        return {};
    });

    editor.dependent( 'TASK.ID', function ( val, data, callback, e) {
        if (debug_dependencies) console.log("Dependency called for TASK.ID ", val, data, e);
        if (debug_dependencies) console.log("Getting requirement_data");
        $.ajax( {
            url: 'requirement_data.php',
            type: "POST",
            data: data,
            dataType: 'json',
            success: function ( json ) {
                var options = json.options['COMMITMENTS.RES_REQUIREMENT_ID'];
                for (var i = 0; i < options.length; i++) {
                    options[i].label = options[i].value + ": " + (options[i].label ? options[i].label : '');
                }
                callback( json );
                var requirement_id_field = editor.field( 'COMMITMENTS.RES_REQUIREMENT_ID' );
                requirement_id_field.input().attr( 'placeholder', 'Select a Requirement' );
                requirement_id_field.input().children().first().text('Select a Requirement');
                // requirement_id_field.show();
                if (debug_dependencies) console.log("Done requirement_data");
            }
        });

        if (val === '') {
            //if (debug_dependencies) console.log("Hiding COMMITMENTS.RES_REQUIREMENT_ID");
            //this.field('COMMITMENTS.RES_REQUIREMENT_ID').hide();
        } else {
            if (debug_dependencies) console.log("Showing COMMITMENTS.RES_REQUIREMENT_ID");
            this.field('COMMITMENTS.RES_REQUIREMENT_ID').show();
        }
        return {};
    });
    // editor.dependent( 'TASK.ID', 'requirement_data.php' );

    editor.dependent( 'FUNDING_AGENCY.ID', function ( val, data, _callback, e) {
        if (debug_dependencies) console.log("Dependency called for FUNDING_AGENCY.ID ", val, data, e);
        if (val === '') {
            if (debug_dependencies) console.log("Hiding COMMITMENTS.INSTITUTE_ID");
            this.field('COMMITMENTS.INSTITUTE_ID').hide();
        } else {
            if (debug_dependencies) console.log("Showing COMMITMENTS.INSTITUTE_ID");
            this.field('COMMITMENTS.INSTITUTE_ID').show();
        }
        return {};
    });
    editor.dependent( 'FUNDING_AGENCY.ID', 'institute_data.php' );

    editor.dependent(['COMMITMENTS.RES_REQUIREMENT_ID', 'COMMITMENTS.YEAR', 'COMMITMENTS.VALUE', 'COMMITMENTS.INSTITUTE_ID', 'COMMITMENTS.DESCRIPTION'],
        function ( val, data, _callback, e) {
            if (debug_dependencies) console.log("Dependency called for any COMMITMENTS field update ", val, data, e);

            if (debug_dependencies) console.log("Toggling red/green");
            if (data.values['COMMITMENTS.RES_REQUIREMENT_ID'] == '' ||
                data.values['COMMITMENTS.YEAR'] == '' ||
                data.values['COMMITMENTS.VALUE'] == '' ||
                data.values['COMMITMENTS.INSTITUTE_ID'] == '') {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-green");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-red");
            } else {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-red");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-green");
            }

            validate();

            return {};
        }
    );

    // Activate an inline edit on click of a table cell
    $('#table').on( 'click', 'tbody td:not(.select-checkbox)', function (e) {
        if (debug) console.log("Clicked any cell except select-checkbox", e);
        // Focus on the input in the cell that was clicked when Editor opens
        editor.one( 'open', () => {
            $('input', this).focus();
        });

        table.rows().deselect();

        table.buttons( ['.buttons-create'] ).enable(false);
        editor.on('preClose', function(e) {
            if (debug) console.log('preClose', e);
            update_buttons(table, current_user, user_roles);
            update_selected(table);
            if (debug) console.log('preClose Done');
        });

        if (debug) console.log("Enable all editable cells");
        // Only enable the (databasetable) editable fields
        editor.inline(
            table.cells(this.parentNode, ".editable").nodes(), {
                onBlur: 'submit',
            }
        );
    } );

    // Setup DataTable
    var table = new DataTable('#table', {
        lengthChange: false,

        layout: {
            top2Start: 'buttons',
            top2End: 'search',
            top: 'searchPanes',
            topStart: null,
            topEnd: null,
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        ajax: {
            url: data_script,
            type: "POST",
            dataSrc: function ( json ) {
                current_user = json.user;

                // Update User Roles
                user_roles = decode_user_roles(json.options["COMMITMENTS.ID"]);
                console.log("User Roles", user_roles);

                set_user(json.user, json.has_role_admin, json.has_role_role_manager, user_roles, table);
                update_roles(json.data, current_user, user_roles, table);

                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                // Convert 'use_task' records
                for (var i = 0; i < json.data.length; i++) {
                    if (json.data[i].COMMITMENTS.RES_REQUIREMENT_ID == json.data[i].TASK.ID) {
                        json.data[i].COMMITMENTS.RES_REQUIREMENT_ID = '0';
                    }
                }

                return json.data;
            }
        },
        fixedColumns: {
            leftColumns: fixed_columns
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        // switch off scroller and paging in case of a test to make sure it loads all records
        paging:         !is_test,
        scroller:       !is_test,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,  // NOTE: delete local storage for searchpanes to work first time...

        // for SearchPanes
        columnDefs: [
            {
                searchPanes: {
                    show: false,
                },
                targets: [0, 1, 2, 3, 6, 8, 13, 14],
            },
            {
                searchPanes: {
                    initCollapsed: true,
                    cascadePanes: true,
                    show: true,
                },
                targets: [4, 5, 7, 9, 10, 11, 12],
            },
            {
                targets: [13],
                render: function ( data, _type, _row, _meta ) {
                    return data.toFixed(2);
                },
            },
        ],
        // serverSide: true,

        columns: [
            {
                data: "CAN_EDIT",
                className: "can_edit",
                visible: false,
            },
            {
                data: "CAN_EDIT_BECAUSE",
                className: "can_edit_because",
                visible: false,
            },
            {
				data: null,
				defaultContent: '',
				className: 'select',
				orderable: true,
                searchable: false,
                editField: "COMMITMENTS.ID",
                render: function ( data, type, row ) {
                    // if (debug) console.log(type+" "+data+" "+row.CAN_EDIT);
                    return type == 'sort' ? row.CAN_EDIT : data;
                },
			},
            {
                data: "COMMITMENTS.ID",
                className: 'id dt-body-right dt-foot-right'
            },
            {
                data: "SYSTEM_NODE.TITLE",
                editField: "SYSTEM_NODE.ID",
                className: 'system dt-foot-right',
                defaultContent: ''
            },
            {
                data: "ACTIVITY.TITLE",
                editField: "ACTIVITY.ID",
                className: 'activity dt-foot-right',
                defaultContent: ''
            },
            {
                data: "TASK.ID",
                className: 'taskid dt-body-right dt-foot-right',
                defaultContent: ''
            },
            {
                data: "TASK.SHORT_TITLE",
                // editField: "TASK.ID",
                className: 'task dt-foot-right',
                defaultContent: ''
            },
            {
                data: "COMMITMENTS.RES_REQUIREMENT_ID",
                className: 'reqid dt-body-right dt-foot-right',
                defaultContent: '',
            },
            {
                data: "RES_REQUIREMENT.TITLE",
                editField: "COMMITMENTS.RES_REQUIREMENT_ID",
                className: 'requirement dt-foot-right',
                defaultContent: '',
                // render: function ( data, _type, row ) {
                //     return row.COMMITMENTS.RES_REQUIREMENT_ID == 0 ? use_task : data;
                // },
            },
            {
                data: "FUNDING_AGENCY.NAME",
                editField: "FUNDING_AGENCY.ID",
                className: 'funding_agency dt-foot-right',
                defaultContent: ''
            },
            {
                data: "INSTITUTE.ONAME",
                editField: "COMMITMENTS.INSTITUTE_ID",
                className: 'institute dt-foot-right',
                defaultContent: ''
            },

            {
                data: "COMMITMENTS.YEAR",
                className: 'year dt-body-right dt-foot-right',
            },
            {
                data: "COMMITMENTS.VALUE",
                className: 'value editable dt-body-right dt-foot-right',
            },
            {
                data: "COMMITMENTS.DESCRIPTION",
                className: 'description comments editable'
            },
        ],
        order: [ 6, 'asc' ],        // keep sorting on task id for the test record to be visible
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },
        buttons: [
            {
                extend: "create",
                editor: editor,
                formTitle: "Create New " + (is_prediction ? "Prediction" : "Commitment"),
                formButtons: [
                    { text: 'Create', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            // { extend: "createInline", editor: editor, text: 'New (Inline)' },
            {
                extend: "edit",
                editor: editor,
                formTitle: "Edit " + (is_prediction ? "Prediction" : "Commitment"),
                formButtons: [
                    { text: 'Edit', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: "selected",
                text: 'Duplicate',
                className: 'x-buttons-duplicate',
                action: function ( _e, _dt, _node, _config ) {
                    // Start in edit mode, and then change to create
                    editor
                        .edit( table.rows( {selected: true} ).indexes(), {
                            title: 'Duplicate ' + (is_prediction ? "Prediction" : "Commitment"),
                            buttons: [
                                { text: 'Create from existing', className: 'submit', action: function () { this.submit(); } },
                                { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                            ]
                        } )
                        .mode( 'create' );
                }
            },
            {
                extend: "remove",
                editor: editor,
                formTitle: "Delete " + (is_prediction ? "Prediction" : "Commitment"),
                formButtons: [
                    { text: 'Delete', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        rowCallback: function( row, data ) {
            if ( data.CAN_EDIT ) {
                $('td.select', row).addClass( 'select-checkbox' );
                $('td.select', row).addClass( 'select-enabled' );
            } else {
                $('td.select', row).removeClass( 'select-checkbox' );
                $('td.select', row).removeClass( 'select-enabled' );
            }
        },
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            var api = this.api();

            update_selected(api);
            count_unique(api, 4);
            count_unique(api, 5);
            count_unique(api, 6);
            count_unique(api, 8);
            count_unique(api, 10);
            count_unique(api, 11);
            sum(api,13);
        }
    } );

    table.on( 'draw select deselect', function() {
        var enable_edit = has_role_admin(current_user, user_roles);

        if (enable_edit) {
            // end inline edit if any
            editor.close();
        }

        update_buttons(table, current_user, user_roles);
        update_selected(table);
    });

    table.paging = !is_test;
    table.scroller = !is_test;
    table.columns( [0,1,3] ).visible( debug );
} );
