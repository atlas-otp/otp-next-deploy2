
function creperson(data, creuser) {
    // Combine the first and last names into a single table field
    if (data == null) return null;

    if ('CREPERSON' in data) {
        return person(data.CREPERSON.LNAME, data.CREPERSON.FNAME);
    } else {
        return creuser;
    }
}

function crud(t_delete, t_pk_prev) {
    if (t_delete == 'Y') {
        return "Delete";
    }

    if (t_pk_prev == null) {
        return "Create";
    }

    return  "Update";
}

function description(data) {
    return data != null ? "'" + data + "'" : "";
}

$(document).ready(function() {
    var debug = false;

    // Setup DataTable
    var table = $('#table_history').DataTable( {
        lengthChange: false,
        layout: {
            topStart: 'buttons',
            topEnd: 'search',
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        ajax: {
            url: "commitments_history_data.php",
            type: "POST",
            dataSrc: function ( json ) {
                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                console.log(json.options);
                var user_roles = decode_user_roles(json.options['COMMITMENTS_AVER.ID']);
                set_user(json.user, false,false, user_roles, table);

                return json.data;
            }
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,
        deferLoading:   50,

        paging:         true,
        scroller:       true,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,    // NOTE: delete local storage for searchpanes to work first time...
        // fuzzySearch: {
        //     toggleSmart: true
        // },

        // for SearchPanes
        // columnDefs: [
        //     {
        //         searchPanes: {
        //             show: false,
        //         },
        //         targets: [0, 1, 2, 3, 5, 10, 11],
        //     },
        //     {
        //         searchPanes: {
        //             initCollapsed: true,
        //             cascadePanes: true,
        //             show: true,
        //         },
        //         targets: [4, 6, 7, 8, 9],
        //     }
        // ],
        // serverSide: true,

        columns: [
            {
                data: "COMMITMENTS_AVER.T_WORKID",
                className: 't_workid'
            },
            {
                data: "COMMITMENTS_AVER.T_PK",
                className: 't_pk'
            },
            {
                data: "COMMITMENTS_AVER.T_PK_PREV",
                className: 't_pk_prev'
            },
            {
                data: "COMMITMENTS_AVER.A_CREUSER",
                className: 'a_creuser',
                visible: debug
            },
            {
                data: null,
                render: function( data, _type, row ) {
                    return creperson(data, row.COMMITMENTS_AVER.A_CREUSER);
                },
                className: 'creperson',
            },
            {
                data: "COMMITMENTS_AVER.A_CREDATE",
                className: 'a_credate'
            },
            {
                data: "COMMITMENTS_AVER.T_START",
                className: 't_start',
                visible: debug
            },

            {
                data: "COMMITMENTS_AVER.T_STATUS",
                className: 't_status',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.T_DELETE",
                className: 't_delete',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.IS_CURRENT_VERSION",
                className: 'is_current_version',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.IS_CURRENT_AUTH_VERSION",
                className: 'is_current_auth_version',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.VERSION",
                className: 'version'
            },

            {
                data: null,
                render: function( _data, _type, row ) {
                    return crud(row.COMMITMENTS_AVER.T_DELETE, row.COMMITMENTS_AVER.T_PK_PREV);
                },
                className: 'type',
            },

            {
                data: null,
                render: function( data, _type, row ) {
                    var result = "<ul>";

                    var type = crud(row.COMMITMENTS_AVER.T_DELETE, row.COMMITMENTS_AVER.T_PK_PREV);

                    if (row.COMMITMENTS_AVER.T_STATUS == "R") {
                        result += "<li>Record was REVOKED</li>"
                    }

                    if (row.COMMITMENTS_AVER.T_STATUS == "N") {
                        result += "<li>Record is NEW</li>"
                    }

                    if (row.COMMITMENTS_AVER.T_DELETE == 'Y') {
                        result += "<i>"
                    }

                    if (row.COMMITMENTS_AVER.IS_CURRENT_VERSION == 'Y') {
                        result += "<b>"
                    }

                    switch(type) {
                        case "Create":
                        case "Delete":
                            result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": "
                                    + row.COMMITMENTS_AVER.YEAR
                                    + ", "
                                    + row.COMMITMENTS_AVER.VALUE
                                    + ", "
                                    + row.TASK.SHORT_TITLE
                                    + "/"
                                    + row.RES_REQUIREMENT.TITLE
                                    + ", "
                                    + row.INSTITUTE.ONAME
                                    + ", "
                                    + description(row.COMMITMENTS_AVER.DECRIPTION)
                                    + "</li>";
                            break;
                        case "Update":

                            if (row.COMMITMENTS_AVER.ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": ID changed from "
                                    + row.COMMITMENTS_AVER.ID_PREV
                                    + " to "
                                    + row.COMMITMENTS_AVER.ID
                                    + "</li>";
                            }

                            if (row.COMMITMENTS_AVER.YEAR_CHANGED == "Y") {
                                result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": Year changed from "
                                    + row.COMMITMENTS_AVER.YEAR_PREV
                                    + " to "
                                    + row.COMMITMENTS_AVER.YEAR
                                    + "</li>";
                            }

                            if (row.COMMITMENTS_AVER.VALUE_CHANGED == "Y") {
                                result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": Value changed from "
                                    + row.COMMITMENTS_AVER.VALUE_PREV
                                    + " to "
                                    + row.COMMITMENTS_AVER.VALUE
                                    + "</li>";
                            }

                            if (row.COMMITMENTS_AVER.RES_REQUIREMENT_ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": Requirement changed from "
                                    + row.TASK_PREV.SHORT_TITLE + "/" + row.RES_REQUIREMENT_PREV.TITLE
                                    + " to "
                                    + row.TASK.SHORT_TITLE + "/" + row.RES_REQUIREMENT.TITLE
                                    + "</li>";
                            }

                            if (row.COMMITMENTS_AVER.INSTITUTE_ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": Instutute changed from "
                                    + row.INSTITUTE_PREV.ONAME
                                    + " to "
                                    + row.INSTITUTE.NAME
                                    + "</li>";
                            }

                            if (row.COMMITMENTS_AVER.DESCRIPTION_CHANGED == "Y") {
                                result += "<li>"
                                    + row.COMMITMENTS_AVER.ID
                                    + ": Description changed from "
                                    + description(row.COMMITMENTS_AVER.DESCRIPTION_PREV)
                                    + " to "
                                    + description(row.COMMITMENTS_AVER.DESCRIPTION)
                                    + "</li>";
                            }

                            break;
                    }

                    if (row.COMMITMENTS_AVER.IS_CURRENT_VERSION == 'Y') {
                        result += "</b>"
                    }

                    if (row.COMMITMENTS_AVER.T_DELETE == 'Y') {
                        result += "</i>"
                    }

                    result += "</ul>"
                    return result;
                },
                className: 'changed'
            },

            {
                data: "COMMITMENTS_AVER.ID_CHANGED",
                className: 'id_changed',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.ID_PREV",
                className: 'id_prev',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.ID",
                className: 'id',
                visible: debug
            },

            // {
            //     data: null,
            //     render: function( data, _type, _row ) {
            //         // Combine the first and last names into a single table field
            //         return data != null ? data.PUB_PERSON_PREV.LNAME + ', ' + data.PUB_PERSON_PREV.FNAME : data;
            //     },
            //     className: 'user_prev',
            //     visible: debug
            // },

            {
                data: "COMMITMENTS_AVER.YEAR_CHANGED",
                className: 'year_changed',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.YEAR_PREV",
                className: 'year_prev',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.YEAR",
                className: 'year',
                visible: debug
            },

            {
                data: "COMMITMENTS_AVER.VALUE_CHANGED",
                className: 'value_changed',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.VALUE_PREV",
                className: 'value_prev',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.VALUE",
                className: 'value',
                visible: debug
            },

            {
                data: "COMMITMENTS_AVER.RES_REQUIREMENT_ID_CHANGED",
                className: 'res_requirement_id_changed',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.RES_REQUIREMENT_ID_PREV",
                className: 'res_requirement_id_prev',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.RES_REQUIREMENT_ID",
                className: 'res_requirement_id',
                visible: debug
            },

            {
                data: "COMMITMENTS_AVER.INSTITUTE_ID_CHANGED",
                className: 'institute_id_changed',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.INSTITUTE_ID_PREV",
                className: 'institute_id_prev',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.INSTITUTE_ID",
                className: 'institute_id',
                visible: debug
            },

            {
                data: "COMMITMENTS_AVER.DESCRIPTION_CHANGED",
                className: 'description_changed',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.DESCRIPTION_PREV",
                className: 'description_prev',
                visible: debug
            },
            {
                data: "COMMITMENTS_AVER.DESCRIPTION",
                className: 'description',
                visible: debug
            }
        ],
        order: [[ 1, 'desc' ]],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },

    } );
 } );
