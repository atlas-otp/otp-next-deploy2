const CAN_EDIT = 0, CAN_EDIT_BECAUSE = 1, SELECT = 2;
const REQUIREMENT_ID = 3, REQUIREMENT = 4;
const TASK_ID = 5, TASK = 6;
const LOAD = 7, ARCHIVED = 8, RECOGNITION_CODE = 9, RECOGNITION = 10, REQ_EQUALS_ALLOC = 11;

const WILL_BE_ASSIGNED = "Will be assigned";
const SUPPLY_A_SUBTASK_NAME = "Supply a Sub-Task Name";
const SELECT_A_TASK = "Select a Task";
const SELECT_A_RECOGNITION = "Select a Recognition";
const SUPPLY_A_LOAD = "Supply a Load";
// const SELECT_AN_ACTIVITY = "Select an Activity";
// const SELECT_A_WBS = "Select a WBS";

let editor; // use a global for the submit and return data rendering
let table;
let debug;
let current_user;
let user_roles;
let data_script = "sub_tasks_data.php";
let duplicate = false;

let allow_new = false;
let allow_edit = false;
let allow_duplicate = false;
let allow_remove = false;

function add_can_edit(data, user, user_roles) {
    current_user = user;

    for (let i = 0; i < data.length; i++) {
        if (data[i].ROLE == 'ROLE_ADMIN') {
            // we do not want to change any ADMIN roles
            data[i].CAN_EDIT = false;
            data[i].CAN_EDIT_BECAUSE = "BLOCKED";
        // } else if (data[i].RO) {
        //     // we cannot change VIEWs
        //     data[i].CAN_EDIT = false;
        //     data[i].CAN_EDIT_BECAUSE = "VIEW";
        } else {
            let can_edit_and_because = can_edit(user, user_roles, data[i].SYSTEM_ID, data[i].ACTIVITY_ID, data[i].WBS_ID);
            data[i].CAN_EDIT = can_edit_and_because.can_edit;
            data[i].CAN_EDIT_BECAUSE = can_edit_and_because.can_edit_because;
        }
    }
}

$(document).ready(function() {
    let queryString = window.location.search;
    let params = new URLSearchParams(queryString);

    // query string ?test
    let is_test = params.get("test") !== null;

    // query string ?debug
    debug = params.get("debug") !== null;
    let fixed_columns = debug ? 6 : 2;

    let is_local = window.location.hostname == 'localhost' || window.location.hostname == 'otp-next.localhost';

    let fixed_left_columns = debug ? 5 : 3;     // SELECT, TASK_ID, TASK
                             //(year != null ? debug ? 10 : 6 : 0) +                       // SELECT, NAME, INST, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             //(task_id != null ? debug ? 12 : 6 : 0) +                    // SELECT, YEAR, NAME, INST, REQ_ID, REQUIREMENT
                             //(req_id != null ? debug ? 10 : 4 : 0) +                     // SELECT, YEAR, NAME, INST
                             //(person_id != null ? debug ? 10 : 5 : 0) +                  // SELECT, YEAR, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             //(last_institute_id != null ? debug ? 10 : 6 : 0) +          // SELECT, YEAR, NAME, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             //((system != null) && (year == null) ? debug ? 10 : 6 : 0);  // SELECT, YEAR, NAME, INST, TASK_ID_REQ_ID, TASK_REQUIREMENT,
    let fixed_right_columns = 0;

    if (debug) {
        console.log(window.location);
        console.log("test = ", is_test);
        console.log("debug = ", debug);
        console.log("local = ", is_local);
    }

    // Setup Editor
    editor = new DataTable.Editor({
        ajax: {
            url: data_script,
            // Modify what we send if needed
            data: function ( d ) {
                // For CREATE and DELETE no modifications needed
                if ((d.action == 'create') || (d.action == 'remove')) {
                    if (debug) console.log("Data unchanged for CREATE or REMOVE");
                    return;
                }

                // NOTE below breaks double editing as 'table' changes
                // related test: test_edit_inline_system_twice
                // Send also initial values before change in 'init_data'
                // to enable to detect (and possibly resolve) conflicts (a la git)
                // [row, value] = Object.entries(d.data)[0];
                // [table, value] = Object.entries(value)[0];
                // [field, value] = Object.entries(value)[0];

                // // if (debug) console.log(editor.init_data['ROLE_ASSIGNMENT']);
                // // FIXME check for row, for multi-submit
                // d.init_data = {}
                // d.init_data[row] = {}
                // d.init_data[row][table] = {}
                // d.init_data[row][table][field] = editor.init_data[table][field]

                if (debug) console.log("Data populated for EDIT (currently disabled)");
                if (debug) console.log(d);
            }
        },
        table: "#table",
        fields: [
            // all Editable fields
            {
                label: "Sub-Task ID:",
                name: "RES_REQUIREMENT.ID",
                type: "display",
                def: WILL_BE_ASSIGNED,
                data: function (data, _type, _set) {
                    if (duplicate) {
                        return WILL_BE_ASSIGNED;
                    }
                    return data.RES_REQUIREMENT.ID;
                }
            },
            {
                label: "Sub-Task:",
                name: "RES_REQUIREMENT.TITLE",
                data: function (data, _type, _set) {
                    if (duplicate) {
                        return "COPY: " + data.RES_REQUIREMENT.TITLE;
                    }
                    return data.RES_REQUIREMENT.TITLE;
                },
                type: "text",
                attr: {
                    maxlength: 250,
                    placeholder: SUPPLY_A_SUBTASK_NAME
                }
            },
            {
                label: "Task:",
                name: "RES_REQUIREMENT.TASK_ID",
                type: 'datatable',
                config: {
                    paging:         true,
                    scrollY:        150,
                    scrollCollapse: true,
                    deferRender:    true,
                    scroller:       false,
                    columns: [
                        {
                            title: '<b>Task ID</b>: Name',
                            data: 'label'
                        }
                    ],
                    order: 0
                },
                placeholder: SELECT_A_TASK,
                placeholderValue: ""
            },
            {
                label: "Load:",
                name: "RES_REQUIREMENT.LOAD",
                def: 100,
                type: "text",
                attr: {
                    maxlength: 50,
                    placeholder: SUPPLY_A_LOAD
                }
            },
            {
                label: "Archived:",
                name: "RES_REQUIREMENT.ARCHIVED",
                type: "checkbox",
                separator: '|',
                options: [
                    { label: '', value: 1 }
                ],
            },
            {
                label: "Recognition Code:",
                name: "RES_REQUIREMENT.RECOGNITION_CODE",
                type: "select",
                placeholder: SELECT_A_RECOGNITION,
                placeholderValue: ""
            },
            {
                label: "Req == Alloc:",
                name: "RES_REQUIREMENT.REQ_EQUALS_ALLOC",
                type: "checkbox",
                separator: '|',
                options: [
                    { label: '', value: 1 }
                ],
            },

        ]
    } );

    editor.on('initEdit', function (_e, node, data, items, type) {
        // Called just before editing (inline or normal) is done.
        if (debug) console.log('initEdit', node, type, data, items);

        // TBD Store initial data before change (as prepared in ajax.data)
        // to enable to detect (and possibly resolve) conflicts (a la git)
        editor.init_data = data;
    });

    editor.on('preOpen', function(e, mode, action) {
        // Called before a form (inline or normal) is opened. Called after initEdit.
        if (debug) console.log('preOpen', mode, action, e);

        if (action === 'create') {
            if (!has_role_admin(current_user, user_roles)) {
                if (debug) console.log("No new roles can be created if not ADMIN");
                return false;
            }
        } else {
            // NOTE: editor.field('CAN_EDIT').val() is not filled at first select, so we use the datatable
            let modifier = editor.modifier();
            let data = table.row( modifier ).data();

            // No edits/deletes if CAN_EDIT field is false
            if (data && (typeof data !== "undefined") && !data.CAN_EDIT) {
                if (debug) console.log("No edits if CAN_EDIT field is false");
                return false;
            }

            // No edits possible if no username
            if ((data.USERNAME == '') && (action == 'edit')) {
                if (debug) console.log("No edits possible if no username");
                return false;
            }
        }

        do_validate = false;

        // set_change_handler(editor, 'SYSTEM_ID', mode, debug);
        // set_change_handler(editor, 'WBS_ID', mode, debug);
        // set_change_handler(editor, 'END_DATE', mode, debug);

        // // Final select on ActivityId (if activiy manager) should submit
        // editor.field('ACTIVITY_ID').input().off('change');
        // if (mode === 'inline') {
        //     editor.field('ACTIVITY_ID').input().on('change', function(e, data) {
        //         if (!data || !data.editor) {
        //             if (editor.field('ROLE').val() == 'ROLE_ACTIVITY_MANAGER') {
        //                 if (debug) console.log("Select Submit on activity manager ActivityId");
        //                 editor.submit();
        //             }
        //         }
        //     } );
        // }

    } );

    let do_validate = false;

    function validate() {
        if (do_validate) {
            // Check for valid sub-task
            let subtask = editor.field('RES_REQUIREMENT.TITLE');
            subtask.error(subtask.val() == "" ? SUPPLY_A_SUBTASK_NAME : "");

            let task_id = editor.field('RES_REQUIREMENT.TASK_ID');
            task_id.error(task_id.val() == "" ? SELECT_A_TASK : "");

            let load = editor.field('RES_REQUIREMENT.LOAD');
            load.error(load.val() == "" ? SUPPLY_A_LOAD : "");

            let recognition_code = editor.field('RES_REQUIREMENT.RECOGNITION_CODE');
            recognition_code.error(recognition_code.val() == "" ? SELECT_A_RECOGNITION : "");
        }
    }

    editor.on('preSubmit', function(e, data, action) {
        if (debug) console.log('preSubmit', e, action, data);

        do_validate = true;

        // Validate fields before submitting to server

        // DELETE is ok
        if (action == "remove") {
            if (debug) console.log("DELETE is ok");
            return true;
        }

        let row = data.data[Object.keys(data.data)[0]];

        delete row.RES_REQUIREMENT.ID;

        validate();

        // Any missing fields, abort submit
        if ( this.inError() ) {
            if (debug) console.log("Some error prevented submit for action: ", action);
            return false;
        }

        return true;
    });

    // handle server replies and errors
    editor.on('postSubmit', function (_e, json, data, action, xhr) {
        if (debug) console.log("postSubmit", action, json, data, xhr);

        if (json === null) {
            console.log(data);
            console.log('responseText:', xhr.responseText);
            alert("Server error in script: " + data_script + ", look at console log.");
        }

        update_roles(json.data, current_user, user_roles, table);

        //   if ( json.FieldErrors ) {
        //      .... take a look at json.fieldErrors
        //   }
    });

    // jump to new or edited record
    editor.on( 'submitComplete', function ( e, json, data, action ) {
        if (debug) console.log("submitComplete", data, action);
        if ( action === 'create' || action === 'edit' ) {
            if (is_test) {
                $(`#${data.DT_RowId}`).addClass("test_subtasks_record");
            }
            table.row(`#${data.DT_RowId}`).scrollTo();
        }

        // Deselect original after copy
        if (duplicate) {
            table.rows().deselect();
        }
        duplicate = false;
    });

    //  USER_ID -> PICTURE_URL
    // editor.dependent( 'USER_ID', 'user_data.php' );
    // editor.dependent('USER_ID', function ( val, data, callback, e) {
    //     if (debug) console.log("Dependency called for USER_ID -> PICTURE_URL", val, data, e);
    //     $.ajax( {
    //         url: 'picture_url_data.php',
    //         data: data,
    //         dataType: 'json',
    //         success: function ( json ) {
    //             let picture_url = json.options.PICTURE_URL;
    //             let picture_url_field = $('#DTE_Field_PICTURE_URL');
    //             // Set my own picture if running locally
    //             picture_url =  !picture_url ? '../img/no_photo.png' : is_local ? '../img/duns.jpg' : picture_url;
    //             if (debug) console.log("picture_url: ", picture_url);
    //             $('img', picture_url_field).attr( 'src', picture_url);
    //         }
    //     } );

    //     return {};
    // });

    // ROLE
    // editor.dependent('ROLE', function ( val, data, _callback, e) {
    //     if (debug) console.log("Dependency called for ROLE ", val, data, e);

    //     // Reset fields if we change roles
    //     let old_val = data.row != undefined ? data.row.ROLE : undefined;
    //     if (val != old_val) {
    //         if (debug) console.log("Resetting System, Activity and WBS");
    //         this.field('SYSTEM_ID').val('');
    //         this.field('ACTIVITY_ID').val('');
    //         this.field('WBS_ID').val('');
    //     }

    //     switch(val) {
    //         case "ROLE_ADMIN":
    //             if (debug) console.log("ADMIN: hide all fields");
    //             return { hide: ['USERNAME', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID'] };
    //         case "ROLE_ROLE_MANAGER":
    //             if (debug) console.log("ROLE_MANAGER: hide all fields");
    //             return { hide: ['USERNAME', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID'] };
    //         case "ROLE_ACTIVITY_MANAGER":
    //             if (debug) console.log("ACTIVITY: show WBS_ID");
    //             return { show: ['ACTIVITY_ID'], hide: ['USERNAME', 'SYSTEM_ID', 'WBS_ID'] };
    //         case "ROLE_SYSTEM_MANAGER":
    //             if (debug) console.log("SYSTEM: show SYSTEM");
    //             return { show: ['SYSTEM_ID'], hide: ['USERNAME', 'ACTIVITY_ID', 'WBS_ID'] };
    //         case "ROLE_WBS_MANAGER":
    //             if (debug) console.log("WBS: show ACTIVITY, WBS");
    //             return { show: ['ACTIVITY_ID', 'WBS_ID'], hide: ['USERNAME', 'SYSTEM_ID'] };
    //         default:
    //             if (debug) console.log("default: hide all fields");
    //             return { hide: ['USERNAME', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID'] };
    //     }
    // } );

    // editor.dependent( 'ACTIVITY_ID', 'wbs_data.php' );

    // FIXME ENABLE
    // editor.dependent(['USER_ID', 'ROLE', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID', 'COMMENTS'],
    //     function ( val, data, callback, e) {
    //         if (debug) console.log("Dependency called for any ROLE_ASSIGNMENT field update ", val, data, e);

    //         if (data.values.USER_ID == '' ||
    //             data.values.ROLE == '' ||
    //             (data.values.ROLE == 'ROLE_SYSTEM_MANAGER' && data.values.SYSTEM_ID == '') ||
    //             (data.values.ROLE == 'ROLE_ACTIVITY_MANAGER' && data.values.ACTIVITY_ID == '') ||
    //             (data.values.ROLE == 'ROLE_WBS_MANAGER' && data.values.WBS_ID == '')) {
    //             $('div.DTE_Form_Buttons button.submit').removeClass("gradient-green");
    //             $('div.DTE_Form_Buttons button.submit').addClass("gradient-red");
    //         } else {
    //             $('div.DTE_Form_Buttons button.submit').removeClass("gradient-red");
    //             $('div.DTE_Form_Buttons button.submit').addClass("gradient-green");
    //         }

    //         validate();
    //         return {};
    //     }
    // );

    // Activate an inline edit on click of a table cell
    // $('#table').on( 'click', 'tbody td:not(.select-checkbox)', function (e) {
    //     if (debug) console.log("Clicked any cell except select-checkbox", e);
    //     // Focus on the input in the cell that was clicked when Editor opens
    //     editor.one( 'open', () => {
    //         $('input', this).focus();
    //     });

    //     table.buttons( ['.buttons-create'] ).enable(false);
    //     editor.one('preClose', function(e) {
    //         if (debug) console.log('preClose', e);
    //         table.buttons( ['.buttons-create'] ).enable(has_role_admin(current_user, user_roles));
    //     });

    //     if (debug) console.log("Enable all editable cells");
    //     // Only enable the (databasetable) editable fields
    //     editor.inline(
    //         table.cells(this.parentNode, ".editable").nodes(), {
    //             onBlur: 'submit',
    //         }
    //     );
    // } );

    // Setup DataTable
    table = new DataTable('#table', {
        lengthChange: false,
        layout: {
            topStart: 'buttons',
            topEnd: 'search',
        //     top: 'searchPanes',
        //     topStart: null,
        //     topEnd: null,
        //     bottom: null,
             bottomStart: null,
        //     bottomEnd: null
        },
        ajax: {
            url: data_script,
            dataSrc: function ( json ) {
                current_user = json.user;

                // Update User Roles
                user_roles = decode_user_roles(json.options["RES_REQUIREMENT.ID"]);
                console.log("User Roles", user_roles);
                console.log("User", json.user, "has_admin_role", json.has_role_admin, "has_role_role_manager", json.has_role_role_manager);

                set_user(json.user, json.has_role_admin, json.has_role_role_manager, user_roles, table);
                update_roles(json.data, current_user, user_roles, table);

                let db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                // TBD
                allow_new = true; // json.options.TASK_ID.length > 0;
                allow_edit = allow_new;
                allow_duplicate = allow_new;
                allow_remove = allow_new;

                return json.data;
            }
        },
        fixedHeader: {
            header: true,
            footer: true
        },
        fixedColumns: {
            leftColumns: fixed_left_columns,
            rightColumns: fixed_right_columns
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        // switch off scroller and paging in case of a test to make sure it loads all records
        paging:         !is_test,
        scroller:       !is_test,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,    // NOTE: delete local storage for searchpanes to work first time...
        // fuzzySearch: {
        //     toggleSmart: true
        // },

        // for SearchPanes
        // columnDefs: [
        //     {
        //         searchPanes: {
        //             show: false,
        //         },
        //         targets: [0, 1, 2, 3, 4, 6, 11, 12, 13],
        //     },
        //     {
        //         searchPanes: {
        //             initCollapsed: true,
        //             cascadePanes: true,
        //             show: true,
        //         },
        //         targets: [5, 7, 8, 9, 10],
        //     }
        // ],
        // serverSide: true,

        columns: [
            {
                data: "CAN_EDIT",
                className: "can_edit",
                visible: false,
            },
            {
                data: "CAN_EDIT_BECAUSE",
                className: "can_edit_because",
                visible: false,
            },
            {
				data: null,
				defaultContent: '',
				className: 'select',
				orderable: true,
                searchable: false,
//                editField: "USER_ID",
                render: function ( data, type, row ) {
                    // if (debug) console.log(type+" "+data+" "+row.CAN_EDIT);
                    return type == 'sort' ? row.CAN_EDIT : data;
                }
			},
            {
                data: "RES_REQUIREMENT.ID",
                className: 'requirement_id',
                visible: true
            },
            {
                data: "RES_REQUIREMENT.TITLE",
                className: 'requirement'
            },
            {
                data: "RES_REQUIREMENT.TASK_ID",
                className: 'task_id',
                visible: true
            },
            {
                data: "TASK.SHORT_TITLE",
                className: 'task'
            },
            {
                data: "RES_REQUIREMENT.LOAD",
                className: 'load',
            },
            {
                searchable: false,
                data: "RES_REQUIREMENT.ARCHIVED",
                className: 'archived dt-body-center',
                render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="checkbox-archived" disabled>';
                    }
                    return data;
                },
            },
            {
                data: "RES_REQUIREMENT.RECOGNITION_CODE",
                className: 'recognition_code',
                visible: false
            },
            {
                data: "RECOGNITION.LABEL",
                className: 'recognition',
                editField: "RES_REQUIREMENT.RECOGNITION_CODE"
            },
            {
                searchable: false,
                data: "RES_REQUIREMENT.REQ_EQUALS_ALLOC",
                className: 'req_equals_alloc dt-body-center',
                render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="checkbox-req-equals-alloc" disabled>';
                    }
                    return data;
                },
            },
        ],
        order: [[ ARCHIVED, 'asc' ], [ REQUIREMENT_ID, 'asc' ]],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },
        // keys: {
        //     columns: ':not(:first-child)',
        //     keys: [ "\t".charCodeAt(0) ],    // or [ 9 ]
        //     editor: editor,
        //     editOnFocus: true
        // },

        // AutoFill and KeyTable
        // autoFill: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        // keys: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        buttons: [
            {
                extend: "create",
                editor: editor,
                formTitle: "Create New Sub-Task",
                formButtons: [
                    { text: 'Create', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            // { extend: "createInline", editor: editor, text: 'New (Inline)' },
            {
                extend: "edit",
                editor: editor,
                formTitle: "Edit Sub-Task",
                formButtons: [
                    { text: 'Edit', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: "selected",
                text: "Duplicate",
                className: 'buttons-duplicate',
                action: function ( e, dt, node, config ) {
                    // Start in edit mode, and then change to create
                    duplicate = true;
                    editor
                        .edit( table.rows( {selected: true} ).indexes(), {
                            title: 'Duplicate Sub-Task',
                            buttons: [
                                { text: 'Create from existing', className: 'submit', action: function () { this.submit(); } },
                                { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                            ]
                        } )
                        .mode( 'create' );
                },
            },
            {
                extend: "remove",
                editor: editor,
                formTitle: "Delete Sub-Task",
                formButtons: [
                    { text: 'Delete', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        rowCallback: function( row, data ) {
            if ( data.CAN_EDIT ) {
                $('td.select', row).addClass( 'select-checkbox' );
                $('td.select', row).addClass( 'select-enabled' );
            } else {
                $('td.select', row).removeClass( 'select-checkbox' );
                $('td.select', row).removeClass( 'select-enabled' );
            }

            // Set the checked state of the checkbox in the table
            $('input.checkbox-archived', row).prop( 'checked', data.RES_REQUIREMENT.ARCHIVED);
            $('input.checkbox-req-equals-alloc', row).prop( 'checked', data.RES_REQUIREMENT.REQ_EQUALS_ALLOC);
        },
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            let api = this.api();

            count_unique(api, TASK);
            count_unique(api, LOAD);
            count_unique(api, RECOGNITION);
        }
    } );

    table.on( 'draw select deselect', function () {
        if (debug) console.log('draw select deselect');
        // TBD
        let enable_new = allow_new;
        let enable_edit = allow_edit;
        let enable_duplicate = allow_new;
        let enable_remove = allow_remove;

        let selectedRows = table.rows( { search: 'applied', selected: true } );

        enable_new = enable_new && (selectedRows.count() == 0);
        enable_edit = enable_edit && (selectedRows.count() == 1);
        enable_duplicate = enable_duplicate && (selectedRows.count() == 1);
        enable_remove = enable_remove && (selectedRows.count() > 0);

        // table.rows( { selected: true } ).every( function ( _rowIdx, _tableLoop, _rowLoop ) {
        //     enable_edit = enable_edit && (this.data().USERNAME != '');
        // });

        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> new enabled", enable_new);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> edit enabled", enable_edit);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> duplicate enabled", enable_duplicate);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> remove enabled", enable_remove);

        count_selected(table, selectedRows.count(), SELECT);

        table.buttons( ['.buttons-create'] ).enable(enable_new);
        table.buttons( ['.buttons-edit'] ).enable(enable_edit);
        table.buttons( ['.buttons-duplicate'] ).enable(enable_duplicate);
        table.buttons( ['.buttons-remove'] ).enable(enable_remove);
    });

    // table.scrollX = !is_test;
    table.paging = !is_test;
    table.scroller = !is_test;
    table.columns( [CAN_EDIT, CAN_EDIT_BECAUSE] ).visible( debug );


    // Display the buttons
    // bootstrap specific
    // new $.fn.dataTable.Buttons( table, [
    //     { extend: "create", editor: editor },
    //     // FIXME: disabled as we cannot disable 'datatable' type
    //     { extend: "edit",   editor: editor },
    //     { extend: "remove", editor: editor }
    // ] );

    // Bootstrap specific
    // table.buttons().container().appendTo( $('.col-md-6:eq(0)', table.table().container() ) );

 } );
