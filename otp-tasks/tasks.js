const CAN_EDIT = 0, CAN_EDIT_BECAUSE = 1, SELECT = 2;
const TASK_ID = 3, TASK = 4, CATEGORY_CODE = 5, CATEGORY = 6;
const ARCHIVED = 7, LOCATION_CODE = 8, LOCATION = 9;
const SYSTEM_ID = 10, SYSTEM = 11;
const ACTIVITY_ID = 12, ACTIVITY = 13;
const WBS_ID = 14, WBS = 15;
const DESCRIPTION = 16, COMMENTS = 17;

const WILL_BE_ASSIGNED = "Will be assigned";
const SUPPLY_A_TASK_NAME = "Supply a Task Name";
const SELECT_A_CATEGORY = "Select a Category";
const SELECT_A_LOCATION = "Select a Location";
const SELECT_A_SYSTEM = "Select a System";
const SELECT_AN_ACTIVITY = "Select an Activity";
const SELECT_A_WBS = "Select a WBS";

let editor; // use a global for the submit and return data rendering
let table;
let debug;
let current_user;
let user_roles;
let data_script = "tasks_data.php";
let duplicate = false;

let allow_new = false;
let allow_edit = false;
let allow_duplicate = false;
let allow_remove = false;

function add_can_edit(data, user, user_roles) {
    current_user = user;

    for (let i = 0; i < data.length; i++) {
        if (data[i].ROLE == 'ROLE_ADMIN') {
            // we do not want to change any ADMIN roles
            data[i].CAN_EDIT = false;
            data[i].CAN_EDIT_BECAUSE = "BLOCKED";
        // } else if (data[i].RO) {
        //     // we cannot change VIEWs
        //     data[i].CAN_EDIT = false;
        //     data[i].CAN_EDIT_BECAUSE = "VIEW";
        } else {
            let can_edit_and_because = can_edit(user, user_roles, data[i].TASK.SYSTEM_ID, data[i].ACTIVITY_ID, data[i].WBS_ID);
            data[i].CAN_EDIT = can_edit_and_because.can_edit;
            data[i].CAN_EDIT_BECAUSE = can_edit_and_because.can_edit_because;
        }
    }
}

$(document).ready(function() {
    let queryString = window.location.search;
    let params = new URLSearchParams(queryString);

    // query string ?test
    let is_test = params.get("test") !== null;

    // query string ?debug
    debug = params.get("debug") !== null;

    let is_local = window.location.hostname == 'localhost' || window.location.hostname == 'otp-next.localhost';

    let fixed_left_columns = debug ? 5 : 3;     // SELECT, TASK_ID, TASK
                             //(year != null ? debug ? 10 : 6 : 0) +                       // SELECT, NAME, INST, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             //(task_id != null ? debug ? 12 : 6 : 0) +                    // SELECT, YEAR, NAME, INST, REQ_ID, REQUIREMENT
                             //(req_id != null ? debug ? 10 : 4 : 0) +                     // SELECT, YEAR, NAME, INST
                             //(person_id != null ? debug ? 10 : 5 : 0) +                  // SELECT, YEAR, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             //(last_institute_id != null ? debug ? 10 : 6 : 0) +          // SELECT, YEAR, NAME, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             //((system != null) && (year == null) ? debug ? 10 : 6 : 0);  // SELECT, YEAR, NAME, INST, TASK_ID_REQ_ID, TASK_REQUIREMENT,
    let fixed_right_columns = 0;

    if (debug) {
        console.log(window.location);
        console.log("test = ", is_test);
        console.log("debug = ", debug);
        console.log("local = ", is_local);
    }

    // Setup Editor
    editor = new DataTable.Editor({
        ajax: {
            url: data_script,
            // Modify what we send if needed
            data: function ( d ) {
                // For CREATE and DELETE no modifications needed
                if ((d.action == 'create') || (d.action == 'remove')) {
                    if (debug) console.log("Data unchanged for CREATE or REMOVE");
                    return;
                }

                // NOTE below breaks double editing as 'table' changes
                // related test: test_edit_inline_system_twice
                // Send also initial values before change in 'init_data'
                // to enable to detect (and possibly resolve) conflicts (a la git)
                // [row, value] = Object.entries(d.data)[0];
                // [table, value] = Object.entries(value)[0];
                // [field, value] = Object.entries(value)[0];

                // // if (debug) console.log(editor.init_data['ROLE_ASSIGNMENT']);
                // // FIXME check for row, for multi-submit
                // d.init_data = {}
                // d.init_data[row] = {}
                // d.init_data[row][table] = {}
                // d.init_data[row][table][field] = editor.init_data[table][field]

                if (debug) console.log("Data populated for EDIT (currently disabled)");
                if (debug) console.log(d);
            }
        },
        table: "#table",
        fields: [
            // all Editable fields
            {
                label: "Task ID:",
                name: "TASK.ID",
                type: "display",
                def: WILL_BE_ASSIGNED,
                data: function (data, _type, _set) {
                    if (duplicate) {
                        return WILL_BE_ASSIGNED;
                    }
                    return data.TASK.ID;
                }
            },
            {
                label: "Task:",
                name: "TASK.SHORT_TITLE",
                data: function (data, _type, _set) {
                    if (duplicate) {
                        return "COPY: " + data.TASK.SHORT_TITLE;
                    }
                    return data.TASK.SHORT_TITLE;
                },
                type: "text",
                attr: {
                    maxlength: 50,
                    placeholder: SUPPLY_A_TASK_NAME
                }
            },
            {
                label: "Category:",
                name: "TASK.CATEGORY_CODE",
                type: "select",
                placeholder: SELECT_A_CATEGORY,
                placeholderValue: ""
            },
            {
                label: "Archived:",
                name: "TASK.ARCHIVED",
                type: "checkbox",
                separator: '|',
                options: [
                    { label: '', value: 1 }
                ],
            },
            {
                label: "Location:",
                name: "TASK.TASK_LOCATION_CODE",
                type: "select",
                placeholder: SELECT_A_LOCATION,
                placeholderValue: ""
            },
            {
                label: "System:",
                name: "TASK.SYSTEM_ID",
                type: "select",
                placeholder: SELECT_A_SYSTEM,
                placeholderValue: ""
            },
            {
                label: "Activity:",
                name: "ACTIVITY.ID",
                type: "select",
                placeholder: SELECT_AN_ACTIVITY,
                placeholderValue: ""
            },
            {
                label: "WBS:",
                name: "TASK.WBS_ID",
                type: "select",
                placeholder: SELECT_A_WBS,
                placeholderValue: ""
            },
            {
                label: "Description:",
                name: "TASK.DESCRIPTION",
                type: 'textarea'
            },
            {
                label: "Comments:",
                name: "TASK.COMMENTS",
                type: 'textarea'
            },
        ]
    } );

    editor.on('initEdit', function (_e, node, data, items, type) {
        // Called just before editing (inline or normal) is done.
        if (debug) console.log('initEdit', node, type, data, items);

        // TBD Store initial data before change (as prepared in ajax.data)
        // to enable to detect (and possibly resolve) conflicts (a la git)
        editor.init_data = data;
    });

    editor.on('preOpen', function(e, mode, action) {
        // Called before a form (inline or normal) is opened. Called after initEdit.
        if (debug) console.log('preOpen', mode, action, e);

        // NOTE: editor.field('CAN_EDIT').val() is not filled at first select, so we use the datatable
        let modifier = editor.modifier();
        let data = table.row( modifier ).data();

        // No edits/deletes if CAN_EDIT field is false
        if (data && (typeof data !== "undefined") && !data.CAN_EDIT) {
            if (debug) console.log("No edits if CAN_EDIT field is false");
            return false;
        }

        // No edits possible if no username
        if ((data.USERNAME == '') && (action == 'edit')) {
            if (debug) console.log("No edits possible if no username");
            return false;
        }

        do_validate = false;
   } );

    let do_validate = false;

    function validate() {
        if (do_validate) {

            // Check for valid Task
            let task = editor.field('TASK.SHORT_TITLE');
            task.error(task.val() == "" ? SUPPLY_A_TASK_NAME : "");

            let category = editor.field('TASK.CATEGORY_CODE');
            category.error(category.val() == "" ? SELECT_A_CATEGORY : "");

            let location = editor.field('TASK.TASK_LOCATION_CODE');
            location.error(location.val() == "" ? SELECT_A_LOCATION : "");

            let system_id = editor.field('TASK.SYSTEM_ID');
            system_id.error(isNaN(parseInt(system_id.val())) ? SELECT_A_SYSTEM : "");

            let activity_id = editor.field('ACTIVITY.ID');
            activity_id.error(isNaN(parseInt(activity_id.val())) ? SELECT_AN_ACTIVITY : "");

            let wbs_id = editor.field('TASK.WBS_ID');
            wbs_id.error(isNaN(parseInt(wbs_id.val())) ? SELECT_A_WBS : "");
        }
    }

    editor.on('preSubmit', function(e, data, action) {
        if (debug) console.log('preSubmit', e, action, data);

        do_validate = true;

        // Validate fields before submitting to server

        // DELETE is ok
        if (action == "remove") {
            if (debug) console.log("DELETE is ok");
            return true;
        }

        let row = data.data[Object.keys(data.data)[0]];

        delete row.TASK.ID;
        delete row.ACTIVITY.ID;

        validate();

        // Any missing fields, abort submit
        if ( this.inError() ) {
            if (debug) console.log("Some error prevented submit for action: ", action);
            return false;
        }

        return true;
    });

    // handle server replies and errors
    editor.on('postSubmit', function (_e, json, data, action, xhr) {
        if (debug) console.log("postSubmit", action, json, data, xhr);

        if (json === null) {
            console.log(data);
            console.log('responseText:', xhr.responseText);
            alert("Server error in script: " + data_script + ", look at console log.");
        }

        update_roles(json.data, current_user, user_roles, table);

        //   if ( json.FieldErrors ) {
        //      .... take a look at json.fieldErrors
        //   }
    });

    // jump to new or edited record
    editor.on( 'submitComplete', function ( e, json, data, action ) {
        if (debug) console.log("submitComplete", data, action);
        if ( action === 'create' || action === 'edit' ) {
            if (is_test) {
                $(`#${data.DT_RowId}`).addClass("test_tasks_record");
            }
            table.row(`#${data.DT_RowId}`).scrollTo();
        }

        // Deselect original after copy
        if (duplicate) {
            table.rows().deselect();
        }
        duplicate = false;
    });

    editor.dependent( 'ACTIVITY.ID', 'wbs_data.php' );

    editor.dependent(['TASK.SHORT_TITLE', 'TASK.ARCHIVED', 'TASK.CATEGORY_CODE', 'TASK.TASK_LOCATION_CODE', 'TASK.SYSTEM_ID', 'TASK.WBS_ID', 'TASK.DESCRIPTION', 'TASK.COMMENTS'],
        function ( val, data, callback, e) {
            if (debug) console.log("Dependency called for any 'validateable' field update ", val, data, e);

            if ((data.values["TASK.SHORT_TITLE"] == '') ||
                (data.values["TASK.CATEGORY_CODE"] == '') ||
                (data.values["TASK.TASK_LOCATION_CODE"] == '') ||
                (data.values["TASK.SYSTEM_ID"] == '') ||
                (data.values["TASK.WBS_ID"] == '')) {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-green");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-red");
            } else {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-red");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-green");
            }

            validate();
            return {};
        }
    );

    // Activate an inline edit on click of a table cell
    // $('#table').on( 'click', 'tbody td:not(.select-checkbox)', function (e) {
    //     if (debug) console.log("Clicked any cell except select-checkbox", e);
    //     // Focus on the input in the cell that was clicked when Editor opens
    //     editor.one( 'open', () => {
    //         $('input', this).focus();
    //     });

    //     table.buttons( ['.buttons-create'] ).enable(false);
    //     editor.one('preClose', function(e) {
    //         if (debug) console.log('preClose', e);
    //         table.buttons( ['.buttons-create'] ).enable(has_role_admin(current_user, user_roles));
    //     });

    //     if (debug) console.log("Enable all editable cells");
    //     // Only enable the (databasetable) editable fields
    //     editor.inline(
    //         table.cells(this.parentNode, ".editable").nodes(), {
    //             onBlur: 'submit',
    //         }
    //     );
    // } );

    // Setup DataTable
    table = new DataTable('#table', {
        lengthChange: false,
        layout: {
             topStart: 'buttons',
             topEnd: 'search',
        //     top: 'searchPanes',
        //     topStart: null,
        //     topEnd: null,
        //     bottom: null,
             bottomStart: null,
        //     bottomEnd: null
        },
        ajax: {
            url: data_script,
            dataSrc: function ( json ) {
                current_user = json.user;

                // Update User Roles
                user_roles = decode_user_roles(json.options["TASK.ID"]);
                console.log("User Roles", user_roles);
                console.log("User", json.user, "has_admin_role", json.has_role_admin, "has_role_role_manager", json.has_role_role_manager);

                set_user(json.user, json.has_role_admin, json.has_role_role_manager, user_roles, table);
                update_roles(json.data, current_user, user_roles, table);

                let db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                // TBD
                allow_new = true; // json.options.TASK_ID.length > 0;
                allow_edit = allow_new;
                allow_duplicate = allow_new;
                allow_remove = allow_new;

                return json.data;
            }
        },
        fixedHeader: {
            header: true,
            footer: true
        },
        fixedColumns: {
            leftColumns: fixed_left_columns,
            rightColumns: fixed_right_columns
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        // switch off scroller and paging in case of a test to make sure it loads all records
        paging:         !is_test,
        scroller:       !is_test,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,    // NOTE: delete local storage for searchpanes to work first time...
        // fuzzySearch: {
        //     toggleSmart: true
        // },

        // for SearchPanes
        // columnDefs: [
        //     {
        //         searchPanes: {
        //             show: false,
        //         },
        //         targets: [0, 1, 2, 3, 4, 6, 11, 12, 13],
        //     },
        //     {
        //         searchPanes: {
        //             initCollapsed: true,
        //             cascadePanes: true,
        //             show: true,
        //         },
        //         targets: [5, 7, 8, 9, 10],
        //     }
        // ],
        // serverSide: true,

        columns: [
            {
                data: "CAN_EDIT",
                className: "can_edit",
                visible: false,
            },
            {
                data: "CAN_EDIT_BECAUSE",
                className: "can_edit_because",
                visible: false,
            },
            {
				data: null,
				defaultContent: '',
				className: 'select',
				orderable: true,
                searchable: false,
                render: function ( data, type, row ) {
                    // if (debug) console.log(type+" "+data+" "+row.CAN_EDIT);
                    return type == 'sort' ? row.CAN_EDIT : data;
                }
			},
            {
                data: "TASK.ID",
                className: 'task_id',
                visible: true
            },
            {
                data: "TASK.SHORT_TITLE",
                className: 'task'
            },
            {
                data: "TASK.CATEGORY_CODE",
                className: 'category_code',
                visible: false
            },
            {
                data: "CATEGORY.LABEL",
                className: 'category',
                editField: "TASK.CATEGORY_CODE"
            },
            {
                data: "TASK.ARCHIVED",
                className: 'archived dt-body-center',
                render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="checkbox-archived" disabled>';
                    }
                    return data;
                },
            },
            {
                data: "TASK.TASK_LOCATION_CODE",
                className: 'location_code',
                visible: false
            },
            {
                data: "LOCATION.LABEL",
                className: 'location',
                editField: "TASK.TASK_LOCATION_CODE"
            },
            {
                data: "TASK.SYSTEM_ID",
                className: 'system_id',
                visible: false
            },
            {
                data: "SYSTEM_NODE.TITLE",
                className: 'system'
            },
            {
                data: "ACTIVITY.ID",
                className: 'activity_id',
                visible: false
            },
            {
                data: "ACTIVITY.TITLE",
                className: 'activity'
            },
            {
                data: "TASK.WBS_ID",
                className: 'wbs_id',
                visible: false
            },
            {
                data: "WBS_NODE.TITLE",
                className: 'wbs'
            },
            {
                data: "TASK.DESCRIPTION",
                className: 'description'
            },
            {
                data: "TASK.COMMENTS",
                className: 'comments'
            },
        ],
        order: [[ ARCHIVED, 'asc' ], [ TASK_ID, 'asc' ]],
        select: {
            style:    'os',
            selector: '.select-enabled,.archived',
            blurable: true
        },
        // keys: {
        //     columns: ':not(:first-child)',
        //     keys: [ "\t".charCodeAt(0) ],    // or [ 9 ]
        //     editor: editor,
        //     editOnFocus: true
        // },

        // AutoFill and KeyTable
        // autoFill: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        // keys: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        buttons: [
            {
                extend: "create",
                editor: editor,
                formTitle: "Create New Role",
                formButtons: [
                    { text: 'Create', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            // { extend: "createInline", editor: editor, text: 'New (Inline)' },
            {
                extend: "edit",
                editor: editor,
                formTitle: "Edit Role",
                formButtons: [
                    { text: 'Edit', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: "selected",
                text: "Duplicate",
                className: 'buttons-duplicate',
                action: function ( e, dt, node, config ) {
                    // Start in edit mode, and then change to create
                    duplicate = true;
                    editor
                        .edit( table.rows( {selected: true} ).indexes(), {
                            title: 'Duplicate Role',
                            buttons: [
                                { text: 'Create from existing', className: 'submit', action: function () { this.submit(); } },
                                { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                            ]
                        } )
                        .mode( 'create' );
                },
            },
            {
                extend: "remove",
                editor: editor,
                formTitle: "Delete Role",
                formButtons: [
                    { text: 'Delete', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        rowCallback: function( row, data ) {
            if ( data.CAN_EDIT ) {
                $('td.select', row).addClass( 'select-checkbox' );
                $('td.select', row).addClass( 'select-enabled' );
            } else {
                $('td.select', row).removeClass( 'select-checkbox' );
                $('td.select', row).removeClass( 'select-enabled' );
            }

            // Set the checked state of the checkbox in the table
            $('input.checkbox-archived', row).prop( 'checked', data.TASK.ARCHIVED);
        },
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            let api = this.api();

            count_unique(api, CATEGORY);
            count_unique(api, LOCATION);
            count_unique(api, ACTIVITY);
            count_unique(api, WBS);
        }
    } );

    table.on( 'draw select deselect', function () {
        if (debug) console.log('draw select deselect');
        // TBD
        let enable_new = allow_new;
        let enable_edit = allow_edit;
        let enable_duplicate = allow_new;
        let enable_remove = allow_remove;

        let selectedRows = table.rows( { search: 'applied', selected: true } );

        enable_new = enable_new && (selectedRows.count() == 0);
        enable_edit = enable_edit && (selectedRows.count() == 1);
        enable_duplicate = enable_duplicate && (selectedRows.count() == 1);
        enable_remove = enable_remove && (selectedRows.count() > 0);

        // table.rows( { selected: true } ).every( function ( _rowIdx, _tableLoop, _rowLoop ) {
        //     enable_edit = enable_edit && (this.data().USERNAME != '');
        // });

        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> new enabled", enable_new);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> edit enabled", enable_edit);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> duplicate enabled", enable_duplicate);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> remove enabled", enable_remove);

        count_selected(table, selectedRows.count(), SELECT);

        table.buttons( ['.buttons-create'] ).enable(enable_new);
        table.buttons( ['.buttons-edit'] ).enable(enable_edit);
        table.buttons( ['.buttons-duplicate'] ).enable(enable_duplicate);
        table.buttons( ['.buttons-remove'] ).enable(enable_remove);
    });

    // table.scrollX = !is_test;
    table.paging = !is_test;
    table.scroller = !is_test;
    table.columns( [CAN_EDIT, CAN_EDIT_BECAUSE] ).visible( debug );


    // Display the buttons
    // bootstrap specific
    // new $.fn.dataTable.Buttons( table, [
    //     { extend: "create", editor: editor },
    //     // FIXME: disabled as we cannot disable 'datatable' type
    //     { extend: "edit",   editor: editor },
    //     { extend: "remove", editor: editor }
    // ] );

    // Bootstrap specific
    // table.buttons().container().appendTo( $('.col-md-6:eq(0)', table.table().container() ) );

 } );
