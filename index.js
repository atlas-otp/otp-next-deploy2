$(document).ready(function() {
    "use strict";

    var table = new DataTable('#table', {
      "ajax": {
        "url": "./index.json",
        "type": "POST",
        "dataSrc": ""
      },
      "paging": false,
      "info": false,
      // "scrollY": calcDataTableHeight(),
      "scrollCollapse": true,
      "columns": [
        { "data": "id" },
        { "data": "name" },
        { "data": "description" },
        { "data": "local" },
        { "data": "staging-dev" },
        { "data": "staging-test" },
        { "data": "staging-alpha" },
        { "data": "staging-beta" },
        { "data": "prod" },
      ],
      "columnDefs": [
        {
          "targets": [3,4,5,6,7,8],
          "render": function ( data, type, row, meta ) {
            if (type == "sort") {
              return data;
            }
            return data == undefined || data == "" ? "" : "<a href=\"" + data + "\">" + row.name + "</a>";
          }
        },
      ]
    });
  });
