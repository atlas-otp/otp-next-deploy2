<?php
/**
 * Oracle database driver for DataTables libraries.
 *
 * Note that this software uses the oci_* methods in PHP and NOT the Oracle PDO
 * driver, which is poorly supported.
 *
 *  @author    SpryMedia
 *  @copyright 2014 SpryMedia ( http://sprymedia.co.uk )
 *  @license   http://editor.datatables.net/license DataTables Editor
 *  @link      http://editor.datatables.net
 *
 * Modified to handle CERN's ATLAS OTP Database
 * - TNS connector, ignore host and port
 * - added procedure for update
 * - re-arranged "RETURNING" of ID
 */

namespace DataTables\Database\Driver;
if (!defined('DATATABLES')) exit();

use PDO;
use DataTables\Database\Query;
use DataTables\Database\Driver\OracleResult;


/**
 * Oracle driver for DataTables Database Query class
 *  @internal
 */
class OracleOTPQuery extends Query {
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Private properties
	 */
	private $_stmt;

	private $_editor_pkey_value;

	protected $_identifier_limiter = array( '"', '"' );

	protected $_field_quote = '"';

	protected $_supportsAsAlias = false;

	private $_debug_sql;


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Public methods
	 */

	static function connect( $user, $pass='', $host='', $port='', $db='', $dsn='' )
	{
		if ( is_array( $user ) ) {
			$opts = $user;
			$user = $opts['user'];
			$pass = $opts['pass'];
			$port = $opts['port'];
			$host = $opts['host'];
			$db   = $opts['db'];
			$dsn  = isset( $opts['dsn'] ) ? $opts['dsn'] : '';
		}

		if ( $port !== "" ) {
			$port = ":{$port}";
		}

		if ( ! is_callable( 'oci_connect' ) ) {
			echo json_encode( array(
				"error" => "oci methods are not available in this PHP install to connect to Oracle"
			) );
			exit(0);
		}

		$conn = @oci_connect($user, $pass, $db, 'utf8');

		if ( ! $conn ) {
			// If we can't establish a DB connection then we return a DataTables
			// error.
			$e = oci_error();

			echo json_encode( array(
				"error" => "An error occurred while connecting to the database ".
					"'{$db}'. The error reported by the server was: ".$e['message']
			) );
			exit(0);
		}

		// Use ISO date and time styles
		$stmt = oci_parse($conn,  "ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'" );
		$res = oci_execute( $stmt );

		$stmt = oci_parse($conn,  "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'" );
		$res = oci_execute( $stmt );

		return $conn;
	}


	public static function transaction ( $conn )
	{
		// no op
	}

	public static function commit ( $conn )
	{
		oci_commit( $conn );
	}

	public static function rollback ( $conn )
	{
		oci_rollback( $conn );
	}


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Protected methods
	 */

	protected function _prepare( $sql )
	{

		$resource = $this->database()->resource();
		$pkey = $this->pkey();
		$id_sql = null;

		// If insert, add the pkey column
		if ( $this->_type === 'insert' && $pkey ) {
			// NOT allowed for OTP Oracle as it uses procedure to CRUD.
			// $sql .= ' RETURNING '.$this->_protect_identifiers(is_array($pkey) ? $pkey[0] : $pkey).' INTO :editor_pkey_value';
			$id_sql = 'SELECT '
				.$this->_protect_identifiers(is_array($pkey) ? $pkey[0] : $pkey)
				.' FROM '.$this->_build_table()
				.' WHERE ';

			for ( $i=0 ; $i<count($this->_field) ; $i++ ) {
				if ($i > 0) $id_sql .= ' AND ';
				$id_sql .= $this->_protect_identifiers( $this->_field[$i] );
				if ($this->_bindings[$i]['value'] == null) {
					$id_sql .= ' IS NULL ';
				} else {
					$id_sql .= ' = '.' :'.$this->_safe_bind( $this->_field[$i] );
				}
			}
		}
		else if ( $this->_type === 'select' && $this->_oracle_offset !== null ) {
			$sql = '
				select *
				from ('.$sql.')
				where rownum > '.$this->_oracle_offset .' AND rownum <= '.($this->_oracle_offset+$this->_oracle_limit);
		}

		$this->database()->debugInfo( $sql, $this->_bindings );

		$this->_stmt = oci_parse( $resource, $sql );

		// Debug
		$this->_debug_sql = $sql;

		// If insert, add a binding for the returned id
		if ( $this->_type === 'insert' && $pkey ) {
			// oci_bind_by_name(
			// 	$this->_stmt,
			// 	':editor_pkey_value',
			// 	$this->_editor_pkey_value,
			// 	36
			// );
			$this->database()->debugInfo( $id_sql, $this->_id_bindings  );

			$this->_id_stmt = oci_parse( $resource, $id_sql );
		}

		// Bind values
		for ( $i=0 ; $i<count($this->_bindings) ; $i++ ) {
			$binding = $this->_bindings[$i];

			oci_bind_by_name(
				$this->_stmt,
				$binding['name'],
				$binding['value']
			);
		}

		// Bind values for returning ID
		if ( $this->_type === 'insert' && $pkey ) {
			for ( $i=0 ; $i<count($this->_id_bindings) ; $i++ ) {
				$binding = $this->_id_bindings[$i];
				if ($binding['value'] != null) {
					oci_bind_by_name(
						$this->_id_stmt,
						$binding['name'],
						$binding['value']
					);
				}
			}
		}
	}


	protected function _exec()
	{
		$resource = $this->database()->resource();

		$transaction_id = $this->database()->transaction_id();
		$transaction_type = $this->database()->transaction_type();
		$transaction_comment = $this->database()->transaction_comment();

		$needs_setup = !in_array($this->_type, array('select', 'raw'));

		if ( $needs_setup ) {
			// use "call" instead of "exec", and add "()" for void functions.
			// $stmt = oci_parse( $resource, "call generic.set_user(999980)" );
			$stmt = oci_parse( $resource, "call generic.set_user(".$transaction_id.")" );
			$res = @oci_execute( $stmt, OCI_NO_AUTO_COMMIT );
			if ( ! $res ) {
				$e = oci_error( $stmt );
				throw new \Exception( "Oracle SQL error: ".$e['message'] );

				return false;
			}

			$stmt = oci_parse( $resource, "call transaction.run('Y')" );
			$res = @oci_execute( $stmt, OCI_NO_AUTO_COMMIT );
			if ( ! $res ) {
				$e = oci_error( $stmt );
				throw new \Exception( "Oracle SQL error: ".$e['message'] );

				return false;
			}

			$stmt = oci_parse( $resource, "call transaction.set_comments('".$transaction_comment."')" );
			$res = @oci_execute( $stmt, OCI_NO_AUTO_COMMIT );
			if ( ! $res ) {
				$e = oci_error( $stmt );
				throw new \Exception( "Oracle SQL error: ".$e['message'] );

				return false;
			}

			// 1	System Transaction
			// 2	Task Transaction
			// 3	Booking Period Transaction
			// 4	Shift Booking Transaction
			// 5	Admin
			$stmt = oci_parse( $resource, "call transaction.set_transaction_type(".$transaction_type.")" );
			$res = @oci_execute( $stmt, OCI_NO_AUTO_COMMIT );
			if ( ! $res ) {
				$e = oci_error( $stmt );
				throw new \Exception( "Oracle SQL error: ".$e['message'] );

				return false;
			}
		}

		$res = @oci_execute( $this->_stmt, OCI_NO_AUTO_COMMIT );

		if ( ! $res ) {
			$e = oci_error( $this->_stmt );
			print("<pre>\n");
			print("SQL:\n\n");
			print("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS';\n");
			print($this->_debug_sql.";\n\n" );
			throw new \Exception( "Oracle SQL error: ".$e['message'] );

			return false;
		}

		$res = oci_commit( $resource );

		if ( $needs_setup ) {
			$stmt = oci_parse( $resource, "call transaction.stop()" );
			$res = @oci_execute( $stmt, OCI_NO_AUTO_COMMIT );
			if ( ! $res ) {
				$e = oci_error( $stmt );
				throw new \Exception( "Oracle SQL error: ".$e['message'] );

				return false;
			}
		}

		// Retrieve ID
		$pkey = $this->pkey();
		if ( $this->_type === 'insert' && $pkey ) {
			oci_execute( $this->_id_stmt, OCI_NO_AUTO_COMMIT );
			$row = oci_fetch_array($this->_id_stmt, OCI_NUM);
			if ( ! $row) {
				$e = oci_error( $this->_id_stmt );
				throw new \Exception( "Oracle SQL error: ".$e['message'] );

				return false;
			}
			$this->_editor_pkey_value = $row[0];
		}

		return new OracleResult( $resource, $this->_stmt, $this->_editor_pkey_value );
	}


	protected function _build_table()
	{
		$out = array();

		for ( $i=0, $ien=count($this->_table) ; $i<$ien ; $i++ ) {
			$t = $this->_table[$i];

			if ( strpos($t, ' as ') ) {
				$a = explode( ' as ', $t );
				$out[] = $this->_protect_identifiers($a[0]).' '.$this->_protect_identifiers($a[1]);
			}
			else {
				$out[] = $t;
			}
		}

		return ' '.implode(', ', $out).' ';
	}


	private $_oracle_offset = null;
	private $_oracle_limit = null;

	protected function _build_limit()
	{
		$this->_oracle_offset = $this->_offset;
		$this->_oracle_limit = $this->_limit;

		return ' ';
	}
}
