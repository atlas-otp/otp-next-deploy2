<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->

<?php
error_reporting(E_ALL & ~E_STRICT);
ini_set("display_errors", "1");
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;
?>


<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../');

require("config.php");

require('vendor/autoload.php');

require_once('otp-photos/PhotoUrlSigner.php');

use Unleash\Client\UnleashBuilder;

function flag_is_enabled(string $flag, string $env = null): bool {
    global $ff_details;

    if ($flag == '') {
        return true;
    }

    if (!$env) {
        $env = $ff_details["env"];
    }

    $instance_id = $ff_details["instance_id"];
    $app_url = $ff_details["app_url"];

    $unleash = UnleashBuilder::create()
    ->withAppName($env)
    ->withInstanceId($instance_id)
    ->withAppUrl($app_url)
    ->withCacheTimeToLive(0)    // remove for single env
    ->build();

    $feature = $env.'-'.$flag;
    return $unleash->isEnabled($feature);
}

function get_picture_url(string $user_id): ?string {
    global $db;
    global $photos_details;

    $ais_id = NULL;

    $rows = $db
        ->select( 'PUB_PERSON', 'AIS_PERSON_ID', ['ID' => $user_id, 'PHOTO' => 1], ['AIS_PERSON_ID'] )
        ->fetchAll();

    if (array_key_exists(0, $rows) &&
        array_key_exists('AIS_PERSON_ID', $rows[0])) {
        $ais_id = $rows[0]['AIS_PERSON_ID'];
    } else {
        $ais_id = NULL;
    }

    $picture_url = NULL;
    if ($ais_id) {
        $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

        $expiration = (new DateTime())->modify('10 mins');

        $server = $photos_details["gateway"];

        $picture_url = $server . "?ais_id=" . $ais_id;
        $picture_url = $urlSigner->sign($picture_url, $expiration);
    }

    return $picture_url;
}
?>

<?php
if (!flag_is_enabled('')) {
	header("HTTP/1.0 501 Not Implemented");
    die("ERROR: Feature '' is disabled\n");
}
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no"> -->
<meta name="description" content="OTP Next, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href=".//ico/atlas-144x144.png">
<link rel="shortcut icon" type="image/ico" href=".//ico/atlas.ico">

<title>OTP Deployments</title>

<!-- CSS -->

<!-- default -->
<link rel="stylesheet" type="text/css" href=".//assets/datatables.min.css">


<link rel="stylesheet" type="text/css" href=".//assets/select2.css">


<link rel="stylesheet" type="text/css" href=".//editor/css/editor.dataTables.css">


<link rel="stylesheet" type="text/css" href=".//css/otp-next.css">



<!-- JAVASCRIPT -->


<script type="text/javascript" language="javascript" src=".//assets/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src=".//assets/vfs_fonts.js"></script>


<script type="text/javascript" language="javascript" src=".//assets/datatables.js"></script>


<script type="text/javascript" language="javascript" src=".//assets/jquery.mask.js"></script>

<script type="text/javascript" language="javascript" src=".//assets/select2.js"></script>

<!-- <script type="text/javascript" language="javascript" src=".//assets/dataTables.fuzzySearch.js"></script> -->

<!-- Editor -->
<script type="text/javascript" language="javascript" src=".//editor/js/dataTables.editor.js"></script>



<script type="text/javascript" language="javascript" src=".//editor/js/editor.select2.js"></script>
<script type="text/javascript" language="javascript" src=".//editor/js/editor.mask.js"></script>
<script type="text/javascript" language="javascript" src=".//editor/js/editor.display.js"></script>

<script type="text/javascript" language="javascript" src=".//js/editor.image.js"></script>

<script type="text/javascript" language="javascript" src=".//js/otp-next.js"></script>

<script type="text/javascript" language="javascript" src="index.js"></script>





        
        
    </head>

    <body>
        <div id="table_header" class="card border-secondary">
	<h5 id="table_title" class="card-header text-white bg-secondary">
		OTP Deployments
		<span class="js-example-basic-single label float-left" id="selectors"></span>
		<a class="label label-blue btn float-right" href="https://gitlab.cern.ch/atlas-otp/otp-next/-/tree/master/src" target="otp-next" role="button" class="btn btn-primary btn-sm float-end">
		Version: <?php include './/version.txt'?>
		</a>
		<span class="label float-right" id="table_db">None</span>
		
		<select class="js-example-basic-single label float-right" name="state" id="table_user"></select>
	</h5>
	<div class="card-body">
        <table id="table" class="display table table-striped" width="100%" style="width:100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>local</th>
			<th>staging-dev</th>
			<th>staging-test</th>
			<th>staging-alpha</th>
			<th>staging-beta</th>
            <th>production</th>
		</tr>
	</thead>
</table>

    </div>
</div>


        <!-- footer.html -->
Copyright&copy; 2021-2025 CERN


        
        
    </body>
</html>
