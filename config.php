<?php

// if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Database user / pass
 *
 * Example for Test Database, just needs password
 */
$sql_details = array(
	"type" => NULL,         // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => "",           // Database password (TBS)
	"host" => NULL, 		// Database host
	"port" => NULL,         // Database connection port (can be left empty for default)
	"db"   => NULL,         // Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "",           // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   	// PHP PDO attributes array. See the PHP documentation for all options
);

$ff_details = array(
    "token" => NULL,
    "project_id" => NULL,
    "instance_id" => NULL,
    "env" => NULL,
    "app_url" => NULL
);

$photos_details = array(
    "gateway" => "",            // https://otp-photos-atlas-dev.web.cern.ch/
	"secret" => ""
);

$upload_token = null;
$upload_branch = null;

$upload_prediction_token = null;
$upload_prediction_branch = null;

$local_config = 'config-local.php';
$path = stream_resolve_include_path($local_config);
if ($path && file_exists($path)) {
    include($local_config);
}

$sql_details["type"] = getenv("ORACLE_DB_TYPE") ?: $sql_details["type"];
if (!$sql_details["type"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_TYPE not defined\n");
}

$sql_details["user"] = getenv("ORACLE_DB_USER") ?: $sql_details["user"];
if (!$sql_details["user"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_USER not defined\n");
}

$sql_details["pass"] = getenv("ORACLE_DB_PASSWORD") ?: $sql_details["pass"];
if (!$sql_details["pass"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_PASSWORD not defined\n");
}

$sql_details["db"] = getenv("ORACLE_DB") ?: $sql_details["db"];
if (!$sql_details["db"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB not defined\n");
}

$ff_details["token"] = getenv("FF_TOKEN") ?: $ff_details["token"];
if (!$ff_details["token"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("FF_TOKEN not defined\n");
}

$ff_details["project_id"] = getenv("FF_PROJECT_ID") ?: $ff_details["project_id"];
if (!$ff_details["project_id"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("FF_PROJECT_ID not defined\n");
}

$ff_details["instance_id"] = getenv("FF_INSTANCE_ID") ?: $ff_details["instance_id"];
if (!$ff_details["instance_id"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("FF_INSTANCE_ID not defined\n");
}

$ff_details["env"] = getenv("FF_ENV") ?: $ff_details["env"];
if (!$ff_details["env"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("FF_ENV not defined\n");
}

$ff_details["app_url"] = getenv("FF_APP_URL") ?: $ff_details["app_url"];
if (!$ff_details["app_url"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("FF_APP_URL not defined\n");
}

$photos_details["gateway"] = getenv("PHOTOS_GATEWAY") ?: $photos_details["gateway"];
if (!$photos_details["gateway"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("PHOTOS_GATEWAY not defined\n");
}

$photos_details["secret"] = getenv("PHOTOS_SECRET") ?: $photos_details["secret"];
if (!$photos_details["secret"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("PHOTOS_SECRET not defined\n");
}

$upload_token = getenv("UPLOAD_TOKEN") ?: $upload_token;
if (!$upload_token) {
    header("HTTP/1.0 502 Bad Gateway");
    die("UPLOAD_TOKEN not defined\n");
}

$upload_branch = getenv("UPLOAD_BRANCH") ?: $upload_branch;
if (!$upload_branch) {
    header("HTTP/1.0 502 Bad Gateway");
    die("UPLOAD_BRANCH not defined\n");
}

$upload_prediction_token = getenv("UPLOAD_PREDICTION_TOKEN") ?: $upload_prediction_token;
if (!$upload_prediction_token) {
    header("HTTP/1.0 502 Bad Gateway");
    die("UPLOAD_PREDICTION_TOKEN not defined\n");
}

$upload_prediction_branch = getenv("UPLOAD_PREDICTION_BRANCH") ?: $upload_prediction_branch;
if (!$upload_prediction_branch) {
    header("HTTP/1.0 502 Bad Gateway");
    die("UPLOAD_PREDICTION_BRANCH not defined\n");
}
