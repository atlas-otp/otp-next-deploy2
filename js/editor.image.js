// Image field type plug-in code
(function ($, DataTable) {

    if ( ! DataTable.ext.editorFields ) {
        DataTable.ext.editorFields = {};
    }

    var Editor = DataTable.Editor;
    var _fieldTypes = DataTable.ext.editorFields;

    _fieldTypes.image = {
        create: function ( conf ) {
            // console.log("Create", conf);

            conf._enabled = true;

            // Create the elements to use for the input
            conf._input = $(
                '<div id="'+Editor.safeId( conf.id )+'">'+
                    '<img src="'+'../img/no_photo.png'+'"/ width="60px" height="75px">'+
                '</div>');

            return conf._input;
        },

        // fake the get and set
        get: function ( conf ) {
            // console.log("Get", conf);
            // return $('img', conf._input).attr( 'src'
            return conf._src;
        },

        // fake the get and set
        set: function ( conf, val ) {
            // console.log("Set", conf, val);
            // console.log($('img', conf._input).attr( 'src'));
            // $('img', conf._input).attr( 'src', val );
            conf._src = val;
        },

        enable: function ( conf ) {
            // console.log("Enable", conf);
            conf._enabled = true;
            $(conf._input).removeClass( 'disabled' );
        },

        disable: function ( conf ) {
            // console.log("Disable", conf);
            conf._enabled = false;
            $(conf._input).addClass( 'disabled' );
        }
    };

})(jQuery, jQuery.fn.dataTable);
