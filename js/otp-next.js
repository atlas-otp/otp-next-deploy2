/*global navigator, DataTable, jQuery, console, alert */

function database(name) {
    // Which Database
    let text = '';
    let clazz = '';
    switch(name) {
        case "devdb19":
            text = 'Dev DB: ';
            clazz = 'label-red';
            break;
        case "atlas_otp":
            text = 'Prod DB: ';
            clazz = 'label-green';
            break;
        case "atlr":
            text = 'Prod DB: ';
            clazz = 'label-green';
            break;
        case "int8r":
            text = 'Test DB: ';
            clazz = 'label-orange';
            break;
        case "atlas_otp_local":
            text = 'Local DB: ';
            clazz = 'label-yellow';
            break;
        default:
            text = 'Unknown DB: ';
            clazz = 'label-blue';
            break;
    }
    text = text + name;

    return {
        text: text,
        clazz: clazz
    };
}

function update_roles(data, user, user_roles, table) {
    // Add authorization fields
    add_can_edit(data, user, user_roles);
    table.rows().invalidate();
    table.draw(false);

    // console.log('changed to user', user, user_roles[user].has_role_admin);
}

function set_user(user, has_role_admin, has_role_role_manager, user_roles, table) {
    let option;

    // add prived users
    for (let  username in user_roles) {
        option = $("<option>").val(username).html(user_roles[username].lname + ", " + user_roles[username].fname + " (" + username  + ")" + (user_roles[username].has_role_admin ? " - Admin" : "") + (user_roles[username].has_role_role_manager ? " - Role Manager" : ""));
        if (username == user) {
            option.attr("selected", "selected");
            $('#table_user').append(option);
        }
        if (has_role_admin) {
            $('#table_user').append(option);
        }
    }

    // add non-priv user
    if (!(user in user_roles)) {
        option = $("<option>").val(user).html("(" + user  + ")");
        option.attr("selected", "selected");
        $('#table_user').append(option);
    }

    $('#table_user').trigger('change');
    $('#table_user').addClass( has_role_admin ? 'label-orange' : has_role_role_manager ? 'label-yellow' : 'label-green' );
    $('#table_user').on( "change", function() {
        let  user = $('#table_user')[0].value;
        update_roles(table.data(), user, user_roles, table);
    });
}

function set_change_handler(editor, name, mode, debug) {
    editor.field(name).input().off('change');
    if (mode === 'inline') {
        // Final select on name should submit
        editor.field(name).input().on('change', function(e, data) {
            if (!data || !data.editor) {
                if (debug) console.log("Select Submit on final", name);
                editor.submit();
            }
        } );
    }
}

const activity_ids = [1, 2, 3, 6, 24];
// user_roles: dictionary by USERNAME of info and roles array [{role: ROLE_WBS_MANAGER, system_id: 3, activity_id: 2 wbs_id: 14}]
//
// Check to see if there are any roles that can edit this entry of system 3, activity 2 or wbs 14.
//
// system_id: system id to be checked for
// activity_id: activity id be checked for
// wbs_id: wbs to be checked for (in conjunction with activity)
function can_edit(username, user_roles, system_id, activity_id, wbs_id) {

    if (username in user_roles) {
        let roles = user_roles[username].roles;

        for (let i = 0; i < roles.length; i++) {
            switch(roles[i].role) {
                case "ROLE_ADMIN":
                    return {can_edit: true, can_edit_because: 'ROLE_ADMIN'};
                case "ROLE_ROLE_MANAGER":
                    return {can_edit: true, can_edit_because: 'ROLE_ROLE_MANAGER'};
                case "ROLE_ACTIVITY_MANAGER":
                    if (activity_ids.includes(roles[i].activity_id) &&
                        activity_ids.includes(activity_id) &&
                        activity_id &&
                        activity_id == roles[i].activity_id) {
                        return {can_edit: true, can_edit_because: 'ROLE_ACTIVITY_MANAGER'};
                    }
                    break;
                case "ROLE_SYSTEM_MANAGER":
                    if (system_id &&
                        system_id == roles[i].system_id) {
                        return {can_edit: true, can_edit_because: 'ROLE_SYSTEM_MANAGER'};
                    }
                    break;
                case "ROLE_WBS_MANAGER":
                    if (!activity_ids.includes(roles[i].wbs_id) &&
                        !activity_ids.includes(wbs_id) &&
                        wbs_id &&
                        wbs_id == roles[i].wbs_id) {
                        return {can_edit: true, can_edit_because: 'ROLE_WBS_MANAGER'};
                    }
                    break;
                default:
                    break;
            }
        }
    }
    return {can_edit: false, can_edit_because: ''};
}

function has_role_admin(username, user_roles) {
    if (username in user_roles) {
        return user_roles[username].has_role_admin;
    }
    return false;
}

function has_role_role_manager(username, user_roles) {
    if (username in user_roles) {
        return user_roles[username].has_role_role_manager;
    }
    return false;
}

// return a dictionary by USERNAME with info and array of roles
function decode_user_roles(json_array) {
    if (json_array == undefined) {
        console.log("Please call 'decode_user_roles' with a valid fieldname (e.g. json.options.ID or json.options[\"TASK.ID\"])");
    }

    let user_roles = {};
    for (let i = 0; i < json_array.length; i++) {
        // format: USERNAME:LNAME:FNAME:ROLE:SYSTEM_ID:ACTIVITY_ID:WBS_ID
        let parts = json_array[i].label.split(':', 7);
        let username = parts[0];
        let role = parts[3];
        if (!(username in user_roles)) {
            user_roles[username] = {};
            user_roles[username].roles = [];
            user_roles[username].has_role_admin = false;
            user_roles[username].has_role_role_manager = false;
        }
        user_roles[username].lname = parts[1];
        user_roles[username].fname = parts[2];
        user_roles[username].has_role_admin = user_roles[username].has_role_admin || (role == 'ROLE_ADMIN');
        user_roles[username].has_role_role_manager = user_roles[username].has_role_role_manager || (role == 'ROLE_ROLE_MANAGER');
        user_roles[username].roles.push({
            role: role,
            system_id: parseInt(parts[4]),
            activity_id: parseInt(parts[5]),
            wbs_id: parseInt(parts[6])
        });
    }

    return user_roles;
}

// function enable(selector) {
//     $(selector).attr("disabled", false);
//     $(selector).removeClass("disabled");
// }

// function disable(selector) {
//     $(selector).attr("disabled", true);
//     $(selector).addClass("disabled");
// }

function count_unique(api, column) {
    let n = api
    .column( column, { search:'applied' } )
    .data()
    .reduce( function (a, b) {
        if (!a) {
            a = new Set();
        }
        a.add(b);
        return a;
    }, 0 );
    $( api.column( column ).footer() ).html(n ? n.size : 0);
    return n.size;
}

function value( i ) {
    return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
            i : 0;
}


function sum(api, column) {
    let s = api
    .column( column, { filter:'applied' } )
    .data()
    .reduce( function (a, b) {
        return a + b;
    }, 0 );
    $( api.column( column, { filter:'applied' }  ).footer() ).html(s.toFixed(2));
    return s;
}

function sum100(api, column) {
    let s = api
    .column( column, { search:'applied' } )
    .data()
    .reduce( function (a, b) {
        return a + b/100;
    }, 0 );
    $( api.column( column ).footer() ).html(s.toFixed(2));
    return s;
}

function count_selected(api, selected, column) {
    let entries = api
    .column( column, { search:'applied' } )
    .data()
    .reduce( function (a, b) {
        if (!a) {
            a = 0;
        }
        return a + 1;
    }, 0 );
    $( api.column( column ).footer() ).html(`${selected}&nbsp;of&nbsp;${entries}`);
    return entries;
}

function set_enabled(field, state) {
    if (state) {
        field.enable();
    } else {
        field.disable();
    }
}

function set_show(field, state) {
    if (state) {
        field.show();
        field.enable();
    } else {
        field.hide();
        field.disable();
    }
}

function get_option_key(db, key) {
    console.log(db);
    return db === 'atlas_otp_local' ? value(key) : String(key);
}

function set_field(db, editor, fieldname, new_value, selected_fieldname, inline) {
    if (new_value == null && !inline) {
        set_show(editor.field(fieldname), true);
        set_show(editor.field(selected_fieldname), false);
    } else {
        set_show(editor.field(fieldname), false);
        set_enabled(editor.field(fieldname), false);
        editor.field(fieldname).val(get_option_key(db, new_value));
        set_show(editor.field(selected_fieldname), true);
        editor.field(selected_fieldname).val(new_value);
    }
}

function copy_to_clipboard(anchor) {
    console.log(anchor);
    // Get the text field
    let href = document.getElementById(anchor).href;

     // Copy the text inside the text field
    navigator.clipboard.writeText(href);

    // Alert the copied text
    alert("Copied the link: " + href);
}
