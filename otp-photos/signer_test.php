<?php

require_once('PhotoUrlSigner.php');

if (count($argv) < 4) {
    echo "Usage signer_test.php secret expiry url\n";
    exit(1);
}

$urlSigner = new PhotoUrlSigner($argv[1]);
$url = $urlSigner->sign($argv[3], intval($argv[2]));
print($url ."\n");
print($urlSigner->validate($url) . "\n");
?>
