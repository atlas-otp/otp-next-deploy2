import java.net.URI;

public class UrlSignerTest {

    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.out.println("Usage: UrlSignerTest secret expiry url");
            System.exit(1);
        }

        System.out.println(args[0]);
        UrlSigner urlSigner = new UrlSigner(args[0]);

        URI uri = urlSigner.sign(args[2], Integer.parseInt(args[1]));
        System.out.println(uri);
        System.out.println(urlSigner.validate(uri));
    }
}
