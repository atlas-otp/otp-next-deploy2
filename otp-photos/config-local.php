<?php

// if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Database user / pass
 *
 * Example for Test Database, just needs password
 */
// $sql_details = array(
// 	"type" => "Mysql",     		// Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
// 	"user" => "root",   		// Database user name
// 	"pass" => "root",          			// Database password (TBS)
// 	"host" => "127.0.0.1", 		   		// Database host
// 	"port" => "8889",          		// Database connection port (can be left empty for default)
// 	"db"   => "atlas_otp",          	// Database name (dev: devdb19; test: int8r; prod: atlr)
// 	"dsn"  => "charset=utf8mb4",          			// PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
// 	"pdoAttr" => array()   			// PHP PDO attributes array. See the PHP documentation for all options
// );
$sql_details = array(
	"type" => "Mysql",     		// Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "root",   		// Database user name
	"pass" => "root", // "otp-next-for-fun",          			// Database password (TBS)
	"host" => "127.0.0.1", 		   		// Database host
	"port" => "3306",          		// Database connection port (can be left empty for default)
	"db"   => "atlas_otp",          	// Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "charset=utf8mb4",          			// PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   			// PHP PDO attributes array. See the PHP documentation for all options
);

$photos_details = array(
    "url" => "",
    "login" => "otpphoto",
	"password" => "",
	"secret" => ""
);
