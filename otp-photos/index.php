<?php

$photos_details = array(
    "url" => "",
    "login" => "",
    "password" => "",
    "secret" => ""
);

$sql_details = array(
    "db"   => "",
	"type" => "",           // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => ""            // Database password (TBS)
);

require_once('config.php');
require_once('PhotoUrlSigner.php');

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * @param array<string, string> $s
 */
function url_origin( array $s ): string {
    $protocol = $s['HTTP_X_FORWARDED_PROTO'];
    $ssl      = ( $protocol == 'https' );
    $port     = $s['HTTP_X_FORWARDED_PORT'];
    $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
    $host     = $s['HTTP_X_FORWARDED_HOST'];
    return $protocol . '://' . $host . $port;
}

/**
 * @param array<string, string> $s
 */
function full_url( array $s ): string{
    return url_origin( $s ) . $s['REQUEST_URI'];
}

function error(string $msg, bool $show_message): string {
    if ($show_message) {
        header("HTTP/1.0 502 Bad Gateway");
        return $msg;
    }
    header("Content-Type: image/png");
    $no_photo = file_get_contents('no-photo.png');
    return $no_photo ? $no_photo : "";
}

$t0 = microtime(true);

// Get input
parse_str($_SERVER['QUERY_STRING'], $query_items);
$debug = array_key_exists('debug', $query_items);
$time = array_key_exists('time', $query_items);

if ($time) {
    header("Content-Type: text/plain");
}

// Check for oci_connect
if (!function_exists('oci_connect')) {
    die(error("Function oci_connect not defined", $debug));
}

// Check input
$ais_id = $query_items['ais_id'] ?? null;
if ($ais_id == null) {
    die(error("No ais_id supplied", $debug));
}

if (filter_var($ais_id, FILTER_VALIDATE_INT) === FALSE) {
    die(error("Given ais_id is not an integer", $debug));
}

$ais_id = strval($ais_id);

// Check signature
$url = full_url($_SERVER);

if ($time) {
    $t1 = microtime(true);
    $td = $t1 - $t0;
    $t0 = $t1;
    echo "Checks: $td s\n";
}

$urlSigner = new PhotoUrlSigner($photos_details["secret"]);

if (! $urlSigner->validate($url)) {
    die(error("Invalid signature", $debug));
}

if ($time) {
    $t1 = microtime(true);
    $td = $t1 - $t0;
    $t0 = $t1;
    echo "Validate: $td s\n";
}

$cache = new FilesystemAdapter();

$photo_ok = $cache->get($ais_id, function (ItemInterface $item) {
    global $ais_id, $sql_details, $debug;

    $item->expiresAfter(3600);
    // Check with DB
    $otp_connection = oci_connect($sql_details["user"], $sql_details["pass"], $sql_details["db"]);
    if (!$otp_connection) {
        $m = oci_error();
        if ($m) {
            die(error($m['message'], $debug));
        }
        die(error('No database connection', $debug));
    }

    $sql = 'SELECT * FROM PUB_PERSON WHERE AIS_PERSON_ID=' . $ais_id;
    $stid = oci_parse($otp_connection, $sql);
    if (!$stid) {
        die(error("Invalid sql: " . $sql, $debug));
    }

    oci_execute($stid);

    $row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);

    if (!$row) {
        die(error('Non existing ais_id', $debug));
    }

    return $row["PHOTO"] == 1;
});

if (!$photo_ok) {
    die(error('Photo cannot be shown', $debug));
}

if ($time) {
    $t1 = microtime(true);
    $td = $t1 - $t0;
    $t0 = $t1;
    echo "Database: $td s\n";
}

// login and retrieve picture
$login = $photos_details["login"];
$password = $photos_details["password"];
$person_url = $photos_details["url"] . $ais_id;

$result = $cache->get($person_url, function (ItemInterface $item) {
    global $person_url, $login, $password, $debug;

    $item->expiresAfter(24*3600);

    $ch = curl_init();
    // Disable SSL
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    curl_setopt($ch, CURLOPT_URL, $person_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
    $computed_result = curl_exec($ch);

    if($computed_result === false) {
        die(error('Curl error: ' . curl_error($ch), $debug));
    } else {
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($debug) {
            header("HTTP/1.0 200 OK");
            die("Status Code: " . $httpcode);
        }
    }

    curl_close($ch);

    return $computed_result;
});

if ($debug) {
    header("HTTP/1.0 200 OK");
    die("Status Code: Cache Hit");
}

if (!$time) {
    header("Content-Type: image/jpeg");
    echo(strval($result));
}

if ($time) {
    $t1 = microtime(true);
    $td = $t1 - $t0;
    $t0 = $t1;
    echo "Retrieve: $td s\n";
}
?>
