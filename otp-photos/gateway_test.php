<?php

require_once('PhotoUrlSigner.php');

if (count($argv) < 4) {
    echo "Usage gateway_test.php acr_clients photo_url photo_url_acr\n";
    exit(1);
}

$urlSigner = new PhotoUrlSigner("dummy");
$gateway = $urlSigner->get_gateway($argv[1], $argv[2], $argv[3]);
print($gateway ."\n");
?>
