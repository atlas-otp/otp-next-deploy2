<?php
    $photos_details = array(
        "url" => NULL,
        "login" => NULL,
        "password" => NULL,
        "secret" => ""
    );

    require_once('config.php');
    require_once('PhotoUrlSigner.php');

    $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

    $expiration = (new DateTime())->modify('1 mins');

    $server = "/?ais_id=";
    $mark = $server . 388206;
    $mark_debug = $mark . "&debug";
    $mark_time = $mark . "&time";
    $mark_signed = $urlSigner->sign($mark, $expiration);
    $mark_signed_debug = $urlSigner->sign($mark_debug, $expiration);
    $mark_signed_time = $urlSigner->sign($mark_time, $expiration);

    $mark_signed_no_expiry = $urlSigner->sign($mark, 0);
    $mark_signed_no_expiry_debug = $urlSigner->sign($mark_debug, 0);
    $mark_signed_no_expiry_time = $urlSigner->sign($mark_time, 0);

    $no_consent = $server . 384543;
    $no_consent_signed = $urlSigner->sign($no_consent, $expiration);
    $no_consent_signed_debug = $no_consent_signed . "&debug";
    $no_consent_signed_time = $no_consent_signed . "&time";

    $not_in_atlas = $server . 674082;
    $not_in_atlas_signed = $urlSigner->sign($not_in_atlas, $expiration);
    $not_in_atlas_signed_debug = $not_in_atlas_signed . "&debug";
    $not_in_atlas_signed_time = $not_in_atlas_signed . "&time";

    $invalid_ais_id = $server . 123456;
    $invalid_ais_id_signed = $urlSigner->sign($invalid_ais_id, $expiration);
    $invalid_ais_id_signed_debug = $invalid_ais_id_signed . "&debug";
    $invalid_ais_id_signed_time = $invalid_ais_id_signed . "&time";
?>

<h1>OTP Photo Examples</h1>
<table>
    <tr>
        <td>Plain</td>
        <td><?= "<a href=\"$mark_signed\"><img width=\"200px\" src=\"$mark_signed\"></a>"?></td>
        <td><?= "<a href=\"$mark_signed_no_expiry\"><img width=\"200px\" src=\"$mark_signed_no_expiry\"></a>"?></td>
        <td><?= "<img width=\"200px\" src=\"$mark\">"?></td>
        <td><?= "<img width=\"200px\" src=\"${mark_signed}x\">"?></td>
        <td><?= "<img width=\"200px\" src=\"$no_consent_signed\">"?></td>
        <td><?= "<img width=\"200px\" src=\"$not_in_atlas_signed\">"?></td>
        <td><?= "<img width=\"200px\" src=\"$invalid_ais_id_signed\">"?></td>
    </tr>
    <tr>
        <td>Type</td>
        <td>Signed Expires in 1 min</td>
        <td>Signed No Expiry</td>
        <td>Unsigned</td>
        <td>Tampered</td>
        <td>No Consent</td>
        <td>Not in Database (ATLAS)</td>
        <td>Invalid ais_id</td>
    </tr>
<!--
    <tr>
        <td>URL</td>
        <td><?= "$mark_signed" ?></td>
        <td><?= "$mark_signed_no_expiry" ?></td>
        <td><?= "$mark" ?></td>
        <td><?= "${mark_signed}x" ?></td>
        <td><?= "$no_consent_signed" ?></td>
        <td><?= "$not_in_atlas_signed" ?></td>
        <td><?= "$invalid_ais_id_signed" ?></td>
    </tr>
-->
<tr>
        <td>Debug</td>
        <td><?= "<iframe width=\"200px\" src=\"$mark_signed_debug\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$mark_signed_no_expiry_debug\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$mark_debug\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"${mark_signed_debug}x\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$no_consent_signed_debug\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$not_in_atlas_signed_debug\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$invalid_ais_id_signed_debug\" seamless></iframe>"?></td>
    </tr>
    <tr>
        <td>Time</td>
        <td><?= "<iframe width=\"200px\" src=\"$mark_signed_time\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$mark_signed_no_expiry_time\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$mark_time\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"${mark_signed_time}x\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$no_consent_signed_time\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$not_in_atlas_signed_time\" seamless></iframe>"?></td>
        <td><?= "<iframe width=\"200px\" src=\"$invalid_ais_id_signed_time\" seamless></iframe>"?></td>
    </tr>
</table>
