<?php

require_once('vendor/autoload.php');

use Spatie\UrlSigner\MD5UrlSigner;
use Spatie\UrlSigner\Exceptions\InvalidExpiration;
use League\Uri\QueryString;
use League\Uri\Uri;

class PhotoUrlSigner extends MD5UrlSigner
{
    /**
     * @param string $signatureKey
     */
    public function __construct($signatureKey) {
        parent::__construct($signatureKey);
    }

    /**
     * Generate a token to identify the secure action.
     * debug and time parameter are not used in signature
     *
     * @param string $url
     * @param string $expiration
     *
     * @return string
     */
    protected function createSignature($url, $expiration)
    {
        $uri = Uri::createFromString($url);

        $intendedQuery = QueryString::extract($uri->getQuery());

        unset($intendedQuery['debug']);
        unset($intendedQuery['time']);

        $queryString = $this->buildQueryStringFromArray($intendedQuery);
        // echo $queryString;
        // $url = (string) $uri->withQuery($queryString ?? '');

        // print("{$queryString}::{$expiration}::{$this->signatureKey}\n");
        return md5("{$queryString}::{$expiration}::{$this->signatureKey}");
    }

    /**
     * Get a secure URL to a controller action.
     *
     * @param string        $url
     * @param \DateTime|int $expiration
     *
     * @throws InvalidExpiration
     *
     * @return string
     */
    public function sign($url, $expiration)
    {
        if (is_int($expiration) && ($expiration == 0)) {
            $next_century = DateTime::createFromFormat('d/m/Y H:i:s', '01/01/2100 00:00:00', new DateTimeZone('Europe/Paris'));
            $expiration = $next_century ? $next_century : 0;
        }

        return parent::sign($url, $expiration);
    }

    /**
     * @param string $ais_id
     * @param string $acr_clients
     * @param string $photos_gateway
     * @param string $photos_gateway_acr
     *
     * @return string
     */
    public function sign_id($ais_id, $acr_clients = "10.144.0.0/12 10.145.0.0/12",
                                     $photos_gateway = "https://otp-photos-atlas.web.cern.ch",
                                     $photos_gateway_acr = "https://otp-photos-atlas-acr.web.cern.ch")
    {
        $gateway = $this->get_gateway($acr_clients, $photos_gateway, $photos_gateway_acr);

        $url = $gateway . "/?ais_id=" . $ais_id;

        $signed_url = $this->sign($url, 10);

        return $signed_url;
    }

    /**
     * @param string $acr_clients
     * @param string $photos_gateway
     * @param string $photos_gateway_acr
     *
     * @return string
     */
    public function get_gateway($acr_clients, $photos_gateway, $photos_gateway_acr)
    {
        $gateway = $photos_gateway;
        $ip = $this->get_client_ip();
        $clients = preg_split('/[ ,\n]/', $acr_clients);
        if ($clients and $ip) {
            foreach($clients as $acr_client) {
                $ip_range = gethostbyname($acr_client);
                if ($this->ip_in_range( $ip, $ip_range )) {
                    $gateway = $photos_gateway_acr;
                    break;
                }
            }
        }
        return $gateway;
    }

    /**
     * Check if a given ip is in a network
     * @param  string $ip    IP to check in IPV4 format eg. 127.0.0.1
     * @param  string $range IP/CIDR netmask eg. 127.0.0.0/24, also 127.0.0.1 is accepted and /32 assumed
     * @return boolean true if the ip is in this range / false if not.
     */
    protected function ip_in_range( $ip, $range ) {
        if ( strpos( $range, '/' ) == false ) {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list( $range, $netmask ) = explode( '/', $range, 2 );
        $range_decimal = ip2long( $range );
        $ip_decimal = ip2long( $ip );
        $wildcard_decimal = pow( 2, ( 32 - intval($netmask) ) ) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
    }

    /**
     * @return string
     */
    protected function get_client_ip() {
        $ip = "";
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } else if (isset($_SERVER["REMOTE_ADDR"])) {
            $ip =  $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }
}
