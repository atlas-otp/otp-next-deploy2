<?php

class PhotoUrlSigner
{
    /**
     * The key that is used to generate secure signatures.
     *
     * @var string
     */
    protected $signatureKey;

    /**
     * The URL's query parameter name for the expiration.
     *
     * @var string
     */
    protected $expiresParameter;

    /**
     * The URL's query parameter name for the signature.
     *
     * @var string
     */
    protected $signatureParameter;

    /**
     * @param string $signatureKey
     * @param string $expiresParameter
     * @param string $signatureParameter
     */
    public function __construct($signatureKey, $expiresParameter = 'expires', $signatureParameter = 'signature')
    {
        date_default_timezone_set('Europe/Amsterdam');

        if ($signatureKey == '') {
            exit(1);
        }

        $this->signatureKey = $signatureKey;
        $this->expiresParameter = $expiresParameter;
        $this->signatureParameter = $signatureParameter;
    }

    /**
     * Generate a token to identify the secure action.
     * debug and time parameter are not used in signature
     *
     * @param string $url
     * @param int $expiration
     *
     * @return string
     */
    protected function createSignature($url, $expiration)
    {
        $query = array();
        $url = parse_url($url);
        if ($url) {
            if (array_key_exists("query", $url)) {
                $query_string = $url["query"];
                parse_str($query_string, $query);
            }
        }

        unset($query['debug']);
        unset($query['time']);

        $query = http_build_query($query);

        // print("{$query}::{$expiration}::{$this->signatureKey}\n");
        return md5("{$query}::{$expiration}::{$this->signatureKey}");
    }

    /**
     * Get a secure URL to a controller action.
     *
     * @param string        $url
     * @param \DateTime|int $expiration
     *
     * @return string
     */
    public function sign($url, $expiration)
    {
        if (is_int($expiration) && ($expiration == 0)) {
            $next_century = DateTime::createFromFormat('d/m/Y H:i:s', '01/01/2100 00:00:00', new DateTimeZone('Europe/Paris'));
            $expiration = $next_century ? $next_century : 0;
        }

        $expiration = $this->getExpirationTimestamp($expiration);
        $signature = $this->createSignature($url, $expiration);

        return $this->signUrl($url, $expiration, $signature);
    }

    /**
     * @param string $ais_id
     * @param string $acr_clients
     * @param string $photos_gateway
     * @param string $photos_gateway_acr
     *
     * @return string
     */
    public function sign_id($ais_id, $acr_clients = "10.144.0.0/12 10.145.0.0/12",
                                     $photos_gateway = "https://otp-photos-atlas.web.cern.ch",
                                     $photos_gateway_acr = "https://otp-photos-atlas-acr.web.cern.ch")
    {
        $gateway = $this->get_gateway($acr_clients, $photos_gateway, $photos_gateway_acr);

        $url = $gateway . "/?ais_id=" . $ais_id;

        $signed_url = $this->sign($url, 10);

        return $signed_url;
    }

    /**
     * Retrieve the expiration timestamp for a link based on an absolute DateTime or a relative number of days.
     *
     * @param \DateTime|int $expiration The expiration date of this link.
     *                                  - DateTime: The value will be used as expiration date
     *                                  - int: The expiration time will be set to X days from now
     *
     * @return int
     */
    protected function getExpirationTimestamp($expiration)
    {
        if (is_int($expiration)) {
            $expiration = (new DateTime())->modify((int) $expiration.' days');
        }

        if (! $expiration instanceof DateTime) {
            exit(1);
        }

        if (! $this->isFuture($expiration->getTimestamp())) {
            exit(1);
        }

        return $expiration->getTimestamp();
    }

    /**
     * Check if a timestamp is in the future.
     *
     * @param int $timestamp
     *
     * @return bool
     */
    protected function isFuture($timestamp)
    {
        return $timestamp >= (new DateTime())->getTimestamp();
    }

    /**
     * Add expiration and signature query parameters to an url.
     *
     * @param string       $url
     * @param int          $expiration
     * @param string       $signature
     *
     * @return string
     */
    protected function signUrl($url, $expiration, $signature)
    {
        $query = array();
        $url = parse_url($url);
        if ($url) {
            if (array_key_exists("query", $url)) {
                $query_string = $url["query"];
                parse_str($query_string, $query);
            }
        }

        $query[$this->expiresParameter] = $expiration;
        $query[$this->signatureParameter] = $signature;

        $query = http_build_query($query);
        // missing port, missing fragment
        $r = "";
        if ($url && array_key_exists("scheme", $url)) {
            $r = $r . $url['scheme'] . "://";
        }
        if ($url && array_key_exists("host", $url)) {
            $r = $r . $url['host'];
        }
        if ($url && array_key_exists("path", $url)) {
            $r = $r . $url['path'];
        }
        $r = $r . "?" . $query;
        if ($url && array_key_exists("fragment", $url)) {
            $r = $r . "#" . $url['fragment'];
        }
        return $r;
    }

    /**
     * Validate a signed url.
     *
     * @param string $url
     *
     * @return bool
     */
    public function validate($url)
    {
        $query = array();
        $parts = parse_url($url);
        if (! $parts) {
            return false;
        }

        if (array_key_exists("query", $parts)) {
            $query_string = $parts["query"];
            parse_str($query_string, $query);
        }

        if ($this->isMissingAQueryParameter($query)) {
            return false;
        }

        $expiration = (int) $query[$this->expiresParameter];

        if (! $this->isFuture($expiration)) {
            return false;
        }

        if (! $this->hasValidSignature($url)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $acr_clients
     * @param string $photos_gateway
     * @param string $photos_gateway_acr
     *
     * @return string
     */
    public function get_gateway($acr_clients, $photos_gateway, $photos_gateway_acr)
    {
        $gateway = $photos_gateway;
        $ip = $this->get_client_ip();
        $clients = preg_split('/[ ,\n]/', $acr_clients);
        if ($clients and $ip) {
            foreach($clients as $acr_client) {
                $ip_range = gethostbyname($acr_client);
                if ($this->ip_in_range( $ip, $ip_range )) {
                    $gateway = $photos_gateway_acr;
                    break;
                }
            }
        }
        return $gateway;
    }

    /**
     * Check if a given ip is in a network
     * @param  string $ip    IP to check in IPV4 format eg. 127.0.0.1
     * @param  string $range IP/CIDR netmask eg. 127.0.0.0/24, also 127.0.0.1 is accepted and /32 assumed
     * @return boolean true if the ip is in this range / false if not.
     */
    protected function ip_in_range( $ip, $range ) {
        if ( strpos( $range, '/' ) == false ) {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list( $range, $netmask ) = explode( '/', $range, 2 );
        $range_decimal = ip2long( $range );
        $ip_decimal = ip2long( $ip );
        $wildcard_decimal = pow( 2, ( 32 - intval($netmask) ) ) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
    }

    /**
     * @return string
     */
    protected function get_client_ip() {
        $ip = "";
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } else if (isset($_SERVER["REMOTE_ADDR"])) {
            $ip =  $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }

    /**
     * Check if a query is missing a necessary parameter.
     *
     * @param array $query
     *
     * @return bool
     */
    protected function isMissingAQueryParameter($query)
    {
        if (! isset($query[$this->expiresParameter])) {
            return true;
        }

        if (! isset($query[$this->signatureParameter])) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the url has a forged signature.
     *
     * @param string $url
     *
     * @return bool
     */
    protected function hasValidSignature($url)
    {
        $query = array();
        $parts = parse_url($url);
        if (! $parts) {
            return false;
        }

        if (array_key_exists("query", $parts)) {
            $query_string = $parts["query"];
            parse_str($query_string, $query);
        }

        $expiration = (int) $query[$this->expiresParameter];
        $providedSignature = (string) $query[$this->signatureParameter];

        $intendedUrl = $this->getIntendedQuery($url);

        $validSignature = $this->createSignature($intendedUrl, $expiration);

        return $this->hash_equals($validSignature, $providedSignature);
    }

    /**
     * Retrieve the intended URL by stripping off the UrlSigner specific parameters.
     *
     * @param string $url
     *
     * @return string
     */
    protected function getIntendedQuery($url)
    {
        $query = array();
        $url = parse_url($url);
        if ($url) {
            if (array_key_exists("query", $url)) {
                $query_string = $url["query"];
                parse_str($query_string, $query);
            }
        }

        unset($query[$this->expiresParameter]);
        unset($query[$this->signatureParameter]);

        $query = http_build_query($query);

        return "?" . $query;
    }

    /**
     * Compares two strings
     *
     * @param string $str1
     * @param string $str2
     *
     * @return bool
     */
    protected function hash_equals($str1, $str2) {
        if(strlen($str1) != strlen($str2)) {
            return false;
        } else {
            $res = $str1 ^ $str2;
            $ret = 0;
            for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
            return !$ret;
        }
    }
}
