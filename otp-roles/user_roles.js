
$(document).ready(function() {
    var table = new DataTable('#table', {
        ajax: {
            url: "user_roles_data.php",
            dataSrc: function ( json ) {
                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                var user_roles = decode_user_roles(json.options.ID);
                set_user(json.user, false,false, user_roles, table);

                return json.data;
            }
        },
        lengthChange: false,

        layout: {
            topStart: 'buttons',
            topEnd: 'search',
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        scrollX:        '100%',
        scrollY:        '50vh',
        scrollCollapse: true,
        scroller:       true,
        deferRender:    true,
        stateSave:      true,
        columns: [
            { data: "ID" },
            { data: "USERNAME" },
            { data: "LNAME" },
            { data: "FNAME" },
            { data: "ROLE" },
            { data: "SYSTEM_ID" },
            { data: "ACTIVITY_ID" },
            { data: "WBS_ID" }
        ]
     } );
} );
