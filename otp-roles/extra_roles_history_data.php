<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../');

require("config.php");

require('vendor/autoload.php');

require_once('otp-photos/PhotoUrlSigner.php');

use Unleash\Client\UnleashBuilder;

function flag_is_enabled(string $flag, string $env = null): bool {
    global $ff_details;

    if ($flag == '') {
        return true;
    }

    if (!$env) {
        $env = $ff_details["env"];
    }

    $instance_id = $ff_details["instance_id"];
    $app_url = $ff_details["app_url"];

    $unleash = UnleashBuilder::create()
    ->withAppName($env)
    ->withInstanceId($instance_id)
    ->withAppUrl($app_url)
    ->withCacheTimeToLive(0)    // remove for single env
    ->build();

    $feature = $env.'-'.$flag;
    return $unleash->isEnabled($feature);
}

function get_picture_url(string $user_id): ?string {
    global $db;
    global $photos_details;

    $ais_id = NULL;

    $rows = $db
        ->select( 'PUB_PERSON', 'AIS_PERSON_ID', ['ID' => $user_id, 'PHOTO' => 1], ['AIS_PERSON_ID'] )
        ->fetchAll();

    if (array_key_exists(0, $rows) &&
        array_key_exists('AIS_PERSON_ID', $rows[0])) {
        $ais_id = $rows[0]['AIS_PERSON_ID'];
    } else {
        $ais_id = NULL;
    }

    $picture_url = NULL;
    if ($ais_id) {
        $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

        $expiration = (new DateTime())->modify('10 mins');

        $server = $photos_details["gateway"];

        $picture_url = $server . "?ais_id=" . $ais_id;
        $picture_url = $urlSigner->sign($picture_url, $expiration);
    }

    return $picture_url;
}
?>
<?php
if (!flag_is_enabled('roles')) {
	header("HTTP/1.0 501 Not Implemented");
    die("ERROR: Feature 'roles' is disabled\n");
}
?>
<?php

/*
 * Roles table
 */

include( "config.php" );

// DataTables PHP library
include( "editor/lib/DataTables.php" );

/**
 * @var DataTables\Database $db
 * @var DataTables\Database $sql_details
 */

// function is_true($val, $return_null=false){
//     $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
//     return ( $boolval===null && !$return_null ? false : $boolval );
// }

$to_int = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val);
};

$from_int = function ( $val, $data ) {
	return $val == 0 ? NULL : $val;
};

$to_fte = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val)/100.;
};

$from_fte = function ( $val, $data ) {
	return $val*100.;
};

$to_bool = function( $val, $data ) {
	return $val == NULL ? false : (bool)$val;
};

// $from_bool = function( $val, $data ) {
// 	return $val == NULL ? false : (bool)$val;
// };

$to_string = function ( $val, $data ) {
    return $val == NULL ? '' : $val;
};

$from_string = function ( $val, $data ) {
    return $val == '' ? NULL : $val;
};

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
    DataTables\Editor\SearchPaneOptions,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost'])) {
    $user = "DUNS";
    if (array_key_exists('user', $_REQUEST)) {
        $user = $_REQUEST['user'];
    }
} else {
    // Old SSO
    $user = getenv("OIDC_CLAIM_cern_upn");
    if (!$user) {
        // compatible with OKD4 SSO
        $user = $_SERVER['HTTP_X_FORWARDED_USER'];
    }

    if ($user) {
        $user = strtoupper($user);
    } else {
        $user = "";
    }
    // $user = "LBARDO";
}

// get userid, system_id for unknown users
$user_id = 999980;
$user_id_select = $db->select( 'PERSON_SERVICE_ACCOUNT', 'ID', ['USERNAME' => $user] )->fetch();
if ($user_id_select) {
    $user_id = intval($user_id_select['ID']);
}

$system_transaction = 1;
$task_transaction = 2;
$booking_period_transaction = 3;
$shift_booking_transaction = 4;
$admin_transaction = 5;

$user_role_options = Options::inst()
	->table( 'USER_ROLE' )
	->value( 'ID' )
	->label( array('USERNAME', 'LNAME', 'FNAME', 'ROLE', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID') )
	// ->where(function ($q) {
	// 	global $user;

	// 	$q->where( 'USERNAME', $user, '=');
	// })
    ->order( 'LNAME, FNAME' )
	->render( function ( $row ) {
		return $row['USERNAME'].':'.$row['LNAME'].':'.$row['FNAME'].':'.$row['ROLE'].':'.$row['SYSTEM_ID'].':'.$row['ACTIVITY_ID'].':'.$row['WBS_ID'];
	} );

$roles = Editor::inst( $db, 'USER_ROLE', 'ID' )
    ->fields(
        Field::inst( 'USER_ROLE.USERNAME' ),
        Field::inst( 'USER_ROLE.ROLE' ),
        Field::inst( 'USER_ROLE.SYSTEM_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } ),
            Field::inst( 'USER_ROLE.WBS_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } )
    )
    ->write( false )
    ->where( 'USER_ROLE.USERNAME', $user )
    ->get();


function has_role(string $requested_role, int $system_or_wbs_id = null): bool {
    global $roles;

    foreach ($roles['data'] as $role_assignment) {
        if (!array_key_exists('USER_ROLE', $role_assignment)) {
            return false;
        }
        $role = $role_assignment['USER_ROLE']['ROLE'];
        $system_id = $role_assignment['USER_ROLE']['SYSTEM_ID'];
        $wbs_id = $role_assignment['USER_ROLE']['WBS_ID'];
        if ($requested_role == $role) {
            switch($role) {
                case 'ROLE_ADMIN':
                    return true;
                case 'ROLE_ROLE_MANAGER':
                    return true;
                case 'ROLE_ACTIVITY_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                case 'ROLE_SYSTEM_MANAGER':
                    if ($system_or_wbs_id == $system_id) {
                        return true;
                    }
                    break;
                case 'ROLE_WBS_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    return false;
}
?>
<?php

$data = Editor::inst( $db, 'EXTRA_ROLES_AVER', 'T_PK' )
	->fields(
		Field::inst( 'EXTRA_ROLES_AVER.T_WORKID' ),
		Field::inst( 'EXTRA_ROLES_AVER.T_PK' ),
		Field::inst( 'EXTRA_ROLES_AVER.T_PK_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.A_CREUSER' ),
		Field::inst( 'EXTRA_ROLES_AVER.A_CREDATE' ),
		Field::inst( 'EXTRA_ROLES_AVER.T_START' ),
		Field::inst( 'EXTRA_ROLES_AVER.T_STATUS' ),
		Field::inst( 'EXTRA_ROLES_AVER.T_DELETE' ),
		Field::inst( 'EXTRA_ROLES_AVER.IS_CURRENT_VERSION' ),
		Field::inst( 'EXTRA_ROLES_AVER.IS_CURRENT_AUTH_VERSION' ),
		Field::inst( 'EXTRA_ROLES_AVER.VERSION' ),

		Field::inst( 'EXTRA_ROLES_AVER.ID_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.ID_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.ID' )
			->getFormatter( $to_string )
			->options( $user_role_options ),

		Field::inst( 'EXTRA_ROLES_AVER.USER_ID_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.USER_ID_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.USER_ID' ),

		Field::inst( 'PUB_PERSON_PREV.FNAME' ),
		Field::inst( 'PUB_PERSON_PREV.LNAME' ),

		Field::inst( 'PUB_PERSON.FNAME' ),
		Field::inst( 'PUB_PERSON.LNAME' ),

		Field::inst( 'EXTRA_ROLES_AVER.ROLE_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.ROLE_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.ROLE' ),

		Field::inst( 'EXTRA_ROLES_AVER.COMMENTS_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.COMMENTS_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.COMMENTS' )
			->getFormatter( $to_string ),

		Field::inst( 'EXTRA_ROLES_AVER.SYSTEM_ID_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.SYSTEM_ID_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.SYSTEM_ID' )
			->getFormatter( $to_string ),

		Field::inst( 'SYSTEM_NODE_PREV.TITLE' ),
		Field::inst( 'SYSTEM_NODE.TITLE' ),

		Field::inst( 'ACTIVITY_PREV.ID' )
			->getFormatter( $to_string ),
		Field::inst( 'ACTIVITY_PREV.TITLE' ),
		Field::inst( 'ACTIVITY.ID' )
			->getFormatter( $to_string ),
		Field::inst( 'ACTIVITY.TITLE' ),

		Field::inst( 'EXTRA_ROLES_AVER.WBS_ID_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.WBS_ID_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.WBS_ID' )
			->getFormatter( $to_string ),

		Field::inst( 'WBS_NODE_PREV.TITLE' ),
		Field::inst( 'WBS_NODE.TITLE' ),

		Field::inst( 'EXTRA_ROLES_AVER.END_DATE_CHANGED' ),
		Field::inst( 'EXTRA_ROLES_AVER.END_DATE_PREV' ),
		Field::inst( 'EXTRA_ROLES_AVER.END_DATE' )
			->getFormatter( $to_string )
	)
	->leftJoin( 'PUB_PERSON', 'PUB_PERSON.ID', '=', 'EXTRA_ROLES_AVER.USER_ID' )
	->leftJoin( 'PUB_PERSON PUB_PERSON_PREV', 'PUB_PERSON_PREV.ID', '=', 'EXTRA_ROLES_AVER.USER_ID_PREV' )
	->leftJoin( 'PUB_PERSON CREPERSON', 'CREPERSON.ID', '=', 'EXTRA_ROLES_AVER.A_CREUSER' )

	->leftJoin( 'SYSTEM_NODE', 'SYSTEM_NODE.ID', '=', 'EXTRA_ROLES_AVER.SYSTEM_ID' )
	->leftJoin( 'SYSTEM_NODE SYSTEM_NODE_PREV', 'SYSTEM_NODE_PREV.ID', '=', 'EXTRA_ROLES_AVER.SYSTEM_ID_PREV' )

	->leftJoin( 'WBS_NODE', 'WBS_NODE.ID', '=', 'EXTRA_ROLES_AVER.WBS_ID' )
	->leftJoin( 'WBS_NODE WBS_NODE_PREV', 'WBS_NODE_PREV.ID', '=', 'EXTRA_ROLES_AVER.WBS_ID_PREV' )

	// make sure ACTIVITY is set to WBS_NODE.PARENT_ID or if not existing to same as WBS_NODE.ID
	->leftJoin( 'WBS_NODE ACTIVITY', 'WBS_NODE.PARENT_ID = ACTIVITY.ID or (WBS_NODE.PARENT_ID is NULL and ACTIVITY.ID = WBS_NODE.ID)' )
	->leftJoin( 'WBS_NODE ACTIVITY_PREV', 'WBS_NODE_PREV.PARENT_ID = ACTIVITY_PREV.ID or (WBS_NODE_PREV.PARENT_ID is NULL and ACTIVITY_PREV.ID = WBS_NODE_PREV.ID)' )

	->validator( function ( $editor, $action, $data ) {
        if ( $action !== Editor::ACTION_READ ) {
            return 'Cannot modify history data';
        }
    } )
	// removed in editor 2.1.0 ???
	//->write( false )
	->debug(true)
	->process( $_POST )
	->data();

	header('Content-Type: application/json');
	$version = file_get_contents('../version.txt');
	$data[ 'version' ] = $version ? trim($version) : "unknown";
	$data[ 'user' ] = $user;
	$data[ 'db' ] = $sql_details['db'];
	echo json_encode( $data );

