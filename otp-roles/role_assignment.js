
$(document).ready(function() {
    // Setup DataTable
    var table = new DataTable('#table', {
        lengthChange: false,

        layout: {
            top2Start: 'buttons',
            top2End: 'search',
            top: 'searchPanes',
            topStart: null,
            topEnd: null,
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        ajax: {
            url: "role_assignment_data.php",
            dataSrc: function ( json ) {
                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                console.log(json.options);
                var user_roles = decode_user_roles(json.options['ROLE_ASSIGNMENT.ID']);
                set_user(json.user, false,false, user_roles, table);

                return json.data;
            }
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        paging:         true,
        scroller:       true,

        // for SearchPanes
        columnDefs: [
            {
                searchPanes: {
                    show: false,
                },
                targets: [0, 1, 3, 8],
            },
            {
                searchPanes: {
                    initCollapsed: true,
                    cascadePanes: true,
                    show: true,
                },
                targets: [2, 4, 5, 6, 7],
            }
        ],
        // serverSide: true,

        columns: [
            {
                data: "ROLE_ASSIGNMENT.ID",
                className: 'id'
            },
            {
                data: null,
                render: function ( data, _type, _row ) {
                    // Combine the first and last names into a single table field
                    return data.PUB_PERSON.LNAME + ', ' + data.PUB_PERSON.FNAME;
                },
                className: 'user_id'
            },
            {
                data: "PUB_PERSON.USERNAME",
                className: 'username'
            },
            {
                data: "ROLE_ASSIGNMENT.PRIORITY",
                className: 'priority',
                defaultContent: ''
            },
            {
                data: "ROLE_ASSIGNMENT.ROLE",
                render: function ( data, type, _row ) {
                    if (type === 'display') {
                        // Translate Roles to Human Names
                        switch(data) {
                            case "ROLE_ADMIN":
                                return "Admin";
                            case "ROLE_ACTIVITY_MANAGER":
                                return "Activity&nbsp;Manager";
                            case "ROLE_SYSTEM_MANAGER":
                                return "System&nbsp;Manager";
                            case "ROLE_WBS_MANAGER":
                                return "WBS&nbsp;Manager";
                            case "ROLE_ROLE_MANAGER":
                                return "Role&nbsp;Manager";
                            default:
                                return "Unknown&nbsp;Role";
                        }
                    }
                    return data;
                },
                className: 'role',
                defaultContent: ''
            },
            {
                data: "SYSTEM_NODE.TITLE",
                render: function ( data, _type, row ) {
                    if (row.ROLE_ASSIGNMENT.ROLE == "ROLE_SYSTEM_MANAGER") {
                        return data;
                    }
                    return null;
                },
                className: 'system',
                defaultContent: ''
            },
            {
                data: "ACTIVITY.TITLE",
                render: function ( data, _type, row ) {
                    if (row.ROLE_ASSIGNMENT.ROLE == "ROLE_ACTIVITY_MANAGER") {
                        return data;
                    }
                    if (row.ROLE_ASSIGNMENT.ROLE == "ROLE_WBS_MANAGER") {
                        return data;
                    }
                    return null;
                },
                className: 'activity',
                defaultContent: ''
            },
            {
                data: "WBS_NODE.TITLE",
                render: function ( data, _type, row ) {
                    if (row.ROLE_ASSIGNMENT.ROLE == "ROLE_WBS_MANAGER") {
                        return data;
                    }
                    return null;
                },
                className: 'wbs',
                defaultContent: ''
            },
            {
                data: "ROLE_ASSIGNMENT.COMMENTS",
                className: 'comments'
            },
        ],
        order: [[ 1, 'asc' ], [3, 'asc']],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },

        // AutoFill and KeyTable
        // autoFill: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        // keys: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        buttons: [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            var api = this.api();

            count_unique(api, 0);
            count_unique(api, 2);
            count_unique(api, 4);
            count_unique(api, 5);
            count_unique(api, 6);
            count_unique(api, 7);
        }
    } );
 } );
