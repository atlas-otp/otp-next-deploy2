var editor; // use a global for the submit and return data rendering
var debug;
var current_user;
var user_roles;
var data_script = "roles_data.php";
var duplicate = false;
var selected_row_count = 0;

function add_can_edit(data, user, user_roles) {
    current_user = user;

    for (var i = 0; i < data.length; i++) {
        if (data[i].ROLE == 'ROLE_ADMIN') {
            // we do not want to change any ADMIN roles
            data[i].CAN_EDIT = false;
            data[i].CAN_EDIT_BECAUSE = "BLOCKED";
        } else if (data[i].RO) {
            // we cannot change VIEWs
            data[i].CAN_EDIT = false;
            data[i].CAN_EDIT_BECAUSE = "VIEW";
        } else {
            var can_edit_and_because = can_edit(user, user_roles, data[i].SYSTEM_ID, data[i].ACTIVITY_ID, data[i].WBS_ID);
            data[i].CAN_EDIT = can_edit_and_because.can_edit;
            data[i].CAN_EDIT_BECAUSE = can_edit_and_because.can_edit_because;
        }
    }
}

$(document).ready(function() {
    var queryString = window.location.search;
    let params = new URLSearchParams(queryString);

    // query string ?test
    let is_test = params.get("test") !== null;

    // query string ?debug
    debug = params.get("debug") !== null;
    let fixed_columns = debug ? 6 : 2;

    let is_local = window.location.hostname == 'localhost' || window.location.hostname == 'otp-next.localhost';

    if (debug) {
        console.log(window.location);
        console.log("test = ", is_test);
        console.log("debug = ", debug);
        console.log("local = ", is_local);
    }

    // Setup Editor
    editor = new DataTable.Editor({
        ajax: {
            url: data_script,
            // Modify what we send if needed
            data: function ( d ) {
                // For CREATE and DELETE no modifications needed
                if ((d.action == 'create') || (d.action == 'remove')) {
                    if (debug) console.log("Data unchanged for CREATE or REMOVE");
                    return;
                }

                // NOTE below breaks double editing as 'table' changes
                // related test: test_edit_inline_system_twice
                // Send also initial values before change in 'init_data'
                // to enable to detect (and possibly resolve) conflicts (a la git)
                // [row, value] = Object.entries(d.data)[0];
                // [table, value] = Object.entries(value)[0];
                // [field, value] = Object.entries(value)[0];

                // // if (debug) console.log(editor.init_data['ROLE_ASSIGNMENT']);
                // // FIXME check for row, for multi-submit
                // d.init_data = {}
                // d.init_data[row] = {}
                // d.init_data[row][table] = {}
                // d.init_data[row][table][field] = editor.init_data[table][field]

                if (debug) console.log("Data populated for EDIT (currently disabled)");
                if (debug) console.log(d);
            }
        },
        table: "#table",
        fields: [
            // all Editable fields
            {
                // TBD
                label: "Search User:",
                name: "USER_ID",
                type: "datatable",
                // TBD
                config: {
                    paging:         true,
                    scrollY:        150,
                    scrollCollapse: true,
                    deferRender:    true,
                    // See #156, cannot use scroller as selected person (beyond the first few) does not get shown (scrolled to)
                    // scroller:       true,
                    columns: [
                        {
                            title: '<b>Last name</b>, First name',
                            data: 'label'
                        }
                    ]
                }
            },
            {
                label: "Selected User:",
                name: "NAME",
                type: "display",
                data: function ( row, type, val, meta ) {
                    // Combine the first and last names into a single table field
                    return `<b>${row.LNAME}</b>, ${row.FNAME}`;
                },
            },
            {
                label: "Selected:",
                name: "LNAME",
                type: "hidden"
            },
            {
                label: "Selected:",
                name: "FNAME",
                type: "hidden"
            },

            {
                label: "Username:",
                name: "USERNAME",
            }, {
                label: "Photo:",
                name: "PICTURE_URL",
                type: "image"
            }, {
                label: "Role:",
                name: "ROLE",
                type: "select",
                def: "ROLE_WBS_MANAGER",
                placeholder: "Select a Role",
                placeholderValue: ""
            }, {
                // For: ROLE_SYSTEM_MANAGER
                label: "System:",
                name: "SYSTEM_ID",
                type: "select",
                def: "",
                placeholder: "Select a System",
                placeholderValue: ""
            }, {
                // For: ACTIVTY
                label: "Activity:",
                name: "ACTIVITY_ID",
                type: "select",
                def: "",
                placeholder: "Select an Activity",
                placeholderValue: ""
            }, {
                // For: ROLE_WBS_MANAGER
                label: "Work Breakdown Structure:",
                name: "WBS_ID",
                type: "select",
                def: "",
                placeholder: "Select a WBS",
                placeholderValue: ""
            }, {
                label: "End Date:",
                name: "END_DATE",
                type:  'datetime',
                def:   function () {
                    // suggested end date is 2 years from today
                    var end_date = new Date();
                    end_date.setFullYear(end_date.getFullYear() + 2);
                    return end_date;
                },
                opts:  {
                    // disableDays: [ 0, 6 ],
                    minDate: new Date(),
                    buttons: {
                        today: false,
                        clear: true
                    }
                }
            }, {
                label: "Comments:",
                name: "COMMENTS",
                def: ""
            }
        ]
    } );

    editor.on('initEdit', function (_e, node, data, items, type) {
        // Called just before editing (inline or normal) is done.
        if (debug) console.log('initEdit', node, type, data, items);

        // TBD Store initial data before change (as prepared in ajax.data)
        // to enable to detect (and possibly resolve) conflicts (a la git)
        editor.init_data = data;
    });

    editor.on('preOpen', function(e, mode, action) {
        // Called before a form (inline or normal) is opened. Called after initEdit.
        if (debug) console.log('preOpen', mode, action, e);

        if (action === 'create') {
            if (!has_role_admin(current_user, user_roles)) {
                if (debug) console.log("No new roles can be created if not ADMIN");
                return false;
            }
        } else {
            // NOTE: editor.field('CAN_EDIT').val() is not filled at first select, so we use the datatable
            var modifier = editor.modifier();
            var data = table.row( modifier ).data();

            // No edits/deletes if CAN_EDIT field is false
            if (data && (typeof data !== "undefined") && !data.CAN_EDIT) {
                if (debug) console.log("No edits if CAN_EDIT field is false");
                return false;
            }

            // No edits possible if no username
            if ((data.USERNAME == '') && (action == 'edit')) {
                if (debug) console.log("No edits possible if no username");
                return false;
            }
        }

        do_validate = false;

        set_change_handler(editor, 'SYSTEM_ID', mode, debug);
        set_change_handler(editor, 'WBS_ID', mode, debug);
        set_change_handler(editor, 'END_DATE', mode, debug);

        // Final select on ActivityId (if activiy manager) should submit
        editor.field('ACTIVITY_ID').input().off('change');
        if (mode === 'inline') {
            editor.field('ACTIVITY_ID').input().on('change', function(e, data) {
                if (!data || !data.editor) {
                    if (editor.field('ROLE').val() == 'ROLE_ACTIVITY_MANAGER') {
                        if (debug) console.log("Select Submit on activity manager ActivityId");
                        editor.submit();
                    }
                }
            } );
        }

    } );

    var do_validate = false;

    function validate() {
        if (do_validate) {

            // Check for valid USER_ID
            var user_id = editor.field('USER_ID');
            if (user_id.val() == "") {
                if (debug) console.log("Select a User");
                user_id.error("Select a User");
            }

            // Check for valid ROLE and
            // either SYSTEM_ID or ACTIVITY_ID or both ACTIVITY_ID and WBS_ID
            var role = editor.field('ROLE');
            var system_id = editor.field('SYSTEM_ID');
            var activity_id = editor.field('ACTIVITY_ID');
            var wbs_id = editor.field('WBS_ID');

            var activity, system, wbs;
            switch(role.get()) {
                case "ROLE_ADMIN":
                    // ADMIN no further fields needed
                    role.error("");
                    activity_id.error("");
                    system_id.error("");
                    wbs_id.error("");
                    break;
                case "ROLE_ROLE_MANAGER":
                    // ROLE_MANAGER no further fields needed
                    role.error("");
                    activity_id.error("");
                    system_id.error("");
                    wbs_id.error("");
                    break;
                case "ROLE_ACTIVITY_MANAGER":
                    role.error("");
                    system_id.error("");
                    wbs_id.error("");
                    // Check for valid ACTIVITY
                    activity = parseInt(activity_id.val());
                    if (!isNaN(activity) && activity_ids.includes(activity)) {
                        activity_id.error("");
                    } else {
                        if (debug) console.log("Select an Activity");
                        activity_id.error("Select an Activity");
                    }
                    break;
                case "ROLE_SYSTEM_MANAGER":
                    role.error("");
                    activity_id.error("");
                    wbs_id.error("");
                    // Check for valid SYSTEM
                    system = parseInt(system_id.val());
                    if (isNaN(system)) {
                        if (debug) console.log("Select a System");
                        system_id.error("Select a System");
                    }
                    break;
                case "ROLE_WBS_MANAGER":
                    role.error("");
                    system_id.error("");
                    // Check for valid ACTIVITY
                    activity = parseInt(activity_id.val());
                    if (!isNaN(activity) && activity_ids.includes(activity)) {
                        activity_id.error("");
                    } else {
                        if (debug) console.log("Select an Activity");
                        activity_id.error("Select an Activity");
                    }

                    // Check for valid WBS except ACTIVITY
                    wbs = parseInt(wbs_id.val());
                    if (isNaN(wbs) || activity_ids.includes(wbs)) {
                        if (debug) console.log("Select a WBS");
                        wbs_id.error("Select a WBS");
                    } else {
                        wbs_id.error("");
                    }
                    break;
                default:
                    if (debug) console.log("Select a Role");
                    role.error("Select an Role");
                    system_id.error("");
                    activity_id.error("");
                    wbs_id.error("");
                    break;
            }
        }
    }

    editor.on('preSubmit', function(e, data, action) {
        if (debug) console.log('preSubmit', e, action, data);

        do_validate = true;

        // Validate fields before submitting to server

        // DELETE is ok
        if (action == "remove") {
            if (debug) console.log("DELETE is ok");
            return true;
        }

        // Add extra data to edit row
        if (action === 'edit') {
            // if ("EXTRA_ROLES" in data.data[ editor.init_data.DT_RowId ]) {
            //    data.data[ editor.init_data.DT_RowId ].ID = editor.init_data.ID;
            //    data.data[ editor.init_data.DT_RowId ].USER_ID = editor.init_data.USER_ID;
            // }

            // make sure wbs_id is set to activity_id if role is activity_manager
            if (editor.init_data.ROLE == 'ROLE_ACTIVITY_MANAGER') {
                data.data[ editor.init_data.DT_RowId ].WBS_ID = data.data[ editor.init_data.DT_RowId ].ACTIVITY_ID;
            }
        }

        if (action === 'create') {
            // data.data[0].EXTRA_ROLES.ID = 0;

            // make sure wbs_id is set to activity_id if role is activity_manager
            if (data.data[ 0 ].ROLE == 'ROLE_ACTIVITY_MANAGER') {
                data.data[ 0 ].WBS_ID = data.data[ 0 ].ACTIVITY_ID;
            }
        }

        validate();

        // Any missing fields, abort submit
        if ( this.inError() ) {
            if (debug) console.log("Some error prevented submit for action: ", action);
            return false;
        }

        return true;
    });

    // handle server replies and errors
    editor.on('postSubmit', function (_e, json, data, action, xhr) {
        if (debug) console.log("postSubmit", action, json, data, xhr);

        if (json === null) {
            console.log(data);
            console.log('responseText:', xhr.responseText);
            alert("Server error in script: " + data_script + ", look at console log.");
        }

        update_roles(json.data, current_user, user_roles, table);

        //   if ( json.FieldErrors ) {
        //      .... take a look at json.fieldErrors
        //   }
    });

    // jump to new or edited record
    editor.on( 'submitComplete', function ( e, json, data, action ) {
        if (debug) console.log("submitComplete", data, action);
        if ( action === 'create' || action === 'edit' ) {
            if (is_test) {
                $(`#${data.DT_RowId}`).addClass("test_roles_record");
            }
            table.row(`#${data.DT_RowId}`).scrollTo();
        }

        duplicate = false;
    });

    //  USER_ID -> PICTURE_URL
    editor.dependent( 'USER_ID', 'user_data.php' );
    editor.dependent('USER_ID', function ( val, data, callback, e) {
        if (debug) console.log("Dependency called for USER_ID -> PICTURE_URL", val, data, e);
        $.ajax( {
            url: 'picture_url_data.php',
            data: data,
            dataType: 'json',
            success: function ( json ) {
                var picture_url = json.options.PICTURE_URL;
                var picture_url_field = $('#DTE_Field_PICTURE_URL');
                // Set my own picture if running locally
                picture_url =  !picture_url ? '../img/no_photo.png' : is_local ? '../img/duns.jpg' : picture_url;
                if (debug) console.log("picture_url: ", picture_url);
                $('img', picture_url_field).attr( 'src', picture_url);
                callback(json);
            }
        } );

        return {};
    });

    // ROLE
    editor.dependent('ROLE', function ( val, data, _callback, e) {
        if (debug) console.log("Dependency called for ROLE ", val, data, e);

        // Reset fields if we change roles
        var old_val = data.row != undefined ? data.row.ROLE : undefined;
        if (val != old_val) {
            if (debug) console.log("Resetting System, Activity and WBS");
            this.field('SYSTEM_ID').val('');
            this.field('ACTIVITY_ID').val('');
            this.field('WBS_ID').val('');
        }

        switch(val) {
            case "ROLE_ADMIN":
                if (debug) console.log("ADMIN: hide all fields");
                return { hide: ['USERNAME', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID'] };
            case "ROLE_ROLE_MANAGER":
                if (debug) console.log("ROLE_MANAGER: hide all fields");
                return { hide: ['USERNAME', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID'] };
            case "ROLE_ACTIVITY_MANAGER":
                if (debug) console.log("ACTIVITY: show WBS_ID");
                return { show: ['ACTIVITY_ID'], hide: ['USERNAME', 'SYSTEM_ID', 'WBS_ID'] };
            case "ROLE_SYSTEM_MANAGER":
                if (debug) console.log("SYSTEM: show SYSTEM");
                return { show: ['SYSTEM_ID'], hide: ['USERNAME', 'ACTIVITY_ID', 'WBS_ID'] };
            case "ROLE_WBS_MANAGER":
                if (debug) console.log("WBS: show ACTIVITY, WBS");
                return { show: ['ACTIVITY_ID', 'WBS_ID'], hide: ['USERNAME', 'SYSTEM_ID'] };
            default:
                if (debug) console.log("default: hide all fields");
                return { hide: ['USERNAME', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID'] };
        }
    } );

    editor.dependent( 'ACTIVITY_ID', 'wbs_data.php' );

    editor.dependent(['USER_ID', 'ROLE', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID', 'COMMENTS'],
        function ( val, data, callback, e) {
            if (debug) console.log("Dependency called for any ROLE_ASSIGNMENT field update ", val, data, e);

            if (data.values.USER_ID == '' ||
                data.values.ROLE == '' ||
                (data.values.ROLE == 'ROLE_SYSTEM_MANAGER' && data.values.SYSTEM_ID == '') ||
                (data.values.ROLE == 'ROLE_ACTIVITY_MANAGER' && data.values.ACTIVITY_ID == '') ||
                (data.values.ROLE == 'ROLE_WBS_MANAGER' && data.values.WBS_ID == '')) {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-green");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-red");
            } else {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-red");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-green");
            }

            validate();
            return {};
        }
    );

    // Activate an inline edit on click of a table cell
    $('#table').on( 'click', 'tbody td:not(.select-checkbox)', function (e) {
        if (debug) console.log("Clicked any cell except select-checkbox", e);
        // Focus on the input in the cell that was clicked when Editor opens
        editor.one( 'open', () => {
            $('input', this).focus();
        });

        table.buttons( ['.buttons-create'] ).enable(false);
        editor.one('preClose', function(e) {
            if (debug) console.log('preClose', e);
            table.buttons( ['.buttons-create'] ).enable(has_role_admin(current_user, user_roles));
        });

        if (debug) console.log("Enable all editable cells");
        // Only enable the (databasetable) editable fields
        editor.inline(
            table.cells(this.parentNode, ".editable").nodes(), {
                onBlur: 'submit',
            }
        );
    } );

    // Setup DataTable
    var table = new DataTable('#table', {
        lengthChange: false,

        layout: {
            top2Start: 'buttons',
            top2End: 'search',
            top: 'searchPanes',
            topStart: null,
            topEnd: null,
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        ajax: {
            url: data_script,
            dataSrc: function ( json ) {
                current_user = json.user;

                // Update User Roles
                user_roles = decode_user_roles(json.options.ID);
                console.log("User Roles", user_roles);
                console.log("User", json.user, "has_admin_role", json.has_role_admin, "has_role_role_manager", json.has_role_role_manager);

                set_user(json.user, json.has_role_admin, json.has_role_role_manager, user_roles, table);
                update_roles(json.data, current_user, user_roles, table);

                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                return json.data;
            }
        },
        fixedColumns: {
            leftColumns: fixed_columns
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        // switch off scroller and paging in case of a test to make sure it loads all records
        paging:         !is_test,
        scroller:       !is_test,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,    // NOTE: delete local storage for searchpanes to work first time...
        // fuzzySearch: {
        //     toggleSmart: true
        // },

        // for SearchPanes
        columnDefs: [
            {
                searchPanes: {
                    show: false,
                },
                targets: [0, 1, 2, 3, 4, 6, 11, 12, 13],
            },
            {
                searchPanes: {
                    initCollapsed: true,
                    cascadePanes: true,
                    show: true,
                },
                targets: [5, 7, 8, 9, 10],
            }
        ],
        // serverSide: true,

        columns: [
            {
                data: "CAN_EDIT",
                className: "can_edit",
                visible: false,
            },
            {
                data: "CAN_EDIT_BECAUSE",
                className: "can_edit_because",
                visible: false,
            },
            {
                data: "RO",
                className: 'ro'
            },
            {
				data: null,
				defaultContent: '',
				className: 'select',
				orderable: true,
                searchable: false,
//                editField: "USER_ID",
                render: function ( data, type, row ) {
                    // if (debug) console.log(type+" "+data+" "+row.CAN_EDIT);
                    return type == 'sort' ? row.CAN_EDIT : data;
                }
			},
            {
                data: "ID",
                className: 'id'
            },
            {
                data: function ( row, _type, _set ) {
                    // Combine the first and last names into a single table field
                    return `<B>${row.LNAME}</B>, ${row.FNAME}`;
                },
//                editField: "USER_ID",
                className: 'user_id'
            },
            {
                data: "USERNAME",
                className: 'username'
            },
            {
                data: "ROLE",
                render: function ( data, type, _row ) {
                    if (type === 'display') {
                        // Translate Roles to Human Names
                        switch(data) {
                            case "ROLE_ADMIN":
                                return "Admin";
                            case "ROLE_ROLE_MANAGER":
                                return "Role&nbsp;Manager";
                            case "ROLE_ACTIVITY_MANAGER":
                                return "Activity&nbsp;Manager";
                            case "ROLE_SYSTEM_MANAGER":
                                return "System&nbsp;Manager";
                            case "ROLE_WBS_MANAGER":
                                return "WBS&nbsp;Manager";
                            default:
                                return "Unknown&nbsp;Role:" + data;
                        }
                    }
                    return data;
                },
                className: 'role editable',
                defaultContent: ''
            },
            {
                data: "SYSTEM_TITLE",
                render: function ( data, _type, row ) {
                    if (row.ROLE == "ROLE_SYSTEM_MANAGER") {
                        return data;
                    }
                    return null;
                },
                editField: "SYSTEM_ID",
                className: 'system editable',
                defaultContent: ''
            },
            {
                data: "ACTIVITY_TITLE",
                render: function ( data, _type, row ) {
                    if (row.ROLE == "ROLE_ACTIVITY_MANAGER") {
                        return data;
                    }
                    if (row.ROLE == "ROLE_WBS_MANAGER") {
                        return data;
                    }
                    return null;
                },
                editField: "ACTIVITY_ID",
                className: 'activity editable',
                defaultContent: ''
            },
            {
                data: "WBS_TITLE",
                render: function ( data, _type, row ) {
                    if (row.ROLE == "ROLE_WBS_MANAGER") {
                        return data;
                    }
                    return null;
                },
                editField: "WBS_ID",
                className: 'wbs editable',
                defaultContent: ''
            },
            {
                data: "END_DATE",
                className: 'end_date editable',
                render: function (data, _type, _row) {
                    // Remove time component, left YYYY-MM-DD
                    return data.substring(0,10);
                }
            },
            {
                data: function ( row, _type, _set ) {
                    return [ row.EGROUP_NAME, row.APPOINTMENT_NAME ].join('');
                },
                className: 'source',
            },
            {
                data: "COMMENTS",
                className: 'comments editable'
            },
        ],
        order: [[ 5, 'asc' ], [7, 'asc']],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },
        // keys: {
        //     columns: ':not(:first-child)',
        //     keys: [ "\t".charCodeAt(0) ],    // or [ 9 ]
        //     editor: editor,
        //     editOnFocus: true
        // },

        // AutoFill and KeyTable
        // autoFill: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        // keys: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        buttons: [
            {
                extend: "create",
                editor: editor,
                formTitle: "Create New Role",
                formButtons: [
                    { text: 'Create', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            // { extend: "createInline", editor: editor, text: 'New (Inline)' },
            {
                extend: "edit",
                editor: editor,
                formTitle: "Edit Role",
                formButtons: [
                    { text: 'Edit', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: "selected",
                text: "Duplicate",
                className: "buttons-duplicate",
                action: function ( e, dt, node, config ) {
                    // Start in edit mode, and then change to create
                    duplicate = true;
                    editor
                        .edit( table.rows( {selected: true} ).indexes(), {
                            title: 'Duplicate Role',
                            buttons: [
                                { text: 'Create from existing', className: 'submit', action: function () { this.submit(); } },
                                { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                            ]
                        } )
                        .mode( 'create' );
                },
            },
            {
                extend: "remove",
                editor: editor,
                formTitle: "Delete Role",
                formButtons: [
                    { text: 'Delete', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        rowCallback: function( row, data ) {
            if ( data.CAN_EDIT ) {
                $('td.select', row).addClass( 'select-checkbox' );
                $('td.select', row).addClass( 'select-enabled' );
            } else {
                $('td.select', row).removeClass( 'select-checkbox' );
                $('td.select', row).removeClass( 'select-enabled' );
            }
        },
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            var api = this.api();

            count_selected(api, selected_row_count, 3);
            count_unique(api, 6);
            count_unique(api, 7);
            count_unique(api, 8);
            count_unique(api, 9);
            count_unique(api, 10);
        }
    } );

    table.on( 'draw select deselect', function () {
        if (debug) console.log('draw select deselect');
        var enable_new = has_role_admin(current_user, user_roles);
        var enable_edit = has_role_admin(current_user, user_roles);
        var enable_remove = has_role_admin(current_user, user_roles);

        var selectedRows = table.rows( { selected: true } );
        selected_row_count = selectedRows.count();
        enable_edit = enable_edit && (selected_row_count > 0);
        table.rows( { selected: true } ).every( function ( _rowIdx, _tableLoop, _rowLoop ) {
            enable_edit = enable_edit && (this.data().USERNAME != '');
        });

        enable_new = enable_new && !enable_edit;
        enable_remove = enable_remove && (selected_row_count > 0);

        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> new enabled", enable_new);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> edit enabled", enable_edit);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> remove enabled", enable_remove);

        count_selected(table, selected_row_count, 3);

        table.buttons( ['.buttons-create'] ).enable(enable_new);
        table.buttons( ['.buttons-edit'] ).enable(enable_edit);
        table.buttons( ['.buttons-duplicate'] ).enable(enable_new);
        table.buttons( ['.buttons-remove'] ).enable(enable_remove);
    });

    // table.scrollX = !is_test;
    table.paging = !is_test;
    table.scroller = !is_test;
    table.columns( [0,1,2,4] ).visible( debug );


    // Display the buttons
    // bootstrap specific
    // new $.fn.dataTable.Buttons( table, [
    //     { extend: "create", editor: editor },
    //     // FIXME: disabled as we cannot disable 'datatable' type
    //     { extend: "edit",   editor: editor },
    //     { extend: "remove", editor: editor }
    // ] );

    // Bootstrap specific
    // table.buttons().container().appendTo( $('.col-md-6:eq(0)', table.table().container() ) );

 } );
