// Image field type plug-in code
(function ($, DataTable) {

    if ( ! DataTable.ext.editorFields ) {
        DataTable.ext.editorFields = {};
    }

    var Editor = DataTable.Editor;
    var _fieldTypes = DataTable.ext.editorFields;

    _fieldTypes.image = {
        create: function ( conf ) {
            // console.log("Create", conf);

            conf._enabled = true;

            // Create the elements to use for the input
            conf._input = $(
                '<div id="'+Editor.safeId( conf.id )+'">'+
                    '<img src="'+'../img/no_photo.png'+'"/ width="60px" height="75px">'+
                '</div>');

            return conf._input;
        },

        get: function ( conf ) {
            // console.log("Get", conf);
            // return $('img', conf._input).attr( 'src'
            return null;
        },

        set: function ( conf, val ) {
            // console.log("Set", conf, val);
            // console.log($('img', conf._input).attr( 'src'));
            // $('img', conf._input).attr( 'src', val );
        },

        enable: function ( conf ) {
            // console.log("Enable", conf);
            conf._enabled = true;
            $(conf._input).removeClass( 'disabled' );
        },

        disable: function ( conf ) {
            // console.log("Disable", conf);
            conf._enabled = false;
            $(conf._input).addClass( 'disabled' );
        }
    };

})(jQuery, jQuery.fn.dataTable);

var editor; // use a global for the submit and return data rendering
var debug;
var current_user;
var data_script = "extra_roles_data.php";
var duplicate = false;
var user_roles;

function add_can_edit(data, user, user_roles) {
    current_user = user;

    for (var i = 0; i < data.length; i++) {
        var can_edit_and_because = can_edit(user, user_roles, '', '', ''); // FIXME: json.data[i].SYSTEM_ID, json.data[i].WBS_ID);
        data[i].CAN_EDIT = can_edit_and_because.can_edit;
        data[i].CAN_EDIT_BECAUSE = can_edit_and_because.can_edit_because;
    }
}

$(document).ready(function() {
    var queryString = window.location.search;
    let params = new URLSearchParams(queryString);

    // query string ?test
    let is_test = params.get("test") !== null;

    // query string ?debug
    debug = params.get("debug") !== null;

    let is_local = window.location.hostname == 'localhost' || window.location.hostname == 'otp-next.localhost';

    if (debug) {
        console.log(window.location);
        console.log("test = ", is_test);
        console.log("debug = ", debug);
        console.log("local = ", is_local);
    }

    // Setup Editor
    editor = new DataTable.Editor({
        ajax: {
            url: data_script,
            // Modify what we send if needed
            data: function ( d ) {
                // For CREATE and DELETE no modifications needed
                if ((d.action == 'create') || (d.action == 'remove')) {
                    if (debug) console.log("Data unchanged for CREATE or REMOVE");
                    return;
                }

                // NOTE below breaks double editing as 'table' changes
                // related test: test_edit_inline_system_twice
                // Send also initial values before change in 'init_data'
                // to enable to detect (and possibly resolve) conflicts (a la git)
                // [row, value] = Object.entries(d.data)[0];
                // [table, value] = Object.entries(value)[0];
                // [field, value] = Object.entries(value)[0];

                // // if (debug) console.log(editor.init_data['EXTRA_ROLES']);
                // // FIXME check for row, for multi-submit
                // d.init_data = {}
                // d.init_data[row] = {}
                // d.init_data[row][table] = {}
                // d.init_data[row][table][field] = editor.init_data[table][field]

                if (debug) console.log("Data populated for EDIT (currently disabled)");
                if (debug) console.log(d);
            }
        },
        table: "#table",
        fields: [
            // all Editable fields
            {
                // TBD
                label: "Name:",
                name: "EXTRA_ROLES.USER_ID",
                type: "datatable",
                // TBD
                config: {
                    paging:         true,
                    scrollY:        150,
                    scrollCollapse: true,
                    deferRender:    true,
                    scroller:       true,
                    columns: [
                        {
                            title: '<b>Last name</b>, First name',
                            data: 'label'
                        }
                    ]
                }
            }, {
                label: "Username:",
                name: "PUB_PERSON.USERNAME",
            }, {
                label: "Photo:",
                name: "PUB_PERSON.PICTURE_URL",
                type: "image"
            }, {
                label: "Role:",
                name: "EXTRA_ROLES.ROLE",
                type: "select",
                def: "ROLE_WBS_MANAGER",
                placeholder: "Select a Role",
                placeholderValue: ""
            }, {
                // For: ROLE_SYSTEM_MANAGER
                label: "System:",
                name: "EXTRA_ROLES.SYSTEM_ID",
                type: "select",
                def: "",
                placeholder: "Select a System",
                placeholderValue: ""
            }, {
                // For: ACTIVTY
                label: "Activity:",
                name: "ACTIVITY.ID",
                type: "select",
                def: "",
                placeholder: "Select an Activity",
                placeholderValue: ""
            }, {
                // For: ROLE_WBS_MANAGER
                label: "Work Breakdown Structure:",
                name: "EXTRA_ROLES.WBS_ID",
                type: "select",
                def: "",
                placeholder: "Select a WBS",
                placeholderValue: ""
            }, {
                label: "End Date:",
                name: "EXTRA_ROLES.END_DATE",
                type:  'datetime',
                def:   function () {
                    // suggested end date is 2 years from today
                    var end_date = new Date();
                    end_date.setFullYear(end_date.getFullYear() + 2);
                    return end_date;
                },
                opts:  {
                    // disableDays: [ 0, 6 ],
                    minDate: new Date(),
                    buttons: {
                        today: false,
                        clear: true
                    }
                }
            }, {
                label: "Comments:",
                name: "EXTRA_ROLES.COMMENTS",
                def: ""
            }
        ]
    } );

    editor.on('initEdit', function (_e, node, data, items, type) {
        // Called just before editing (inline or normal) is done.
        if (debug) console.log('initEdit', node, type, data, items);

        // TBD Store initial data before change (as prepared in ajax.data)
        // to enable to detect (and possibly resolve) conflicts (a la git)
        editor.init_data = data;
    });

    editor.on('preOpen', function(e, mode, action) {
        // Called before a form (inline or normal) is opened. Called after initEdit.
        if (debug) console.log('preOpen', mode, action, e);

        if (action === 'create') {
            if (!has_role_admin(current_user, user_roles)) {
                if (debug) console.log("No new roles can be created if not ADMIN");
                return false;
            }
        } else {
            // NOTE: editor.field('CAN_EDIT').val() is not filled at first select, so we use the datatable
            var modifier = editor.modifier();
            var data = table.row( modifier ).data();

            // No edits/deletes if CAN_EDIT field is false
            if (data && (typeof data !== "undefined") && !data.CAN_EDIT) {
                if (debug) console.log("No edits if CAN_EDIT field is false");
                return false;
            }

            // No edits possible if no username
            if ((data.PUB_PERSON.USERNAME == '') && (action == 'edit')) {
                if (debug) console.log("No edits possible if no username");
                return false;
            }
        }

        do_validate = false;

        set_change_handler(editor, 'EXTRA_ROLES.SYSTEM_ID', mode, debug);
        set_change_handler(editor, 'EXTRA_ROLES.WBS_ID', mode, debug);
        set_change_handler(editor, 'EXTRA_ROLES.END_DATE', mode, debug);

        // Final select on ActivityId (if activity manager) should submit
        editor.field('ACTIVITY.ID').input().off('change');
        if (mode === 'inline') {
            editor.field('ACTIVITY.ID').input().on('change', function(e, data) {
                if (!data || !data.editor) {
                    if (editor.field('EXTRA_ROLES.ROLE').val() == 'ROLE_ACTIVITY_MANAGER') {
                        if (debug) console.log("Select Submit on activity manager ActivityId");
                        editor.submit();
                    }
                }
            } );
        }

    } );

    var do_validate = false;

    function validate() {
        if (do_validate) {

            // Check for valid USER_ID
            var user_id = editor.field('EXTRA_ROLES.USER_ID');
            user_id.error(user_id.val() == "" ? "Select a User": "");

            // Check for valid ROLE and
            // either SYSTEM_ID or ACTIVITY.ID or both ACTIVITY.ID and WBS_ID
            var role = editor.field('EXTRA_ROLES.ROLE');
            var system_id = editor.field('EXTRA_ROLES.SYSTEM_ID');
            var activity_id = editor.field('ACTIVITY.ID');
            var wbs_id = editor.field('EXTRA_ROLES.WBS_ID');

            var activity, system, wbs;
            switch(role.get()) {
                case "ROLE_ADMIN":
                    // ADMIN no further fields needed
                    role.error("");
                    activity_id.error("");
                    system_id.error("");
                    wbs_id.error("");
                    break;
                case "ROLE_ACTIVITY_MANAGER":
                    role.error("");
                    system_id.error("");
                    wbs_id.error("");
                    // Check for valid ACTIVITY
                    activity = parseInt(activity_id.val());
                    switch(isNaN(activity) || activity) {
                        case 1:
                        case 2:
                        case 3:
                        case 6:
                        case 24:
                            activity_id.error("");
                            break;
                        case true:
                        default:
                            activity_id.error("Select an Activity");
                            break;
                    }
                    break;
                case "ROLE_SYSTEM_MANAGER":
                    role.error("");
                    activity_id.error("");
                    wbs_id.error("");
                    // Check for valid SYSTEM
                    system = parseInt(system_id.val());
                    system_id.error(isNaN(system) ? "Select a System" : "");
                    break;
                case "ROLE_WBS_MANAGER":
                    role.error("");
                    system_id.error("");
                    // Check for valid ACTIVITY
                    activity = parseInt(activity_id.val());
                    switch(isNaN(activity) || activity) {
                        case 1:
                        case 2:
                        case 3:
                        case 6:
                        case 24:
                            activity_id.error("");
                            break;
                        case true:
                        default:
                            activity_id.error("Select an Activity");
                            break;
                    }

                    // Check for valid WBS except ACTIVITY
                    wbs = parseInt(wbs_id.val());
                    switch(isNaN(wbs) || wbs) {
                        case 1:
                        case 2:
                        case 3:
                        case 6:
                        case 24:
                        case true:
                            wbs_id.error("Select a WBS");
                            break;
                        default:
                            wbs_id.error("");
                            break;
                    }
                    break;
                default:
                    role.error("Select an Role");
                    system_id.error("");
                    activity_id.error("");
                    wbs_id.error("");
                    break;
            }
        }
    }

    editor.on('preSubmit', function(e, data, action) {
        if (debug) console.log('preSubmit', e, action, data);

        do_validate = true;

        // Validate fields before submitting to server

        // DELETE is ok
        if (action == "remove") {
            if (debug) console.log("DELETE is ok");
            return true;
        }

        // Add extra data to edit row
        if (action === 'edit') {
            if ("EXTRA_ROLES" in data.data[ editor.init_data.DT_RowId ]) {
                data.data[ editor.init_data.DT_RowId ].EXTRA_ROLES.ID = editor.init_data.EXTRA_ROLES.ID;
                data.data[ editor.init_data.DT_RowId ].EXTRA_ROLES.USER_ID = editor.init_data.EXTRA_ROLES.USER_ID;
            }

            // make sure wbs_id is set to activity_id if role is activity_manager
            if (editor.init_data.EXTRA_ROLES.ROLE == 'ROLE_ACTIVITY_MANAGER') {
                data.data[ editor.init_data.DT_RowId ].EXTRA_ROLES = {};
                data.data[ editor.init_data.DT_RowId ].EXTRA_ROLES.WBS_ID = editor.init_data.ACTIVITY.ID;
            }
        }

        if (action === 'create') {
            data.data[0].EXTRA_ROLES.ID = 0;

            // make sure wbs_id is set to activity_id if role is activity_manager
            if (data.data[ 0 ].EXTRA_ROLES.ROLE == 'ROLE_ACTIVITY_MANAGER') {
                data.data[ 0 ].EXTRA_ROLES.WBS_ID = data.data[ 0 ].ACTIVITY.ID;
            }
        }

        validate();

        // Any missing fields, abort submit
        if ( this.inError() ) {
            if (debug) console.log("Some error prevented submit");
            return false;
        }

        return true;
    });

    // handle server errors
    editor.on('postSubmit', function (_e, json, data, action, xhr) {
        if (debug) console.log("postSubmit", action, json, data, xhr);

        if (json === null) {
            console.log(data);
            console.log('responseText:', xhr.responseText);
            alert("Server error in script: " + data_script + ", look at console log.");
        }

        update_roles(json.data, current_user, user_roles, table);

        //   if ( json.FieldErrors ) {
        //      .... take a look at json.fieldErrors
        //   }
    });

    // jump to new or edited record
    editor.on( 'submitComplete', function ( e, json, data, action ) {
        if (debug) console.log("submitComplete", data, action);

        if ( action === 'create' || action === 'edit' ) {
            if (is_test) {
                $(`#${data.DT_RowId}`).addClass("test_extra_roles_record");
            }
            table.row(`#${data.DT_RowId}`).scrollTo();
        }

        duplicate = false;
    });

    //  USER_ID -> PICTURE_URL
    editor.dependent('EXTRA_ROLES.USER_ID', function ( val, data, callback, e) {
        if (debug) console.log("Dependency called for USER_ID -> PICTURE_URL", val, data, e);
        $.ajax( {
            url: 'extra_roles_picture_url_data.php',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function ( json ) {
                var picture_url = json.options.PICTURE_URL;
                var picture_url_field = $('#DTE_Field_PUB_PERSON-PICTURE_URL');
                //console.log(picture_url_field);
                if (debug) console.log("picture_url: ", picture_url);
                // Set my own picture if running locally
                picture_url =  !picture_url ? '../img/no_photo.png' : is_local ? '../img/duns.jpg' : picture_url;
                $('img', picture_url_field).attr( 'src', picture_url);
                // callback(json);
            }
        } );

        return {};
    });

    // ROLE
    editor.dependent('EXTRA_ROLES.ROLE', function ( val, data, _callback, e) {
        if (debug) console.log("Dependency called for ROLE ", val, data, e);

        // Reset fields if we change roles
        var old_val = data.row != undefined ? data.row.EXTRA_ROLES.ROLE : undefined;
        if (val != old_val) {
            if (debug) console.log("Resetting System, Activity and WBS");
            this.field('EXTRA_ROLES.SYSTEM_ID').val('');
            this.field('ACTIVITY.ID').val('');
            this.field('EXTRA_ROLES.WBS_ID').val('');
        }

        switch(val) {
            case "ROLE_ADMIN":
                if (debug) console.log("ADMIN: hide all fields");
                return { hide: ['PUB_PERSON.USERNAME', 'EXTRA_ROLES.SYSTEM_ID', 'ACTIVITY.ID', 'EXTRA_ROLES.WBS_ID'] };
            case "ROLE_ACTIVITY_MANAGER":
                if (debug) console.log("ACTIVITY: show WBS_ID");
                return { show: ['ACTIVITY.ID'], hide: ['PUB_PERSON.USERNAME', 'EXTRA_ROLES.SYSTEM_ID', 'EXTRA_ROLES.WBS_ID'] };
            case "ROLE_SYSTEM_MANAGER":
                if (debug) console.log("SYSTEM: show SYSTEM");
                return { show: ['EXTRA_ROLES.SYSTEM_ID'], hide: ['PUB_PERSON.USERNAME', 'ACTIVITY.ID', 'EXTRA_ROLES.WBS_ID'] };
            case "ROLE_WBS_MANAGER":
                if (debug) console.log("WBS: show ACTIVITY, WBS");
                return { show: ['ACTIVITY.ID', 'EXTRA_ROLES.WBS_ID'], hide: ['PUB_PERSON.USERNAME', 'EXTRA_ROLES.SYSTEM_ID'] };
            default:
                if (debug) console.log("default: hide all fields");
                return { hide: ['PUB_PERSON.USERNAME', 'EXTRA_ROLES.SYSTEM_ID', 'ACTIVITY.ID', 'EXTRA_ROLES.WBS_ID'] };
        }
    } );

    editor.dependent( 'ACTIVITY.ID', 'extra_roles_wbs_data.php' );

    editor.dependent(['EXTRA_ROLES.USER_ID', 'EXTRA_ROLES.ROLE', 'EXTRA_ROLES.SYSTEM_ID', 'ACTIVITY.ID', 'EXTRA_ROLES.WBS_ID', 'EXTRA_ROLES.COMMENTS'],
        function ( val, data, callback, e) {
            if (debug) console.log("Dependency called for any EXTRA_ROLES or ACTIVITY field update ", val, data, e);

            if (data.values['EXTRA_ROLES.USER_ID'] == '' ||
                data.values['EXTRA_ROLES.ROLE'] == '' ||
                (data.values['EXTRA_ROLES.ROLE'] == 'ROLE_SYSTEM_MANAGER' && data.values['EXTRA_ROLES.SYSTEM_ID'] == '') ||
                (data.values['EXTRA_ROLES.ROLE'] == 'ROLE_ACTIVITY_MANAGER' && data.values['ACTIVITY.ID'] == '') ||
                (data.values['EXTRA_ROLES.ROLE'] == 'ROLE_WBS_MANAGER' && data.values['EXTRA_ROLES.WBS_ID'] == '')) {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-green");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-red");
            } else {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-red");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-green");
            }

            validate();
            return {};
        }
    );

    // Activate an inline edit on click of a table cell
    $('#table').on( 'click', 'tbody td:not(.select-checkbox)', function (e) {
        if (debug) console.log("Clicked any cell except select-checkbox", e);
        // Focus on the input in the cell that was clicked when Editor opens
        editor.one( 'open', () => {
            $('input', this).focus();
        });

        table.buttons( ['.buttons-create'] ).enable(false);
        editor.one('preClose', function(e) {
            if (debug) console.log('preClose', e);
            table.buttons( ['.buttons-create'] ).enable(has_role_admin(current_user, user_roles));
        });

        if (debug) console.log("Enable all editable cells");
        // Only enable the (databasetable) editable fields
        editor.inline(
            table.cells(this.parentNode, ".editable").nodes(), {
                onBlur: 'submit',
            }
        );
    } );

    var selected_row_count = 0;

    // Setup DataTable
    var table = new DataTable('#table', {
        lengthChange: false,
        layout: {
            top2Start: 'buttons',
            top2End: 'search',
            top: 'searchPanes',
            topStart: null,
            topEnd: null,
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        ajax: {
            url: data_script,
            dataSrc: function ( json ) {
                current_user = json.user;

                // Update User Roles
                user_roles = decode_user_roles(json.options["EXTRA_ROLES.ID"]);
                console.log("User Roles", user_roles);

                set_user(json.user, json.has_role_admin, json.has_role_role_manager, user_roles, table);
                update_roles(json.data, current_user, user_roles, table);

                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                return json.data;
            }
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        // switch off scroller and paging in case of a test to make sure it loads all records
        paging:         !is_test,
        scroller:       !is_test,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,    // NOTE: delete local storage for searchpanes to work first time...
        // fuzzySearch: {
        //     toggleSmart: true
        // },

        // for SearchPanes
        columnDefs: [
            {
                searchPanes: {
                    show: false,
                },
                targets: [0, 1, 2, 3, 5, 10, 11],
            },
            {
                searchPanes: {
                    initCollapsed: true,
                    cascadePanes: true,
                    show: true,
                },
                targets: [4, 6, 7, 8, 9],
            }
        ],
        // serverSide: true,

        columns: [
            {
                data: "CAN_EDIT",
                className: "can_edit",
                visible: false,
            },
            {
                data: "CAN_EDIT_BECAUSE",
                className: "can_edit_because",
                visible: false,
            },
            {
				data: null,
				defaultContent: '',
				className: 'select',
				orderable: true,
                searchable: false,
                editField: "EXTRA_ROLES.USER_ID",
                render: function ( data, type, row ) {
                    // if (debug) console.log(type+" "+data+" "+row.CAN_EDIT);
                    return type == 'sort' ? row.CAN_EDIT : data;
                }
			},
            {
                data: "EXTRA_ROLES.ID",
                className: 'id'
            },
            {
                data: null,
                render: function ( data, _type, _row ) {
                    // Combine the first and last names into a single table field
                    return data.PUB_PERSON.LNAME + ', ' + data.PUB_PERSON.FNAME;
                },
                editField: "EXTRA_ROLES.USER_ID",
                className: 'user_id'
            },
            {
                data: "PUB_PERSON.USERNAME",
                className: 'username'
            },
            {
                data: "EXTRA_ROLES.ROLE",
                render: function ( data, type, _row ) {
                    if (type === 'display') {
                        // Translate Roles to Human Names
                        switch(data) {
                            case "ROLE_ADMIN":
                                return "Admin";
                            case "ROLE_ACTIVITY_MANAGER":
                                return "Activity&nbsp;Manager";
                            case "ROLE_SYSTEM_MANAGER":
                                return "System&nbsp;Manager";
                            case "ROLE_WBS_MANAGER":
                                return "WBS&nbsp;Manager";
                            case "ROLE_ROLE_MANAGER":
                                return "Role&nbsp;Manager";
                            default:
                                return "Unknown&nbsp;Role";
                        }
                    }
                    return data;
                },
                className: 'role editable',
                defaultContent: ''
            },
            {
                data: "SYSTEM_NODE.TITLE",
                render: function ( data, _type, row ) {
                    if (row.EXTRA_ROLES.ROLE == "ROLE_SYSTEM_MANAGER") {
                        return data;
                    }
                    return null;
                },
                editField: "EXTRA_ROLES.SYSTEM_ID",
                className: 'system editable',
                defaultContent: ''
            },
            {
                data: "ACTIVITY.TITLE",
                render: function ( data, _type, row ) {
                    if (row.EXTRA_ROLES.ROLE == "ROLE_ACTIVITY_MANAGER") {
                        return data;
                    }
                    if (row.EXTRA_ROLES.ROLE == "ROLE_WBS_MANAGER") {
                        return data;
                    }
                    return null;
                },
                editField: "ACTIVITY.ID",
                className: 'activity editable',
                defaultContent: ''
            },
            {
                data: "WBS_NODE.TITLE",
                render: function ( data, _type, row ) {
                    if (row.EXTRA_ROLES.ROLE == "ROLE_WBS_MANAGER") {
                        return data;
                    }
                    return null;
                },
                editField: "EXTRA_ROLES.WBS_ID",
                className: 'wbs editable',
                defaultContent: ''
            },
            {
                data: "EXTRA_ROLES.END_DATE",
                className: 'end_date editable',
                render: function (data, _type, _row) {
                    // Remove time component, left YYYY-MM-DD
                    return data.substring(0,10);
                }
            },
            {
                data: "EXTRA_ROLES.COMMENTS",
                className: 'comments editable'
            },
        ],
        order: [[ 4, 'asc' ], [6, 'asc']],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },

        // AutoFill and KeyTable
        // autoFill: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        // keys: {
        //     columns: ':not(:first-child)',
        //     editor:  editor
        // },
        buttons: [
            {
                extend: "create",
                editor: editor,
                formButtons: [
                    { text: 'Create', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            // { extend: "createInline", editor: editor, text: 'New (Inline)' },
            {
                extend: "edit",
                editor: editor,
                formButtons: [
                    { text: 'Edit', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: "selected",
                text: 'Duplicate',
                action: function ( e, dt, node, config ) {
                    // Start in edit mode, and then change to create
                    duplicate = true;
                    editor
                        .edit( table.rows( {selected: true} ).indexes(), {
                            title: 'Duplicate record',
                            buttons: [
                                { text: 'Create from existing', className: 'submit', action: function () { this.submit(); } },
                                { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                            ]
                        } )
                        .mode( 'create' );
                },
            },
            {
                extend: "remove",
                editor: editor,
                formButtons: [
                    { text: 'Delete', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        rowCallback: function( row, data ) {
            if ( data.CAN_EDIT ) {
                $('td.select', row).addClass( 'select-checkbox' );
                $('td.select', row).addClass( 'select-enabled' );
            } else {
                $('td.select', row).removeClass( 'select-checkbox' );
                $('td.select', row).removeClass( 'select-enabled' );
            }
        },
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            var api = this.api();

            count_selected(api, selected_row_count, 2);
            count_unique(api, 5);
            count_unique(api, 6);
            count_unique(api, 7);
            count_unique(api, 8);
            count_unique(api, 9);
        }
    } );

    table.on( 'draw select deselect', function () {
        var enable_new = has_role_admin(current_user, user_roles);
        var enable_edit = has_role_admin(current_user, user_roles);
        var enable_remove = has_role_admin(current_user, user_roles);

        var selectedRows = table.rows( { selected: true } );
        selected_row_count = selectedRows.count();
        enable_edit = enable_edit && (selected_row_count > 0);
        table.rows( { selected: true } ).every( function ( _rowIdx, _tableLoop, _rowLoop ) {
            enable_edit = enable_edit && (this.data().PUB_PERSON.USERNAME != '');
        });

        enable_new = enable_new && !enable_edit;
        enable_remove = enable_remove && (selected_row_count > 0);

        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> new enabled", enable_new);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> edit enabled", enable_edit);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> remove enabled", enable_remove);

        count_selected(table, selected_row_count, 2);

        table.buttons( ['.buttons-create'] ).enable(enable_new);
        table.buttons( ['.buttons-edit'] ).enable(enable_edit);
        table.buttons( ['.buttons-remove'] ).enable(enable_remove);
    });

    // table.scrollX = !is_test;
    table.paging = !is_test;
    table.scroller = !is_test;
    table.columns( [0,1,3] ).visible( debug );
 } );
