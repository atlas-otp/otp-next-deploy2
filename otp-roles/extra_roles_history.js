function role(data, type=null, _row=null) {
    // Translate Roles to Human Names
    switch(data) {
        case "ROLE_ADMIN":
            return "Admin";
        case "ROLE_ACTIVITY_MANAGER":
            return "Activity&nbsp;Manager";
        case "ROLE_SYSTEM_MANAGER":
            return "System&nbsp;Manager";
        case "ROLE_WBS_MANAGER":
            return "WBS&nbsp;Manager";
        case "ROLE_ROLE_MANAGER":
            return "Role&nbsp;Manager";
        default:
            return "Unknown&nbsp;Role";
    }
    return data;
}

function system( data, role ) {
    if (role == "ROLE_SYSTEM_MANAGER") {
        return data;
    }
    return null;
}

function activity( data, role ) {
    if (role == "ROLE_ACTIVITY_MANAGER") {
        return data;
    }
    if (role == "ROLE_WBS_MANAGER") {
        return data;
    }
    return null;
}

function wbs(data, role) {
    if (role == "ROLE_WBS_MANAGER") {
        return data;
    }
    return null;
}

function date(data) {
    // Remove time component, left YYYY-MM-DD
    return data != null ? data.substring(0,10) : data;
}

function person(lname, fname) {
    return lname + ', ' + fname;
}

function creperson(data, creuser) {
    // Combine the first and last names into a single table field
    if (data == null) return null;

    if ('CREPERSON' in data) {
        return person(data.CREPERSON.LNAME, data.CREPERSON.FNAME);
    } else {
        return creuser;
    }
}

function crud(t_delete, t_pk_prev) {
    if (t_delete == 'Y') {
        return "Delete";
    }

    if (t_pk_prev == null) {
        return "Create";
    }

    return  "Update";
}

function comments(data) {
    return data != null ? "'" + data + "'" : ""
}

$(document).ready(function() {
    var debug = false;

    // Setup DataTable
    var table = $('#table_history').DataTable( {
        lengthChange: false,
        layout: {
            topStart: 'buttons',
            topEnd: 'search',
            bottom: null,
            bottomStart: 'info',
            bottomEnd: null
        },
        ajax: {
            url: "extra_roles_history_data.php",
            type: "POST",
            dataSrc: function ( json ) {
                var db = database(json.db);
                $('#table_db').text(db.text);
                $('#table_db').addClass(db.clazz);

                console.log(json.options);
                var user_roles = decode_user_roles(json.options['EXTRA_ROLES_AVER.ID']);
                set_user(json.user, false,false, user_roles, table);

                return json.data;
            }
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        paging:         true,
        scroller:       true,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,    // NOTE: delete local storage for searchpanes to work first time...
        // fuzzySearch: {
        //     toggleSmart: true
        // },

        // for SearchPanes
        // columnDefs: [
        //     {
        //         searchPanes: {
        //             show: false,
        //         },
        //         targets: [0, 1, 2, 3, 5, 10, 11],
        //     },
        //     {
        //         searchPanes: {
        //             initCollapsed: true,
        //             cascadePanes: true,
        //             show: true,
        //         },
        //         targets: [4, 6, 7, 8, 9],
        //     }
        // ],
        // serverSide: true,

        columns: [
            {
                data: "EXTRA_ROLES_AVER.T_WORKID",
                className: 't_workid'
            },
            {
                data: "EXTRA_ROLES_AVER.T_PK",
                className: 't_pk'
            },
            {
                data: "EXTRA_ROLES_AVER.T_PK_PREV",
                className: 't_pk_prev'
            },
            {
                data: "EXTRA_ROLES_AVER.A_CREUSER",
                className: 'a_creuser',
                visible: debug
            },
            {
                data: null,
                render: function( data, _type, row ) {
                    return creperson(data, row.EXTRA_ROLES_AVER.A_CREUSER);
                },
                className: 'creperson',
            },
            {
                data: "EXTRA_ROLES_AVER.A_CREDATE",
                className: 'a_credate'
            },
            {
                data: "EXTRA_ROLES_AVER.T_START",
                className: 't_start',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.T_STATUS",
                className: 't_status',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.T_DELETE",
                className: 't_delete',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.IS_CURRENT_VERSION",
                className: 'is_current_version',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.IS_CURRENT_AUTH_VERSION",
                className: 'is_current_auth_version',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.VERSION",
                className: 'version'
            },

            {
                data: null,
                render: function( _data, _type, row ) {
                    return crud(row.EXTRA_ROLES_AVER.T_DELETE, row.EXTRA_ROLES_AVER.T_PK_PREV);
                },
                className: 'type',
            },

            {
                data: null,
                render: function( data, _type, row ) {
                    var result = "<ul>";

                    var type = crud(row.EXTRA_ROLES_AVER.T_DELETE, row.EXTRA_ROLES_AVER.T_PK_PREV);

                    if (row.EXTRA_ROLES_AVER.T_STATUS == "R") {
                        result += "<li>Record was REVOKED</li>"
                    }

                    if (row.EXTRA_ROLES_AVER.T_STATUS == "N") {
                        result += "<li>Record is NEW</li>"
                    }

                    if (row.EXTRA_ROLES_AVER.T_DELETE == 'Y') {
                        result += "<i>"
                    }

                    if (row.EXTRA_ROLES_AVER.IS_CURRENT_VERSION == 'Y') {
                        result += "<b>"
                    }

                    switch(type) {
                        case "Create":
                        case "Delete":
                            result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": "
                                    + person(data.PUB_PERSON.LNAME, data.PUB_PERSON.FNAME)
                                    + ", "
                                    + role(row.EXTRA_ROLES_AVER.ROLE)
                                    + ", "
                                    + system(row.SYSTEM_NODE.TITLE, row.EXTRA_ROLES_AVER.ROLE)
                                    + ", "
                                    + row.ACTIVITY.TITLE
                                    + ":"
                                    + row.WBS_NODE.TITLE
                                    + ", "
                                    + date(row.EXTRA_ROLES_AVER.END_DATE)
                                    + ", "
                                    + "'" + row.EXTRA_ROLES_AVER.COMMENTS + "'"
                                    + "</li>";
                            break;
                        case "Update":

                            if (row.EXTRA_ROLES_AVER.ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": ID changed from "
                                    + row.EXTRA_ROLES_AVER.ID_PREV
                                    + " to "
                                    + row.EXTRA_ROLES_AVER.ID
                                    + "</li>";
                            }

                            if (row.EXTRA_ROLES_AVER.USER_ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": USER changed from "
                                    + person(data.PUB_PERSON_PREV.LNAME, data.PUB_PERSON_PREV.FNAME)
                                    + " to "
                                    + person(data.PUB_PERSON.LNAME, data.PUB_PERSON.FNAME)
                                    + "</li>";
                            }

                            if (row.EXTRA_ROLES_AVER.ROLE_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": ROLE changed from "
                                    + role(row.EXTRA_ROLES_AVER.ROLE_PREV)
                                    + " to "
                                    + role(row.EXTRA_ROLES_AVER.ROLE)
                                    + "</li>";
                            }

                            if (row.EXTRA_ROLES_AVER.SYSTEM_ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": SYSTEM changed from "
                                    + system(row.SYSTEM_NODE_PREV.TITLE)
                                    + " to "
                                    + system(row.SYSTEM_NODE.TITLE)
                                    + "</li>";
                            }

                            if (row.EXTRA_ROLES_AVER.WBS_ID_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": ACTIVITY:WBS changed from "
                                    + activity(row.ACTIVITY_PREV.TITLE, row.EXTRA_ROLES_AVER.ROLE_PREV)
                                    + ":"
                                    + wbs(row.WBS_NODE_PREV.TITLE, row.EXTRA_ROLES_AVER.ROLE_PREV)
                                    + " to "
                                    + activity(row.ACTIVITY.TITLE, row.EXTRA_ROLES_AVER.ROLE)
                                    + ":"
                                    + wbs(row.WBS_NODE.TITLE, row.EXTRA_ROLES_AVER.ROLE)
                                    + "</li>";
                            }

                            if (row.EXTRA_ROLES_AVER.END_DATE_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": END_DATE changed from "
                                    + date(row.EXTRA_ROLES_AVER.END_DATE_PREV)
                                    + " to "
                                    + date(row.EXTRA_ROLES_AVER.END_DATE)
                                    + "</li>";
                            }

                            if (row.EXTRA_ROLES_AVER.COMMENTS_CHANGED == "Y") {
                                result += "<li>"
                                    + row.EXTRA_ROLES_AVER.ID
                                    + ": COMMENTS changed from "
                                    + comments(row.EXTRA_ROLES_AVER.COMMENTS_PREV)
                                    + " to "
                                    + comments(row.EXTRA_ROLES_AVER.COMMENTS)
                                    + "</li>";
                            }
                        break;
                    }

                    if (row.EXTRA_ROLES_AVER.IS_CURRENT_VERSION == 'Y') {
                        result += "</b>"
                    }

                    if (row.EXTRA_ROLES_AVER.T_DELETE == 'Y') {
                        result += "</i>"
                    }

                    result += "</ul>"
                    return result;
                },
                className: 'changed'
            },

            {
                data: "EXTRA_ROLES_AVER.ID_CHANGED",
                className: 'id_changed',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.ID_PREV",
                className: 'id_prev',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.ID",
                className: 'id',
                visible: debug
            },

            {
                data: null,
                render: function( data, _type, _row ) {
                    // Combine the first and last names into a single table field
                    return data != null ? data.PUB_PERSON_PREV.LNAME + ', ' + data.PUB_PERSON_PREV.FNAME : data;
                },
                className: 'user_prev',
                visible: debug
            },
            {
                data: null,
                render: function( data, _type, _row ) {
                    // Combine the first and last names into a single table field
                    return data != null ? data.PUB_PERSON.LNAME + ', ' + data.PUB_PERSON.FNAME : data;
                },
                className: 'user',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.USER_ID_CHANGED",
                className: 'user_id_changed',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.USER_ID_PREV",
                className: 'user_id_prev',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.USER_ID",
                className: 'user_id',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.ROLE_CHANGED",
                className: 'role_changed',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.ROLE_PREV",
                className: 'role_prev',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.ROLE",
                render: function( data, _type, _row ) {
                    return role(data);
                },
                className: 'role',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.SYSTEM_ID_CHANGED",
                className: 'system_changed',
                visible: debug
            },
            {
                data: "SYSTEM_NODE_PREV.TITLE",
                render: function ( data, _type, row ) {
                    return system(data, row.EXTRA_ROLES_AVER.ROLE_PREV);
                },
                className: 'system_prev',
                visible: debug
            },
            {
                data: "SYSTEM_NODE.TITLE",
                render: function ( data, _type, row ) {
                    return system(data, row.EXTRA_ROLES_AVER.ROLE);
                },
                className: 'system',
                visible: debug
            },

            {
                data: "ACTIVITY_PREV.TITLE",
                render: function ( data, _type, row ) {
                    return activity(data, row.EXTRA_ROLES_AVER.ROLE_PREV);
                },
                className: 'activity_previous',
                visible: debug
            },
            {
                data: "ACTIVITY.TITLE",
                render: function ( data, _type, row ) {
                    return activity(data, row.EXTRA_ROLES_AVER.ROLE);
                },
                className: 'activity',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.WBS_ID_CHANGED",
                className: 'wbs_changed',
                visible: debug
            },
            {
                data: "WBS_NODE_PREV.TITLE",
                render: function ( data, _type, row ) {
                    return wbs(data, row.EXTRA_ROLES_AVER.ROLE_PREV);
                },
                className: 'wbs_prev',
                visible: debug
            },
            {
                data: "WBS_NODE.TITLE",
                render: function ( data, _type, row ) {
                    return wbs(data, row.EXTRA_ROLES_AVER.ROLE);
                },
                className: 'wbs',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.END_DATE_CHANGED",
                className: 'end_date_changed',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.END_DATE_PREV",
                render: function( data, _type, _row ) {
                    return date(data);
                },
                className: 'end_date_prev',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.END_DATE",
                render: function( data, _type, _row ) {
                    return date(data);
                },
                className: 'end_date',
                visible: debug
            },

            {
                data: "EXTRA_ROLES_AVER.COMMENTS_CHANGED",
                className: 'comments_changed',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.COMMENTS_PREV",
                render: function( data, _type, _row ) {
                    return comments(data);
                },
                className: 'comments_prev',
                visible: debug
            },
            {
                data: "EXTRA_ROLES_AVER.COMMENTS",
                render: function( data, _type, _row ) {
                    return comments(data);
                },
                className: 'comments',
                visible: debug
            }
        ],
        order: [[ 1, 'desc' ]],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },

    } );
 } );
