function uploadCSV()  {

    let form_data = new FormData();
        form_data.append('file', $('#file')[0].files[0]);
        form_data.append('upload', '1');

    $.ajax({

        url : 'uploads-check.php',
        type : 'POST',
        data : form_data,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType

        success: function(response) {
            let website = response.search("https://gitlab.cern.ch/atlas-otp/otp-upload/-/pipelines/"); //the pipeline url should start with this
            if (website != 0) {
                alert(response); //show the errors that were found
            } else {
                window.location.assign(response); //or redirect to the pipeline if there were none
                // window.open(response, "otp-upload-ci");
                // window.open(response, '_blank');
            }
        },
        error: function(request, status, error){
            alert('Something went wrong; please try again\n'+error);
        }
    });

}

// ctrl + f5 to refresh page
