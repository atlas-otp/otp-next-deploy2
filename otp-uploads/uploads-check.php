<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../');

require("config.php");

require('vendor/autoload.php');

require_once('otp-photos/PhotoUrlSigner.php');

use Unleash\Client\UnleashBuilder;

function flag_is_enabled(string $flag, string $env = null): bool {
    global $ff_details;

    if ($flag == '') {
        return true;
    }

    if (!$env) {
        $env = $ff_details["env"];
    }

    $instance_id = $ff_details["instance_id"];
    $app_url = $ff_details["app_url"];

    $unleash = UnleashBuilder::create()
    ->withAppName($env)
    ->withInstanceId($instance_id)
    ->withAppUrl($app_url)
    ->withCacheTimeToLive(0)    // remove for single env
    ->build();

    $feature = $env.'-'.$flag;
    return $unleash->isEnabled($feature);
}

function get_picture_url(string $user_id): ?string {
    global $db;
    global $photos_details;

    $ais_id = NULL;

    $rows = $db
        ->select( 'PUB_PERSON', 'AIS_PERSON_ID', ['ID' => $user_id, 'PHOTO' => 1], ['AIS_PERSON_ID'] )
        ->fetchAll();

    if (array_key_exists(0, $rows) &&
        array_key_exists('AIS_PERSON_ID', $rows[0])) {
        $ais_id = $rows[0]['AIS_PERSON_ID'];
    } else {
        $ais_id = NULL;
    }

    $picture_url = NULL;
    if ($ais_id) {
        $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

        $expiration = (new DateTime())->modify('10 mins');

        $server = $photos_details["gateway"];

        $picture_url = $server . "?ais_id=" . $ais_id;
        $picture_url = $urlSigner->sign($picture_url, $expiration);
    }

    return $picture_url;
}
?>
<?php
if (!flag_is_enabled('uploads')) {
	header("HTTP/1.0 501 Not Implemented");
    die("ERROR: Feature 'uploads' is disabled\n");
}
?>
<?php

/*
 * Roles table
 */

include( "config.php" );

// DataTables PHP library
include( "editor/lib/DataTables.php" );

/**
 * @var DataTables\Database $db
 * @var DataTables\Database $sql_details
 */

// function is_true($val, $return_null=false){
//     $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
//     return ( $boolval===null && !$return_null ? false : $boolval );
// }

$to_int = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val);
};

$from_int = function ( $val, $data ) {
	return $val == 0 ? NULL : $val;
};

$to_fte = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val)/100.;
};

$from_fte = function ( $val, $data ) {
	return $val*100.;
};

$to_bool = function( $val, $data ) {
	return $val == NULL ? false : (bool)$val;
};

// $from_bool = function( $val, $data ) {
// 	return $val == NULL ? false : (bool)$val;
// };

$to_string = function ( $val, $data ) {
    return $val == NULL ? '' : $val;
};

$from_string = function ( $val, $data ) {
    return $val == '' ? NULL : $val;
};

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
    DataTables\Editor\SearchPaneOptions,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost'])) {
    $user = "DUNS";
    if (array_key_exists('user', $_REQUEST)) {
        $user = $_REQUEST['user'];
    }
} else {
    // Old SSO
    $user = getenv("OIDC_CLAIM_cern_upn");
    if (!$user) {
        // compatible with OKD4 SSO
        $user = $_SERVER['HTTP_X_FORWARDED_USER'];
    }

    if ($user) {
        $user = strtoupper($user);
    } else {
        $user = "";
    }
    // $user = "LBARDO";
}

// get userid, system_id for unknown users
$user_id = 999980;
$user_id_select = $db->select( 'PERSON_SERVICE_ACCOUNT', 'ID', ['USERNAME' => $user] )->fetch();
if ($user_id_select) {
    $user_id = intval($user_id_select['ID']);
}

$system_transaction = 1;
$task_transaction = 2;
$booking_period_transaction = 3;
$shift_booking_transaction = 4;
$admin_transaction = 5;

$user_role_options = Options::inst()
	->table( 'USER_ROLE' )
	->value( 'ID' )
	->label( array('USERNAME', 'LNAME', 'FNAME', 'ROLE', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID') )
	// ->where(function ($q) {
	// 	global $user;

	// 	$q->where( 'USERNAME', $user, '=');
	// })
    ->order( 'LNAME, FNAME' )
	->render( function ( $row ) {
		return $row['USERNAME'].':'.$row['LNAME'].':'.$row['FNAME'].':'.$row['ROLE'].':'.$row['SYSTEM_ID'].':'.$row['ACTIVITY_ID'].':'.$row['WBS_ID'];
	} );

$roles = Editor::inst( $db, 'USER_ROLE', 'ID' )
    ->fields(
        Field::inst( 'USER_ROLE.USERNAME' ),
        Field::inst( 'USER_ROLE.ROLE' ),
        Field::inst( 'USER_ROLE.SYSTEM_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } ),
            Field::inst( 'USER_ROLE.WBS_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } )
    )
    ->write( false )
    ->where( 'USER_ROLE.USERNAME', $user )
    ->get();


function has_role(string $requested_role, int $system_or_wbs_id = null): bool {
    global $roles;

    foreach ($roles['data'] as $role_assignment) {
        if (!array_key_exists('USER_ROLE', $role_assignment)) {
            return false;
        }
        $role = $role_assignment['USER_ROLE']['ROLE'];
        $system_id = $role_assignment['USER_ROLE']['SYSTEM_ID'];
        $wbs_id = $role_assignment['USER_ROLE']['WBS_ID'];
        if ($requested_role == $role) {
            switch($role) {
                case 'ROLE_ADMIN':
                    return true;
                case 'ROLE_ROLE_MANAGER':
                    return true;
                case 'ROLE_ACTIVITY_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                case 'ROLE_SYSTEM_MANAGER':
                    if ($system_or_wbs_id == $system_id) {
                        return true;
                    }
                    break;
                case 'ROLE_WBS_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    return false;
}
?>
<?php

global $upload_token;
global $upload_branch;

//INITIALIZE
// line offset = 241
$data = array(); //Initialize the array to store your data
$errors_found = false; //global variable to check for errors

//ERROR FUNCTIONS
function no_year_in_name(string $real_name): bool {
    $correct = true; //check for mistakes

    $pattern = '!\d+!'; //search for all numbers
    preg_match_all($pattern, $real_name, $numbers);
    $numbers = $numbers[0]; //a list with all the numbers in the filename

    foreach ($numbers as $number) { //for each number in there
        $int = (int)$number;
        if ($int >= 1990 && $int <= 2099) { //see if it is between 1990 and 2099
            $correct = false;
        }
    }
    if ($correct) {
        echo "There is no year in the file name (between 1990 & 2099)\n\n"; return true;}
    else {
        return false; //no errors if there is a year
    }
}
/**
 * @param array<string> $task
 * @param array<string> $sub_task
 * @param array<string> $person_id
 * @param array<string> $start_month
 */
function duplicate_task($task, $sub_task, $person_id, $start_month): bool {
    /* Returs true if a duplicate task has been found
    It checks if lines have the same task, subtask and person ID with the same start month*/
    $correct = true; //uncheck if there are errors
    $unique = array(); //an array to save all unique tasks
    for ($i = 0; $i < count($task); $i++) {
        if (in_array("$task[$i], $sub_task[$i], $person_id[$i], $start_month[$i]", $unique)) { //see if the task is in the array
            echo "Duplicate task found on line " . ($i+1) . " (excluding empty lines)\n"; //give an error if it is
            echo "\t$task[$i], $sub_task[$i], $person_id[$i], $start_month[$i]\n";       // and show the duplicate
            $correct = false;
        }
        $unique[$i] = "$task[$i], $sub_task[$i], $person_id[$i], $start_month[$i]";
    }
    if (!$correct) {
        echo "\n"; return true;
    } else {
        return false;
    }
}
/**
 * @param array<string> $person_id
 * @param array<string> $first_name
 * @param array<string> $last_name
 */
function person_id_no_name($person_id, $first_name, $last_name): bool {
    /* If the person_id is 0, and there is a person associated, return true
    */
    $correct = true; //uncheck if miistake was found
    for ($i = 0; $i < count($person_id); $i++) { //for each person id
        if ($person_id[$i] == "0") {            //if it is 0
            if ($first_name[$i] != "" || $last_name[$i] != "") { //there should be no name
                echo "person_id = 0 with a name on line " . ($i+1) . "\n"; //show the line when found
                $correct = false;
            }
        }
    }
    if (!$correct) {
        echo "\n"; return true;
    } else {
        return false;
    }
}
/**
 * @param array<string> $column
 */
function missing_entry($column): bool {
    /* check if there are any empty entries in the inputted list,
    returns true if so */
    $correct = true;
    for ($i = 0; $i < count($column); $i++) {
        if ($column[$i] == "") {             //check if there iis an empty space in the file
            echo "Missing entry on line " . ($i+1) . "\n"; //show the line when found
            $correct = false;
        }
    }
    if (!$correct) {
        echo "\n"; return true;
    } else {
        return false;
    }
}
/**
 * @param array<string> $first_name
 * @param array<string> $last_name
 */
function missing_name($first_name, $last_name): bool {
    /* If an entry in one of the lists is empty, the same entry in the other array should also be empty
    returns true if an instance is found where this is the case*/
    $correct = true;
    for ($i = 0; $i < count($first_name); $i++) {
        // If there is an first name, there should be a last name (and vice versa)
        if (($first_name[$i] != "" && $last_name[$i] == "") || ($first_name[$i] == "" && $last_name[$i] != "")) {
            echo "Missing first or last name on line " . ($i+1) . "\n"; //show the line with the mistake
            $correct = false;
        }
    }
    if (!$correct) {
        echo "\n"; return true;
    } else {
        return false;
    }
}
/**
 * @param array<string> $fte
 */
function fte_non_numeric($fte): bool {
    /* All entry in the list (except the first one) should be numeric values
    returns true if an not numeric value is found*/
    $correct = true;
    for ($i = 1; $i < count($fte); $i++) {
        if (!is_numeric(str_replace("\"", "",$fte[$i]))) { //check if each entry is numeric
            echo "fte is not numeric on line " . ($i+1) . ": " . str_replace("\"", "",$fte[$i]) . "\n";
            $correct = false;
        }
    }
    if (!$correct) {
        echo "\n"; return true;
    } else {
        return false;
    }
}
/**
 * @param array<string> $month_input
 */
function month_not_exists($month_input): bool {
    /* Checks the values in the month, if the first 3 letters of each entry do not represent a month
    return true if a non existing month is found*/
    $correct = true;
    $months = ["jan","feb","mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]; //a array with all possible months
    for ($i = 1; $i < count($month_input); $i++) {
        if (!in_array(strtolower(substr(str_replace("\"", "",$month_input[$i]), 0, 3)), $months)) { //if the first 3 letters of the input represent a month
            $temp_month = substr(str_replace("\"", "",$month_input[$i]), 0, 3);
            echo "Non-existing month on line " . ($i+1) . " " . $temp_month . "\n"; //show the line and month with the mistake
            $correct = false;
        }
    }
    if (!$correct) {echo "\n"; return true;}
    else {return false;}
}

//GITLAB FUNCTIONS
/**
 * @param string $data
 * @return bool|array<mixed>
 */
function gitlab_commit(string $token, $data, string $filename, string $upload_branch, string $action) {
    $ch = curl_init(); // Initiate the curl

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // stop cURL from verifying the peer's certificate
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return the transfer as a string of the return value of curl_exec()
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","PRIVATE-TOKEN: $token")); //requests both to servers and proxies with the given token

    curl_setopt($ch, CURLOPT_URL, 'https://gitlab.cern.ch/api/v4/projects/atlas-otp%2Fotp-upload/repository/commits'); //Input the url to the function
    curl_setopt($ch, CURLOPT_POST, true); // do a regular HTTP POST.

    $info = array(
        'branch' => $upload_branch, // branch where to commit
        'commit_message' => "otp-upload-$action: upload/$filename", // the message with the commit
        'actions' => array(          // the actions the commit should take
           array(
              'action' => $action,       // the action 'update' or 'create'
              'file_path' => "upload/$filename",  // the file to upload or create
              'content' => $data    // the contents of the file
           )
        )
     );

    $payload = json_encode($info, JSON_PRETTY_PRINT);  // Encode all the info to sent to gitlab
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload); // The full data to post in a HTTP "POST" operation

    $res = curl_exec($ch);  // Execute the curl
    $err = curl_error($ch);  // Get the error if there is one
    curl_close($ch);  // Close the curl

    if ($err) {
        echo "cURL Error #:" . $err;
        return false;  // Display the error if cURL does not work
    }

    if (!is_string($res)) {
        echo "Unidentified error";
        return false;
    }
    $decode_res = json_decode($res, true);

    if ($action == "update") {
        return $decode_res;
    } elseif ($decode_res["message"] == "A file with this name already exists") {
        // If the file already exists
        // Try to update the file instead of create
        return gitlab_commit($token, $data, $filename, $upload_branch, "update");
    } elseif (!array_key_exists("id", $decode_res)) {
        // If there is no ID key in the array, print the error/message from gitlab
        echo $res. "\n";
        return false;
    }
    return $decode_res; // Return only if there is an ID
}

/**
 * @return bool|mixed
 */
function get_pipelines(string $token) {
    $ch = curl_init();                                  // Initiate the url
    $url = "https://gitlab.cern.ch/api/v4/projects/atlas-otp%2Fotp-upload/pipelines?per_page=100";
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    // stop cURL from verifying the peer's certificate
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     // return the transfer as a string of the return value of curl_exec()
    curl_setopt($ch, CURLOPT_HTTPHEADER,array("PRIVATE-TOKEN: $token"));    // requests both to servers and proxies with the given token
    curl_setopt($ch, CURLOPT_URL,$url);                 // Input the url to the function
    $result = curl_exec($ch);                           // execute curl and saves the data
    curl_close($ch);  // Close the url
    return is_string($result) ? json_decode($result, true) : false;                  // Return nothing, if not found
}


//READING DATA FROM FILE
//if (array_key_exists('upload', $_POST)) { //Open and read the received file
if (empty($_FILES)) {
    //do nothing if there is no file
    echo "No file found"; return 0;
} else {
    $real_name = json_encode($_FILES['file']["name"]); //remove quote from filename!!!
    if (!$real_name) { echo "Cannot jsondecode name"; return 1; }

    $extension = explode('.',$real_name)[1]; //Get the extension from the filename (.csv/.xlsx)
    $extension = substr($extension, 0, -1);
    $fileName = str_replace("\\", "/", $_FILES['file']['tmp_name']); //Change the filename to get the contents
    $content = file_get_contents($fileName); //read the contents from the file
    if (!$content) { echo "No content in file"; return 1; }

    if ($extension == 'csv') {
        $rows = explode("\n", $content); //split the data data into rows
        for ($row = 0; $row < count($rows); $row++) {
            $cells = explode(",", $rows[$row]); //Split each row into entries
            if (count($cells) > 2) { // If the row is not empty
                for ($col = 0; $col < count($cells); $col++) {
                    $data[$row][$col] = $cells[$col]; // Add each cell to the data array
                }
            } else if ($row != count($rows)-1) { //If there is an empty row, notify and exit (behalve laatse)!!!
                echo "Empty line found on line " . ($row+1) . "\n";
                $errors_found = true;
            }
        }
    }
    else if ($extension == "xlsx") {
        // Credits: https://blog.programster.org/phpspreadsheet-read-excel-file-to-array
        // include the autoloader, so we can use PhpSpreadsheet

        // Create a new Xlsx Reader
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        // Tell the reader to only read the data. Ignore formatting etc.
        $reader->setReadDataOnly(true);
        // Read the spreadsheet file.
        $spreadsheet = $reader->load($fileName);
        $sheet = $spreadsheet->getSheet($spreadsheet->getFirstSheetIndex());
        $data = $sheet->toArray();

        for ($row = 0; $row<count($data); $row++){
            if (count($data[$row]) < 2) {
                echo "Empty line found on line " . ($row+1) . "\n";
                $errors_found = true;
            }
        }
    }
    else {
        echo "Please select a .csv or a .xlsx file";
        return 0;
    }
    if ($errors_found) { // If an empty line was found:
        echo "\n"; // print an extra enter to separate the errors
    }
}
//error_log(print_r($data[1][3]), TRUE);

//DATA PROCESSING
if ($data != array()) {
    //Proceed if data is in $data
    $task_id = [];
    $sub_task_id = [];
    $person_id = [];
    $last_name = [];
    $first_name = [];
    $start_month = [];
    $end_month = [];
    $fte = [];

    //SPLIT DATA IN ARRAYS
    $n_columns = 0;
    for ($i = 0; $i < count($data[0]); $i++) {
        //look through the first row of the file, take the relevant columns from the file
        if (str_contains(strtolower($data[0][$i]), "task id") && !str_contains(strtolower($data[0][$i]), "subtask id")) {
            $task_id = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "subtask id")) {
            $sub_task_id = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "person id")) {
            $person_id = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "last name")) {
            $last_name = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "first name")) {
            $first_name = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "start month")) {
            $start_month = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "end month")) {
            $end_month = array_column($data, $i); $n_columns++;}
        else if (str_contains(strtolower($data[0][$i]), "fte")) {
            $fte = array_column($data, $i); $n_columns++;}
    }

    if ($n_columns < 8) { //If not all columns are found, exit
        echo "not enough columns found in the file\n";
        echo "make sure the first row contains:\n";
        echo "Task ID, Subtask ID, Person ID, Last Name, First Name, Start Month, End Month \& fte\n";
        return 0;
    }

    // FIND ERRORS
    if (no_year_in_name($real_name)) { //Find all errors in the file with the functions above
        $errors_found = true;}
    // if (duplicate_task($task_id, $sub_task_id, $person_id, $start_month)) {
    //     $errors_found = true;}
    if (person_id_no_name($person_id, $first_name, $last_name)) {
        $errors_found = true;}
    if (missing_entry($task_id) || missing_entry($sub_task_id) || missing_entry($person_id) || missing_entry($start_month) || missing_entry($end_month) ||  missing_entry($fte)) {
        $errors_found = true;}
    if (missing_name($first_name, $last_name)) {
        $errors_found = true;}
    if (fte_non_numeric($fte)) {
        $errors_found = true;}
    if (month_not_exists($start_month) || month_not_exists($end_month)) {
        $errors_found = true;}

    // Return the error(s) if any where found
    if ($errors_found) {
        return 0;
    }

    // SAVE FILE TO GITLAB
    $real_name = str_replace('"', "", $real_name); //the uploaded filename
    // $rows = explode("\n", $content); //split the data data into rows

    // commit the file to gitlab with the original data
    $result = gitlab_commit($upload_token, $content, $real_name, $upload_branch, "create");
    if (!$result) {
        echo "error while uploading to gitlab";
        return 0;
    }
    if (is_array($result)) {
        $id = $result["id"]; //The ID number of the gitlab commit

        $pipeline_url = 0; // initialize $pipeline_url
        $start = microtime(true); // time at which the loop was started
        while ($pipeline_url == 0 || microtime(true) - $start <= 10) { //while the pipeline has not been found or until 5 seconds have elapsed.
            $pipelines = get_pipelines($upload_token); // get the url of the pipeline
            foreach ($pipelines as $pipeline) {
                if ($pipeline["sha"] == $id) { // the "sha of the pipeline is the id of the commit
                    $pipeline_url = $pipeline["web_url"]; // return the url of the pipeline
                }
            }
        }
        echo $pipeline_url;
    }
    return 0;
}
return 0;

