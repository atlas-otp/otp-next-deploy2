<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../');

require("config.php");

require('vendor/autoload.php');

require_once('otp-photos/PhotoUrlSigner.php');

use Unleash\Client\UnleashBuilder;

function flag_is_enabled(string $flag, string $env = null): bool {
    global $ff_details;

    if ($flag == '') {
        return true;
    }

    if (!$env) {
        $env = $ff_details["env"];
    }

    $instance_id = $ff_details["instance_id"];
    $app_url = $ff_details["app_url"];

    $unleash = UnleashBuilder::create()
    ->withAppName($env)
    ->withInstanceId($instance_id)
    ->withAppUrl($app_url)
    ->withCacheTimeToLive(0)    // remove for single env
    ->build();

    $feature = $env.'-'.$flag;
    return $unleash->isEnabled($feature);
}

function get_picture_url(string $user_id): ?string {
    global $db;
    global $photos_details;

    $ais_id = NULL;

    $rows = $db
        ->select( 'PUB_PERSON', 'AIS_PERSON_ID', ['ID' => $user_id, 'PHOTO' => 1], ['AIS_PERSON_ID'] )
        ->fetchAll();

    if (array_key_exists(0, $rows) &&
        array_key_exists('AIS_PERSON_ID', $rows[0])) {
        $ais_id = $rows[0]['AIS_PERSON_ID'];
    } else {
        $ais_id = NULL;
    }

    $picture_url = NULL;
    if ($ais_id) {
        $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

        $expiration = (new DateTime())->modify('10 mins');

        $server = $photos_details["gateway"];

        $picture_url = $server . "?ais_id=" . $ais_id;
        $picture_url = $urlSigner->sign($picture_url, $expiration);
    }

    return $picture_url;
}
?>


<?php

/**
 * @return array<string>
 */
function get_environments(string $token, string $project_id): array {
    $opts = array(
        'http'=>array(
            'method'=>"GET",
            'header'=>"PRIVATE-TOKEN: $token\r\n"
        )
    );

    $url = "https://gitlab.cern.ch/api/v4/projects/$project_id/environments";

    $context = stream_context_create($opts);
    $json = file_get_contents($url, false, $context);

    $envs = [];
    if ($json) {
        $envs_info = json_decode($json, true);
        if ($envs_info) {
            foreach($envs_info as $env) {
                array_push($envs, $env['slug']);
            }
        }
    }
    return $envs;
}

/**
 * @return array<string>
 */
function get_flags(string $token, string $project_id): array {
    $opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"PRIVATE-TOKEN: $token\r\n"
        )
    );

    $url = "https://gitlab.cern.ch/api/v4/projects/$project_id/feature_flags";

    $context = stream_context_create($opts);
    $json = file_get_contents($url, false, $context);

    $flags = [];
    if ($json) {
        $flags_info = json_decode($json, true);
        if ($flags_info) {
            foreach($flags_info as $flag_info) {
                $name = $flag_info['name'];
                $env = $flag_info['strategies'][0]['scopes'][0]['environment_scope'];
                $prefix = $env.'-';
                if (substr($name, 0, strlen($prefix)) === $prefix) {
                    $flag = substr($name, strlen($prefix));
                    if (!in_array($flag, $flags)) {
                        array_push($flags, $flag);
                    }
                }
            }
        }
    }
    return $flags;
}

global $ff_details;

$token = $ff_details["token"];

$project_id = $ff_details["project_id"];

$envs = get_environments($token, $project_id);

$flags = get_flags($token, $project_id);

$data = [];
$current_env = $ff_details["env"];

$row = [];
$row['flag'] = '_current_env_';
foreach($envs as $env) {
    $row[$env] = $env == $current_env;
}
array_push($data, $row);

foreach($flags as $flag) {
    $row = [];
    $row['flag'] = $flag;
    foreach($envs as $env) {
        $row[$env] = flag_is_enabled($flag, $env);
    }
    array_push($data, $row);
}

header('Content-Type: application/json');
echo json_encode( $data );

