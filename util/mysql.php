<?php

$sql_details = array(
	"type" => NULL,         // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => "",           // Database password (TBS)
	"host" => "",     		// Database host
	"port" => "",           // Database connection port (can be left empty for default)
	"db"   => "",           // Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "",           // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   	// PHP PDO attributes array. See the PHP documentation for all options
);

define("DATATABLES", true);

require_once '../config.php';

echo "<DL>\n";
echo "<DT>type</DT>\n";
echo "<DD>".$sql_details["type"]."</DD>\n";
echo "<DT>user</DT>\n";
echo "<DD>".$sql_details["user"]."</DD>\n";
echo "<DT>db</DT>\n";
echo "<DD>".$sql_details["db"]."</DD>\n";
echo "<DT>host</DT>\n";
echo "<DD>".$sql_details["host"]."</DD>\n";
echo "<DT>port</DT>\n";
echo "<DD>".$sql_details["port"]."</DD>\n";
echo "<DT>dsn</DT>\n";
echo "<DD>".$sql_details["dsn"]."</DD>\n";
echo "</DL>\n";

if (!function_exists('mysqli_connect')) {
    header("HTTP/1.0 502 Bad Gateway");
    die("function mysql_connect not defined\n");
}

$otpConnection = mysqli_connect($sql_details["host"], $sql_details["user"], $sql_details["pass"], $sql_details["db"], (int)$sql_details["port"]);
if (!$otpConnection) {
    header("HTTP/1.0 502 Bad Gateway");
    if (mysqli_connect_errno()) {
        echo mysqli_connect_error(), "\n";
    }
    exit;
}

mysqli_set_charset($otpConnection, preg_replace('/^charset=/', '', $sql_details["dsn"]));

$sql = 'SELECT * FROM SYSTEM_NODE';
$result = mysqli_query($otpConnection, $sql);
if ($result) {
    echo "<table border='1'>\n";
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        echo "<tr>\n";
        foreach ($row as $item) {
            echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
        }
        echo "</tr>\n";
    }
    echo "</table>\n";
} else {
    header("HTTP/1.0 502 Bad Gateway");
    die("'$sql' not valid\n");
}
