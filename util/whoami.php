<?php

require_once '../config.php';

$user = "";
$where = "";
if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost'])) {
    $user = "DUNS";
    $where = "localhost";
} else {
    // Old SSO
    $user = getenv("OIDC_CLAIM_cern_upn");
    $where = "OIDC_CLAIM_cern_upn";
    if (!$user) {
        // compatible with OKD4 SSO
        $user = $_SERVER['HTTP_X_FORWARDED_USER'];
        $where = "HTTP_X_FORWARDED_USER";
    }
}

echo "<DL>\n";
echo "<DT>user</DT>\n";
echo "<DD>".$user."</DD>\n";
echo "<DT>where</DT>\n";
echo "<DD>".$where."</DD>\n";
echo "</DL>\n";
