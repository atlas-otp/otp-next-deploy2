<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../');

require("config.php");

require('vendor/autoload.php');

require_once('otp-photos/PhotoUrlSigner.php');

use Unleash\Client\UnleashBuilder;

function flag_is_enabled(string $flag, string $env = null): bool {
    global $ff_details;

    if ($flag == '') {
        return true;
    }

    if (!$env) {
        $env = $ff_details["env"];
    }

    $instance_id = $ff_details["instance_id"];
    $app_url = $ff_details["app_url"];

    $unleash = UnleashBuilder::create()
    ->withAppName($env)
    ->withInstanceId($instance_id)
    ->withAppUrl($app_url)
    ->withCacheTimeToLive(0)    // remove for single env
    ->build();

    $feature = $env.'-'.$flag;
    return $unleash->isEnabled($feature);
}

function get_picture_url(string $user_id): ?string {
    global $db;
    global $photos_details;

    $ais_id = NULL;

    $rows = $db
        ->select( 'PUB_PERSON', 'AIS_PERSON_ID', ['ID' => $user_id, 'PHOTO' => 1], ['AIS_PERSON_ID'] )
        ->fetchAll();

    if (array_key_exists(0, $rows) &&
        array_key_exists('AIS_PERSON_ID', $rows[0])) {
        $ais_id = $rows[0]['AIS_PERSON_ID'];
    } else {
        $ais_id = NULL;
    }

    $picture_url = NULL;
    if ($ais_id) {
        $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

        $expiration = (new DateTime())->modify('10 mins');

        $server = $photos_details["gateway"];

        $picture_url = $server . "?ais_id=" . $ais_id;
        $picture_url = $urlSigner->sign($picture_url, $expiration);
    }

    return $picture_url;
}
?>
<?php
if (!flag_is_enabled('calendar')) {
	header("HTTP/1.0 501 Not Implemented");
    die("ERROR: Feature 'calendar' is disabled\n");
}
?>
<?php

/*
 * Roles table
 */

include( "config.php" );

// DataTables PHP library
include( "editor/lib/DataTables.php" );

/**
 * @var DataTables\Database $db
 * @var DataTables\Database $sql_details
 */

// function is_true($val, $return_null=false){
//     $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
//     return ( $boolval===null && !$return_null ? false : $boolval );
// }

$to_int = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val);
};

$from_int = function ( $val, $data ) {
	return $val == 0 ? NULL : $val;
};

$to_fte = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val)/100.;
};

$from_fte = function ( $val, $data ) {
	return $val*100.;
};

$to_bool = function( $val, $data ) {
	return $val == NULL ? false : (bool)$val;
};

// $from_bool = function( $val, $data ) {
// 	return $val == NULL ? false : (bool)$val;
// };

$to_string = function ( $val, $data ) {
    return $val == NULL ? '' : $val;
};

$from_string = function ( $val, $data ) {
    return $val == '' ? NULL : $val;
};

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
    DataTables\Editor\SearchPaneOptions,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost'])) {
    $user = "DUNS";
    if (array_key_exists('user', $_REQUEST)) {
        $user = $_REQUEST['user'];
    }
} else {
    // Old SSO
    $user = getenv("OIDC_CLAIM_cern_upn");
    if (!$user) {
        // compatible with OKD4 SSO
        $user = $_SERVER['HTTP_X_FORWARDED_USER'];
    }

    if ($user) {
        $user = strtoupper($user);
    } else {
        $user = "";
    }
    // $user = "LBARDO";
}

// get userid, system_id for unknown users
$user_id = 999980;
$user_id_select = $db->select( 'PERSON_SERVICE_ACCOUNT', 'ID', ['USERNAME' => $user] )->fetch();
if ($user_id_select) {
    $user_id = intval($user_id_select['ID']);
}

$system_transaction = 1;
$task_transaction = 2;
$booking_period_transaction = 3;
$shift_booking_transaction = 4;
$admin_transaction = 5;

$user_role_options = Options::inst()
	->table( 'USER_ROLE' )
	->value( 'ID' )
	->label( array('USERNAME', 'LNAME', 'FNAME', 'ROLE', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID') )
	// ->where(function ($q) {
	// 	global $user;

	// 	$q->where( 'USERNAME', $user, '=');
	// })
    ->order( 'LNAME, FNAME' )
	->render( function ( $row ) {
		return $row['USERNAME'].':'.$row['LNAME'].':'.$row['FNAME'].':'.$row['ROLE'].':'.$row['SYSTEM_ID'].':'.$row['ACTIVITY_ID'].':'.$row['WBS_ID'];
	} );

$roles = Editor::inst( $db, 'USER_ROLE', 'ID' )
    ->fields(
        Field::inst( 'USER_ROLE.USERNAME' ),
        Field::inst( 'USER_ROLE.ROLE' ),
        Field::inst( 'USER_ROLE.SYSTEM_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } ),
            Field::inst( 'USER_ROLE.WBS_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } )
    )
    ->write( false )
    ->where( 'USER_ROLE.USERNAME', $user )
    ->get();


function has_role(string $requested_role, int $system_or_wbs_id = null): bool {
    global $roles;

    foreach ($roles['data'] as $role_assignment) {
        if (!array_key_exists('USER_ROLE', $role_assignment)) {
            return false;
        }
        $role = $role_assignment['USER_ROLE']['ROLE'];
        $system_id = $role_assignment['USER_ROLE']['SYSTEM_ID'];
        $wbs_id = $role_assignment['USER_ROLE']['WBS_ID'];
        if ($requested_role == $role) {
            switch($role) {
                case 'ROLE_ADMIN':
                    return true;
                case 'ROLE_ROLE_MANAGER':
                    return true;
                case 'ROLE_ACTIVITY_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                case 'ROLE_SYSTEM_MANAGER':
                    if ($system_or_wbs_id == $system_id) {
                        return true;
                    }
                    break;
                case 'ROLE_WBS_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    return false;
}
?>
<?php

use UUID\UUID;

global $user;

$debug = isset($_REQUEST['debug']);
$show_url = isset($_REQUEST['url']);

if ($user == null) {
    header("HTTP/1.0 502 Bad Gateway");
    die("null user, not authenticated ?\n");
}

$authenticated_user = $user;
$text = "";

// Redefine user if needed
if (has_role('ROLE_ADMIN') && isset($_REQUEST['user'])) {
    $user = strtoupper($_REQUEST['user']);
    if ($debug) {
        $text .= "User changed from $authenticated_user to $user\n";
    }
}

// get SHA from Database
if (!function_exists('oci_connect')) {
    header("HTTP/1.0 502 Bad Gateway");
    die("function oci_connect not defined\n");
}

if ($debug) {
    $text .= "type: ".$sql_details["type"]."\n";
    $text .= "user: ".$sql_details["user"]."\n";
    $text .= "db:   ".$sql_details["db"]."\n";
}

$otpConnection = oci_connect($sql_details["user"], $sql_details["pass"], $sql_details["db"]);
if (!$otpConnection) {
    header("HTTP/1.0 502 Bad Gateway");
    $m = oci_error();
    if ($m) {
        echo $m['message'], "\n";
    }
    die ($text);
}

$sql = "SELECT USERNAME from PUB_PERSON where USERNAME = :username";
$stid = oci_parse($otpConnection, $sql);
if (!$stid) {
    header("HTTP/1.0 502 Bad Gateway");
    die("'$sql' not valid\n");
}
oci_bind_by_name($stid, ":username", $user);
oci_execute($stid);

if (!oci_fetch_row($stid)) {
    header("HTTP/1.0 502 Bad Gateway");
    die("user '$user' does not exist\n");
}

$sql = "SELECT UUID FROM PERSON_UUID where USERNAME = :username";
$stid = oci_parse($otpConnection, $sql);
if (!$stid) {
    header("HTTP/1.0 502 Bad Gateway");
    die("'$sql' not valid\n");
}

oci_bind_by_name($stid, ":username", $user);
oci_execute($stid);

$uuid = "";
if ($row = oci_fetch_row($stid)) {
    // User exists, get UUID
    $uuid = $row[0];
} else {
    if ($debug) {
        $text .= "New UUID record created for $user\n";
    }
    $uuid = UUID::uuid4();
    $sql = "INSERT INTO PERSON_UUID(USERNAME,UUID) values (:username, :uuid)";
    $stid = oci_parse($otpConnection, $sql);
    if (!$stid) {
        header("HTTP/1.0 502 Bad Gateway");
        die("'$sql' not valid\n");
    }

    oci_bind_by_name($stid, ":username", $user);
    oci_bind_by_name($stid, ":uuid", $uuid);
    if (!oci_execute($stid)) {
        header("HTTP/1.0 502 Bad Gateway");
        $m = oci_error();
        if ($m) {
            echo $m['message'], "\n";
        }
        die ($text);
    }
    oci_commit($otpConnection);
}

if ($debug) {
    $text .= "uuid: ".$uuid."\n";
}

// Create redirect URL
$server_name = $_SERVER['SERVER_NAME'];

$redirect_host = "not.available";
if ($server_name == "otp-next-atlas.app.cern.ch") {
    $redirect_host = "otp-calendar-atlas.app.cern.ch";
} elseif ($server_name == "otp-next-atlas-alpha.app.cern.ch") {
    $redirect_host = "otp-calendar-atlas-alpha.app.cern.ch";
}

$edit_query = $_REQUEST;
unset($edit_query['url']);
$edit_query['uuid'] = $uuid;
$query_string = http_build_query($edit_query);

$protocol = $debug ? "https" : "webcal";
$url = "$protocol://$redirect_host/otp-calendar.php?$query_string";

if ($debug) {
    header('Content-Type: text/html');
    $text .= "Server Name: $server_name\n";
    $text .= "Query String: $query_string\n";
    $text .= "Redirect to <a href=\"$url\">$url</a>\n";
    echo "<pre>\n$text</pre>\n";
    die();
}

if ($show_url) {
    header('Content-Type: text/html');
    echo "<pre>\n";
    echo "Paste this URL in your Calendar Application\n";
    echo "\n";
    echo "<a href=\"$url\" id=\"calendar_url\">$url</a> <button onclick=\"copy_to_clipboard('calendar_url')\">Copy to Clipboard</button>\n";
    echo "</pre>\n";
    echo "<script type=\"text/javascript\" language=\"javascript\" src=\"./..//js/otp-next.js\"></script>\n";
    die();
}

header("Status: 301 Moved Permanently");
header("Location: ".$url);
die();

