<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '../');

require("config.php");

require('vendor/autoload.php');

require_once('otp-photos/PhotoUrlSigner.php');

use Unleash\Client\UnleashBuilder;

function flag_is_enabled(string $flag, string $env = null): bool {
    global $ff_details;

    if ($flag == '') {
        return true;
    }

    if (!$env) {
        $env = $ff_details["env"];
    }

    $instance_id = $ff_details["instance_id"];
    $app_url = $ff_details["app_url"];

    $unleash = UnleashBuilder::create()
    ->withAppName($env)
    ->withInstanceId($instance_id)
    ->withAppUrl($app_url)
    ->withCacheTimeToLive(0)    // remove for single env
    ->build();

    $feature = $env.'-'.$flag;
    return $unleash->isEnabled($feature);
}

function get_picture_url(string $user_id): ?string {
    global $db;
    global $photos_details;

    $ais_id = NULL;

    $rows = $db
        ->select( 'PUB_PERSON', 'AIS_PERSON_ID', ['ID' => $user_id, 'PHOTO' => 1], ['AIS_PERSON_ID'] )
        ->fetchAll();

    if (array_key_exists(0, $rows) &&
        array_key_exists('AIS_PERSON_ID', $rows[0])) {
        $ais_id = $rows[0]['AIS_PERSON_ID'];
    } else {
        $ais_id = NULL;
    }

    $picture_url = NULL;
    if ($ais_id) {
        $urlSigner = new PhotoUrlSigner($photos_details["secret"]);

        $expiration = (new DateTime())->modify('10 mins');

        $server = $photos_details["gateway"];

        $picture_url = $server . "?ais_id=" . $ais_id;
        $picture_url = $urlSigner->sign($picture_url, $expiration);
    }

    return $picture_url;
}
?>
<?php
if (!flag_is_enabled('allocations')) {
	header("HTTP/1.0 501 Not Implemented");
    die("ERROR: Feature 'allocations' is disabled\n");
}
?>
<?php

/*
 * Roles table
 */

include( "config.php" );

// DataTables PHP library
include( "editor/lib/DataTables.php" );

/**
 * @var DataTables\Database $db
 * @var DataTables\Database $sql_details
 */

// function is_true($val, $return_null=false){
//     $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
//     return ( $boolval===null && !$return_null ? false : $boolval );
// }

$to_int = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val);
};

$from_int = function ( $val, $data ) {
	return $val == 0 ? NULL : $val;
};

$to_fte = function( $val, $data ) {
	return $val == NULL ? 0 : intval($val)/100.;
};

$from_fte = function ( $val, $data ) {
	return $val*100.;
};

$to_bool = function( $val, $data ) {
	return $val == NULL ? false : (bool)$val;
};

// $from_bool = function( $val, $data ) {
// 	return $val == NULL ? false : (bool)$val;
// };

$to_string = function ( $val, $data ) {
    return $val == NULL ? '' : $val;
};

$from_string = function ( $val, $data ) {
    return $val == '' ? NULL : $val;
};

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
    DataTables\Editor\SearchPaneOptions,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost'])) {
    $user = "DUNS";
    if (array_key_exists('user', $_REQUEST)) {
        $user = $_REQUEST['user'];
    }
} else {
    // Old SSO
    $user = getenv("OIDC_CLAIM_cern_upn");
    if (!$user) {
        // compatible with OKD4 SSO
        $user = $_SERVER['HTTP_X_FORWARDED_USER'];
    }

    if ($user) {
        $user = strtoupper($user);
    } else {
        $user = "";
    }
    // $user = "LBARDO";
}

// get userid, system_id for unknown users
$user_id = 999980;
$user_id_select = $db->select( 'PERSON_SERVICE_ACCOUNT', 'ID', ['USERNAME' => $user] )->fetch();
if ($user_id_select) {
    $user_id = intval($user_id_select['ID']);
}

$system_transaction = 1;
$task_transaction = 2;
$booking_period_transaction = 3;
$shift_booking_transaction = 4;
$admin_transaction = 5;

$user_role_options = Options::inst()
	->table( 'USER_ROLE' )
	->value( 'ID' )
	->label( array('USERNAME', 'LNAME', 'FNAME', 'ROLE', 'SYSTEM_ID', 'ACTIVITY_ID', 'WBS_ID') )
	// ->where(function ($q) {
	// 	global $user;

	// 	$q->where( 'USERNAME', $user, '=');
	// })
    ->order( 'LNAME, FNAME' )
	->render( function ( $row ) {
		return $row['USERNAME'].':'.$row['LNAME'].':'.$row['FNAME'].':'.$row['ROLE'].':'.$row['SYSTEM_ID'].':'.$row['ACTIVITY_ID'].':'.$row['WBS_ID'];
	} );

$roles = Editor::inst( $db, 'USER_ROLE', 'ID' )
    ->fields(
        Field::inst( 'USER_ROLE.USERNAME' ),
        Field::inst( 'USER_ROLE.ROLE' ),
        Field::inst( 'USER_ROLE.SYSTEM_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } ),
            Field::inst( 'USER_ROLE.WBS_ID' )
            ->getFormatter( function ( $val, $data ) {
                return $val == NULL ? '' : $val;
            } )
    )
    ->write( false )
    ->where( 'USER_ROLE.USERNAME', $user )
    ->get();


function has_role(string $requested_role, int $system_or_wbs_id = null): bool {
    global $roles;

    foreach ($roles['data'] as $role_assignment) {
        if (!array_key_exists('USER_ROLE', $role_assignment)) {
            return false;
        }
        $role = $role_assignment['USER_ROLE']['ROLE'];
        $system_id = $role_assignment['USER_ROLE']['SYSTEM_ID'];
        $wbs_id = $role_assignment['USER_ROLE']['WBS_ID'];
        if ($requested_role == $role) {
            switch($role) {
                case 'ROLE_ADMIN':
                    return true;
                case 'ROLE_ROLE_MANAGER':
                    return true;
                case 'ROLE_ACTIVITY_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                case 'ROLE_SYSTEM_MANAGER':
                    if ($system_or_wbs_id == $system_id) {
                        return true;
                    }
                    break;
                case 'ROLE_WBS_MANAGER':
                    if ($system_or_wbs_id == $wbs_id) {
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    return false;
}
?>
<?php

/**
 * Encode a DT_RowId string
 *
 * @param string       		$row_id_prefix
 * @param non-empty-string	$separator
 * @param int				$year
 * @param int 				$allocation_id
 * @param int 				$shift_type
 *
 * @return string		   	$row_id
 */
function encode_row_id($row_id_prefix, $separator, $year, $allocation_id, $shift_type) {
	return $row_id_prefix.implode($separator, [$year, $allocation_id, $shift_type]);
}

/**
 * Decodes DT_RowId string
 *
 * @param string       		$row_id
 * @param string       		$row_id_prefix
 * @param non-empty-string	$separator
 *
 * @return array<int>		[year, allocation_id, shift_type]
 */
function decode_row_id($row_id, $row_id_prefix, $separator) {
	$row_id = str_replace($row_id_prefix, '', $row_id);
	return array_map('intval', explode($separator, $row_id));
}

/**
 * @param int 			$requirement_id
 *
 * @return void
 */
function validate_create($requirement_id) {
	global $user, $user_id, $db;

	$roles = $db->raw()
		->bind( ':requirement_id', $requirement_id )
		->bind( ':user_id', $user_id )
		->exec( "SELECT DISTINCT ALL_ROLES.ROLE, ALL_ROLES.WBS_ID, ALL_ROLES.SYSTEM_ID
		FROM RES_REQUIREMENT
		JOIN TASK on TASK.ID = RES_REQUIREMENT.TASK_ID
		JOIN WBS_NODE on WBS_NODE.ID = TASK.WBS_ID
		JOIN ALL_ROLES ON
		(ALL_ROLES.ROLE = 'ROLE_ADMIN') OR
		(ALL_ROLES.ROLE = 'ROLE_ACTIVITY_MANAGER' AND ALL_ROLES.WBS_ID = WBS_NODE.PARENT_ID) OR
		(ALL_ROLES.ROLE = 'ROLE_WBS_MANAGER' AND ALL_ROLES.WBS_ID = WBS_NODE.ID) OR
		(ALL_ROLES.ROLE = 'ROLE_SYSTEM_MANAGER' AND ALL_ROLES.SYSTEM_ID = TASK.SYSTEM_ID)
		WHERE RES_REQUIREMENT.ID = :requirement_id
		AND ALL_ROLES.USER_ID = :user_id
		" )
		->fetchAll();

	// print_r($roles);
	if (count($roles) > 0) {
		return;
	}

	// Error
	header('Content-Type: application/json');
	$data = [
		'data' => [],
		'cancelled' => [],
		'error' => "No create authorization for user: $user"
	];
	echo json_encode($data);
	exit(0);
}

/**
 * @param int 			$allocation_id
 *
 * @return void
 */
function validate_edit_remove($allocation_id) {
	global $user, $user_id, $db;

	$roles = $db->raw()
		->bind( ':allocation_id', $allocation_id )
		->bind( ':user_id', $user_id )
		->exec( "SELECT DISTINCT ALL_ROLES.ROLE, ALL_ROLES.WBS_ID, ALL_ROLES.SYSTEM_ID
		FROM RES_ALLOCATION
		JOIN RES_REQUIREMENT on RES_REQUIREMENT.ID = RES_ALLOCATION.RES_REQUIREMENT_ID
		JOIN TASK on TASK.ID = RES_REQUIREMENT.TASK_ID
		JOIN WBS_NODE on WBS_NODE.ID = TASK.WBS_ID
		JOIN ALL_ROLES ON
		(ALL_ROLES.ROLE = 'ROLE_ADMIN') OR
		(ALL_ROLES.ROLE = 'ROLE_ACTIVITY_MANAGER' AND ALL_ROLES.WBS_ID = WBS_NODE.PARENT_ID) OR
		(ALL_ROLES.ROLE = 'ROLE_WBS_MANAGER' AND ALL_ROLES.WBS_ID = WBS_NODE.ID) OR
		(ALL_ROLES.ROLE = 'ROLE_SYSTEM_MANAGER' AND ALL_ROLES.SYSTEM_ID = TASK.SYSTEM_ID)
		WHERE RES_ALLOCATION.ID = :allocation_id
		AND ALL_ROLES.USER_ID = :user_id
		" )
		->fetchAll();

	// print_r($roles);
	if (count($roles) > 0) {
		return;
	}

	// Error
	header('Content-Type: application/json');
	$data = [
		'data' => [],
		'cancelled' => [],
		'error' => "No edit/remove authorization for user: $user"
	];
	echo json_encode($data);
	exit(0);
}

/**
 * @param int 			$year
 *
 * @return void
 */
function validate_year($year) {
	$current_year = date("Y");
	if (!has_role('ROLE_ADMIN') && ($year < $current_year - 1)) {
		// Error
		header('Content-Type: application/json');
		$data = [
			'data' => [],
			'cancelled' => [],
			'error' => "Cannot update years earlier than ".($current_year - 1)
		];
		echo json_encode($data);
		exit(0);
	}
}

/**
 * @param string				$action
 * @param int 					$year
 * @param int 					$allocation_id
 * @param int 					$shift_type
 * @param array<string,string>	$fields
 *
 * @return void
 */
function push_year($action, $year, $allocation_id, $shift_type, $fields) {

	global $months, $db;

	// make sure we are ok
	validate_edit_remove($allocation_id);
	validate_year($year);

	// run over all fields (months)
	foreach($fields as $field_name => $field_value) {
		if (array_key_exists($field_name, $months)) {
			// print($field_name." ".$field_value."\n");

			// calculate MONTH_CODE
			$month = $months[$field_name];
			$value = intval(floatval($field_value)*100);
			$month_code = "$year$month";
			// print($year." ".$allocation_id." ".$shift_type." ".$month_code."\n");

			// calculate LOAD, DAY_MASK
			$days_in_month = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
			$load = "$value ";
			$load = trim(str_repeat($load, $days_in_month));
			$day_mask = pow(2, $days_in_month) - 1;
			// print($day_mask." ".$load."\n");

			$set = ['DAY_MASK' => $day_mask, 'LOAD' => $load];
			$where = ['RES_ALLOCATION_ID' => $allocation_id, 'MONTH_CODE' => $month_code, 'SHIFT_TYPE' => $shift_type];

			// skip if we create an allocation but it already exists
			if (($action == 'create') && ($db->select('MONTHLY_PATTERN', '*', $where)->count() > 0)) {
				continue;
			}

			$db_reply = $db->push('MONTHLY_PATTERN', $set, $where);
			// print_r($db_reply->fetchAll());
		}
	}
}


global $user;

$time_start = microtime(true);

ini_set('memory_limit', '512M');
// set_time_limit(60);

$year = NULL;
$activity = NULL;
$system = NULL;
$task_id = NULL;
$req_id = NULL;
$person_id = NULL;
$last_institute_id = NULL;
$category_code = NULL;
$action = NULL;

$id_prefix = 'row_';
$primary_key = ['YEAR', 'ALLOCATION_ID', 'SHIFT_TYPE'];
$pkey_separator = '_' . hash('crc32', implode(',', $primary_key)) . '_' ;

$months = [
	"JAN" => "01", "FEB" => "02", "MAR" => "03", "APR" => "04", "MAY" => "05", "JUN" => "06",
	"JUL" => "07", "AUG" => "08", "SEP" => "09", "OCT" => "10", "NOV" => "11", "DEC" => "12"
];

if (array_key_exists('year', $_REQUEST)) {
	$year = $_REQUEST['year'];
}

if (array_key_exists('activity', $_REQUEST)) {
	$activity = $_REQUEST['activity'];
}

if (array_key_exists('system', $_REQUEST)) {
	$system = $_REQUEST['system'];
}

if (array_key_exists('task_id', $_REQUEST)) {
	$task_id = $_REQUEST['task_id'];
}

if (array_key_exists('req_id', $_REQUEST)) {
	$req_id = $_REQUEST['req_id'];
}

if (array_key_exists('person_id', $_REQUEST)) {
	$person_id = $_REQUEST['person_id'];
}

if (array_key_exists('last_institute_id', $_REQUEST)) {
	$last_institute_id = $_REQUEST['last_institute_id'];
}

if (array_key_exists('category_code', $_REQUEST)) {
	$category_code = $_REQUEST['category_code'];
}


// Editor uses this
if (array_key_exists('action', $_REQUEST)) {
	$action = $_REQUEST['action'];
}

$time_init = microtime(true);

$row_ids = null;
if ($action == 'create') {
	$in_data = $_REQUEST['data'];

	$db->set_transaction($user_id, $task_transaction, "allocation_data create");

	$person_id = $in_data[0]['PERSON_ID'];
	$requirement_id = $in_data[0]['REQUIREMENT_ID'];
	$year = $in_data[0]['YEAR'];

	validate_create($requirement_id);
	validate_year($year);

	// look up requirement
	$requirement_load = $db->select('RES_REQUIREMENT', 'LOAD', ['ID' => $requirement_id])->fetch()['LOAD'];

	// Make sure we have an allocation_id
	$allocation_type = 'complex';
	$load = min(100, $requirement_load);
	$is_active = 'Y';

	$set = ['ALLOCATION_TYPE' => $allocation_type, 'LOAD' => $load, 'IS_ACTIVE' => $is_active];
	$where = ['PERSON_ID' => $person_id, 'RES_REQUIREMENT_ID' => $requirement_id];

	// create or update res_allocation
	$db->push('RES_ALLOCATION', $set, $where);

	// retrieve it
	$allocation = $db->select('RES_ALLOCATION', 'ID, LOAD', $where)->fetch();
	$allocation_id = $allocation['ID'];
	$allocation_load = $allocation['LOAD'];

	// Add year info
	$shift_type = 1;
	$row_ids[] = encode_row_id($id_prefix, $pkey_separator, $year, $allocation_id, $shift_type);
	$fields = array_fill_keys(array_keys($months), $allocation_load);
	push_year($action, $year, $allocation_id, $shift_type, $fields);

	// Make it look like a read
	$_REQUEST = [];
	$_POST = [];

} elseif ($action == 'edit') {
	// do the real edit(s)
	$in_data = $_REQUEST['data'];
//	print_r($in_data);

	$db->set_transaction($user_id, $shift_booking_transaction, "allocation_data edit");

	foreach($in_data as $row_id => $fields) {
		$row_ids[] = $row_id;
		// print("$row_id\n");

		// calculate YEAR, ALLOCATION_ID
		[$year, $allocation_id, $shift_type] = decode_row_id($row_id, $id_prefix, $pkey_separator);

		push_year($action, $year, $allocation_id, $shift_type, $fields);
	}
	// print_r($row_ids);

	// Make it look like a read
	$_REQUEST = [];
	$_POST = [];

} elseif ($action == 'remove') {
	// delete record(s)
	$in_data = $_REQUEST['data'];

	$db->set_transaction($user_id, $shift_booking_transaction, "allocation_data remove");

	foreach($in_data as $row_id => $fields) {
		// calculate YEAR, ALLOCATION_ID
		[$year, $allocation_id, $shift_type] = decode_row_id($row_id, $id_prefix, $pkey_separator);

		validate_edit_remove($allocation_id);
		validate_year($year);

		// delete all month patterns (12) in that year
		foreach($months as $month_name => $month) {
			$month_code = "$year$month";

			$where = ['RES_ALLOCATION_ID' => $allocation_id, 'MONTH_CODE' => $month_code, 'SHIFT_TYPE' => $shift_type];
			$db_reply = $db->delete('MONTHLY_PATTERN', $where);
			// print_r($db_reply->fetchAll());
		}
	}

	header('Content-Type: application/json');
	$data = ['data' => []];
	echo json_encode($data);
	exit(0);
}

$time_create_edit = microtime(true);

$user_roles = $db->select('USER_ROLE', '*', ['USERNAME' => $user])->fetchAll();

$data = [];

if (($year != null) || ($task_id != null) || ($req_id != null) ||
    ($person_id != null) || ($last_institute_id != null) ||
	($activity != null) || ($system != null) ||
	($row_ids != null)) {

	$view = 'ALL_ALLOCATIONS';

	$data = Editor::inst( $db, null, $primary_key )
		->readTable($view)
		->fields(
			Field::inst( 'RO' )
			    ->getFormatter( $to_bool )
			    ->set(false),

			Field::inst( 'PERSON_ID' )
				->getFormatter( $to_string )
				->validator( Validate::dbValues() )
				->validator( Validate::notEmpty(
					ValidateOptions::inst()
						->message( 'Select a User' )
					)
				)
				->set( Field::SET_CREATE ),
			Field::inst( 'FNAME' )
				->set(false),
			Field::inst( 'LNAME' )
				->set(false),
			Field::inst( 'PHOTO' )
				->getFormatter( $to_bool )
				->set(false),

			Field::inst( 'LNAME' )		// Field::inst( 'LAST_INSTITUTE' )
				->set(false),

			Field::inst( 'LAST_INSTITUTE_ID' )
				->getFormatter( $to_string )
				->set(false),

			Field::inst( 'LAST_INSTITUTE' )
				->set(false),

			Field::inst( 'YEAR' )
				// ->searchPaneOptions( SearchPaneOptions::inst() )
				->getFormatter( $to_string )
				->setFormatter( $from_string )
				->validator( Validate::notEmpty(
					ValidateOptions::inst()
						->message( 'Select a Year' )
					)
				)
				->validator( Validate::minNum(2008) )
				->set( Field::SET_CREATE ),

			Field::inst( 'TASK_ID' )
				->getFormatter( $to_string )
				->validator( Validate::dbValues() )
				->validator( Validate::notEmpty(
					ValidateOptions::inst()
						->message( 'Select a Task' )
					)
				)
				->set(false),
			Field::inst( 'TASK' )
				->set(false),

			Field::inst( 'REQUIREMENT_ID' )
				->getFormatter( $to_string )
				->setFormatter( $from_string )
				->validator( Validate::notEmpty(
					ValidateOptions::inst()
						->message( 'Select a Requirement' )
					)
				)
				->set( Field::SET_CREATE ),

			Field::inst( 'REQUIREMENT' ) ->set(false),
			Field::inst( 'ACTIVITY' ) ->set(false),
			Field::inst( 'ACTIVITY_ID' ) ->set(false),
			Field::inst( 'SYSTEM' ) ->set(false),
			Field::inst( 'SYSTEM_ID' ) ->set(false),

			Field::inst( 'JAN' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'FEB' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'MAR' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'APR' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'MAY' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'JUN' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'JUL' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'AUG' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'SEP' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'OCT' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'NOV' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
			Field::inst( 'DEC' ) ->getFormatter( $to_fte ) ->setFormatter( $from_fte ),
		)
		// For create and edit
		->where(function ( $q ) use ( $pkey_separator, $id_prefix, $row_ids ) {
			if ($row_ids != NULL) {
				// Disassemble DT_RowId
				$q->or_where( function ( $r ) use ( $pkey_separator, $id_prefix, $row_ids ) {
					foreach($row_ids as $row_id) {
						[$year, $allocation_id, $shift_type] = decode_row_id($row_id, $id_prefix, $pkey_separator);
						// print_r([$year, $allocation_id, $shift_type]);
						$r->where( 'SHIFT_TYPE', $shift_type )
						  ->where( 'ALLOCATION_ID', $allocation_id )
						  ->where( 'YEAR', $year );
					}
				} );
			}
		})
		->where(function ( $q ) use ( $year ) {
			if ($year != NULL) {
				$q->where( 'YEAR', $year );
			}
		})
		->where(function ( $q ) use ( $task_id ) {
			if ($task_id != NULL) {
				$q->where( 'TASK_ID', $task_id );
			}
		})
		->where(function ( $q ) use ( $req_id ) {
			if ($req_id != NULL) {
				$q->where( 'REQUIREMENT_ID', $req_id );
			}
		})
		->where(function ( $q ) use ( $person_id ) {
			if ($person_id != NULL) {
				$q->where( 'PERSON_ID', $person_id );
			}
		})
		->where(function ( $q ) use ( $last_institute_id ) {
			if ($last_institute_id != NULL) {
				$q->where( 'LAST_INSTITUTE_ID', $last_institute_id );
			}
		})

		->where(function ( $q ) use ( $activity ) {
			if ($activity != NULL) {
				$q->where( 'ACTIVITY', $activity );
			}
		})
		->where(function ( $q ) use ( $system ) {
			if ($system != NULL) {
				$q->where( 'SYSTEM', $system );
			}
		})
		->where(function ( $q ) use ( $category_code ) {
			if ($category_code != NULL) {
				$q->where( 'CATEGORY_CODE', $category_code );
			}
		})
		->write(false)
		->debug(true)
		->process( $_POST )
		->data();

		if (($action == null) || ($action == 'read')) {
			$person_ids = Options::inst()
			->table( 'PUB_PERSON' )
			->value( 'PUB_PERSON.ID' )
			->label( array('PUB_PERSON.LNAME', 'PUB_PERSON.FNAME') )
			->where( function ($q) {
				$q
					// no institutions
					// ->where( 'ID', '100000', '<' )
					// no users without a username/cern_upn
					->where( 'PUB_PERSON.USERNAME', null, '!=' );
			} )
			->render( function ( $row ) {
				return '<B>'.$row['PUB_PERSON.LNAME'].'</B>'.', '.$row['PUB_PERSON.FNAME'];
			})
			->exec($db);

			$task_ids = Options::inst()
			->table( 'TASK' )
			->value( 'TASK.ID' )
			->label( array('TASK.SHORT_TITLE', 'TASK.DESCRIPTION') )
			->leftJoin( 'WBS_NODE', 'WBS_NODE.ID', '=', 'TASK.WBS_ID')
			->leftJoin( 'ALL_ROLES',
			'(ALL_ROLES.ROLE = \'ROLE_ADMIN\') OR
			(ALL_ROLES.ROLE = \'ROLE_ACTIVITY_MANAGER\' AND ALL_ROLES.WBS_ID = WBS_NODE.PARENT_ID) OR
			(ALL_ROLES.ROLE = \'ROLE_WBS_MANAGER\' AND ALL_ROLES.WBS_ID = WBS_NODE.ID) OR
			(ALL_ROLES.ROLE = \'ROLE_SYSTEM_MANAGER\' AND ALL_ROLES.SYSTEM_ID = TASK.SYSTEM_ID)')
			->where( function ($q) use ( $user_id ) {
				$q
					->where_in( 'TASK.CATEGORY_CODE', ['Class 3', 'Upgrade Construction'])
					->where( function ($q) use ( $user_id ) {
						$q
						->where( 'ALL_ROLES.USER_ID', $user_id, '=' )
						->or_where( 'ALL_ROLES.USER_ID', 999980, '=' );
					} );
			} )
			->render( function ( $row ) {
				return '<B>'.$row['TASK.ID'].'</B>'.': '.$row['TASK.SHORT_TITLE']; 	// .': '.$row['DESCRIPTION'];
			} )
			->order( 'TASK.ID' )
			->exec($db);

			$tasks = $user_role_options->exec($db);

			$data['options']['TASK'] = $tasks;
			$data['options']['PERSON_ID'] = $person_ids;
			$data['options']['TASK_ID'] = $task_ids;
		}
} else {
	$data[ 'error' ] = "ERROR: too much data, please supply 'year', 'task_id', 'req_id', 'person_id' or 'system'";
	$data[ 'data' ] = [];
	$data[ 'debug' ] = [];
}

$time_read = microtime(true);

header('Content-Type: application/json');
$data[ 'action' ] = $action;
$version = file_get_contents('../version.txt');
$data[ 'version' ] = $version ? trim($version) : "unknown";
$data[ 'user' ] = $user;
$data[ 'user_id' ] = $user_id;
$data[ 'db' ] = $sql_details['db'];
$data[ 'has_role_admin' ] = has_role('ROLE_ADMIN');
$data[ 'has_role_role_manager' ] = has_role('ROLE_ROLE_MANAGER');
$data[ 'user_roles' ] = $user_roles;
$data[ 'timing' ] = [ number_format($time_init - $time_start, 2),
					  number_format($time_create_edit - $time_init, 2),
					  number_format($time_read - $time_create_edit, 2),
					  number_format($time_read - $time_start, 2)];
echo json_encode($data);

