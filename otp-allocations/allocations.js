const CAN_EDIT = 0, CAN_EDIT_BECAUSE = 1, RO = 2, SELECT = 3;
const YEAR = 4, PERSON_ID = 5, NAME = 6, LAST_INSTITUTE_ID = 7, LAST_INSTITUTE = 8;
const TASK_ID_REQ_ID = 9, TASK_REQUIREMENT = 10, TASK_ID = 11, TASK = 12, REQUIREMENT_ID = 13, REQUIREMENT = 14;
const ACTIVITY_SYSTEM = 15, ACTIVITY = 16, SYSTEM = 17;
const JAN = 18, FEB = JAN + 1, MAR = FEB + 1, APR = MAR + 1, MAY = APR + 1, JUN = MAY + 1;
const JUL = JUN + 1, AUG = JUL + 1, SEP = AUG + 1, OCT = SEP + 1, NOV = OCT + 1, DEC = NOV + 1;
const TOTAL = DEC + 1;

let db;
let editor; // use a global for the submit and return data rendering
let table;
let debug;
let is_test;
let extra_param = "";
let current_user;
let data_script = "allocations_data.php";
let user_roles;
let selected_row_count = 0;

let allow_new = false;
let allow_edit = false;

let year;
let activity;
let system;
let task_id;
let req_id;
let person_id;
let last_institute_id;
let category_code;

const months = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];

function log_state(prefix) {
    console.log(prefix);
    console.log("  year = ", year);
    console.log("  task_id = ", task_id);
    console.log("  req_id = ", req_id);
    console.log("  person_id = ", person_id);
    console.log("  last_institute_id = ", last_institute_id);
    console.log("  category_code = ", category_code);
    console.log("  activity = ", activity);
    console.log("  system = ", system);
}

function add_can_edit(data, user, user_roles) {
    current_user = user;
    let current_year = new Date().getFullYear();

    for (let i = 0; i < data.length; i++) {
        if (!has_role_admin(user, user_roles) && (data[i].YEAR < (current_year - 1))) {
            // No edits beyond past year
            data[i].CAN_EDIT = false;
            data[i].CAN_EDIT_BECAUSE = "BLOCKED";
        } else if (data[i].RO) {
            // we cannot change VIEWs
            data[i].CAN_EDIT = false;
            data[i].CAN_EDIT_BECAUSE = "VIEW";
        } else {
            let can_edit_and_because = can_edit(user, user_roles, data[i].SYSTEM_ID, data[i].ACTIVITY_ID, '');
            data[i].CAN_EDIT = can_edit_and_because.can_edit;
            data[i].CAN_EDIT_BECAUSE = can_edit_and_because.can_edit_because;
        }
    }
}



$(document).ready(function() {

    function inline_edit(row) {
        table.buttons( ['.buttons-create'] ).enable(false);
        editor.on('preClose', function(e) {
            if (debug) console.log('preClose', e);
            table.buttons( ['.buttons-create'] ).enable(has_role_admin(current_user, user_roles));
        });

        editor.inline(
            table.cells(row, ".editable").nodes(), {
                onBlur: 'submit',
            }
        );
    }

    // use select2
    // $('.js-example-basic-single').select2();

    let query_string = window.location.search;
    let params = new URLSearchParams(query_string);
    console.log(new URLSearchParams(window.location.hash.substring(1)));
    console.log(params);

    // query string ?test
    is_test = params.get("test") !== null;
    extra_param += is_test ? "&test" : "";

    // query string ?debug
    debug = params.get("debug") !== null;
    extra_param += debug ? "&debug" : "";

    // local user
    extra_param += params.get("user") ? "&user=" + params.get("user") : "";

    // query for one year (and system) or one task or one person
    year = params.get("year");
    task_id = params.get("task_id");
    req_id = params.get("req_id");
    person_id = params.get("person_id");
    last_institute_id = params.get("last_institute_id");
    activity = params.get("activity");
    system = params.get("system");
    category_code = params.get("category_code");

    if ((year == null) && (task_id == null) && (req_id == null) &&
        (person_id == null) && (last_institute_id == null) &&
        (activity == null) && (system == null)) {
        // reload page with defaults for the current year, General Tasks and Class 3
        params.set("year", new Date().getFullYear());
        params.set("activity", "Detector Operation");
        params.set("system", "General Tasks");
        params.set("category_code", "Class 3");
        params.delete("task_id");
        params.delete("req_id");
        window.location.search = params.toString();
        return;
    }


    // defaults if year is set
    if (year != null) {
        if ((activity == null) || (system == null) || (category_code == null)) {
            if (activity == null) {
                // reload page for Detector Operation
                params.set("activity", "Detector Operation");
            }

            if (system == null) {
                // reload page for General Tasks
                params.set("system", "General Tasks");
            }

            if (category_code == null) {
                // reload page for Class 3
                params.set("category_code", "Class 3");
            }

            window.location.search = params.toString();
            return;
        }
    }

    // remove incompatible options, in order of importance
    let original_params_size = params.size;
    if ((params.get("year") != null) || (params.get("category_code") != null) || (params.get("activity") != null) || (params.get("system") != null)) {
        params.delete("person_id");
        params.delete("last_institute_id");
        params.delete("task_id");
        params.delete("req_id");
    }

    if (params.get("task_id") != null) {
        params.delete("year");
        params.delete("activity");
        params.delete("system");
        params.delete("category_code");
        params.delete("person_id");
        params.delete("last_institute_id");
        params.delete("req_id");
    }

    if (params.get("req_id") != null) {
        params.delete("year");
        params.delete("activity");
        params.delete("system");
        params.delete("category_code");
        params.delete("person_id");
        params.delete("last_institute_id");
        params.delete("task_id");
    }

    if (params.get("person_id") != null) {
        params.delete("year");
        params.delete("activity");
        params.delete("system");
        params.delete("category_code");
        params.delete("last_institute_id");
        params.delete("task_id");
        params.delete("req_id");
    }

    if (params.get("last_institute_id") != null) {
        params.delete("year");
        params.delete("activity");
        params.delete("system");
        params.delete("category_code");
        params.delete("person_id");
        params.delete("task_id");
        params.delete("req_id");
    }

    if (original_params_size != params.size) {
        window.location.search = params.toString();
        return;
    }

    // Create the selectors
    const year_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let year = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.delete("task_id");
        params.delete("req_id");
        params.set("year", year);
        window.location.search = params.toString();
    });

    for (let year = (new Date().getFullYear() + 2); year >= 2008; year--) {
        year_select.append('<option value="' + year + '">' + year + '</option>');
    }

    const category_code_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        // $.fn.dataTable.util.escapeRegex()
        let category_code = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.delete("task_id");
        params.delete("req_id");
        params.set("category_code", category_code);
        window.location.search = params.toString();
    });

    category_code_select.append('<option value="' + 'Class 3' + '">' + 'Class 3' + '</option>');
    category_code_select.append('<option value="' + 'Upgrade Construction' + '">' + 'Upgrade Construction' + '</option>');

    const activity_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let activity = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.delete("task_id");
        params.delete("req_id");
        params.set("activity", activity);
        window.location.search = params.toString();
    });

    $.ajax( {
        url: 'activity_data.php',
        type: "POST",
        dataType: 'json',
        success: function ( json ) {
            let options = json.options.ACTIVITY;
            for (let i = 0; i < options.length; i++) {
                activity_select.append('<option value="' + options[i].label + '">' + options[i].label + '</option>');
            }
            activity_select.val(activity);
        }
    });

    const system_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let system = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.delete("task_id");
        params.delete("req_id");
        params.set("system", system);
        window.location.search = params.toString();
    });

    $.ajax( {
        url: 'system_data.php',
        type: "POST",
        dataType: 'json',
        success: function ( json ) {
            let options = json.options.SYSTEM;
            for (let i = 0; i < options.length; i++) {
                system_select.append('<option value="' + options[i].label + '">' + options[i].label + '</option>');
            }
            system_select.val(system);
        }
    });

    const task_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let task_id = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.delete("req_id");
        params.set("task_id", task_id);
        window.location.search = params.toString();
    });

    $.ajax( {
        url: 'task_data.php',
        type: "POST",
        dataType: 'json',
        success: function ( json ) {
            let options = json.options.TASK;
            for (let i = 0; i < options.length; i++) {
                task_select.append('<option value="' + options[i].value + '">' + '<B>' + options[i].value + '</B>: ' + options[i].label + '</option>');
            }
            if (task_id != null) {
                task_select.val(task_id);
            }
        }
    });

    const req_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let req_id = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.delete("task_id");
        params.set("req_id", req_id);
        window.location.search = params.toString();
    });

    if (req_id != null) {
        $.ajax( {
            // FIXME
            url: 'req_data.php?REQ_ID=' + req_id,
            type: "POST",
            dataType: 'json',
            success: function ( json ) {
                let options = json.options.REQUIREMENT;
                for (let i = 0; i < options.length; i++) {
                    req_select.append('<option value="' + options[i].value + '">' + '<B>' + options[i].value + '</B>: ' + options[i].label + '</option>');
                }
                req_select.val(req_id);
                task_select.val(json.options.TASK_ID.value);
                task_id = json.options.TASK_ID.value;
            }
        });
    }

    const person_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let person_id = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.set("person_id", person_id);
        window.location.search = params.toString();
    });

    $.ajax( {
        url: 'user_data.php',
        type: "POST",
        dataType: 'json',
        success: function ( json ) {
            let options = json.options.PERSON;
            for (let i = 0; i < options.length; i++) {
                person_select.append('<option value="' + options[i].value + '">' + options[i].label + '</option>');
            }
            person_select.val(person_id);
        }
    });

    const last_institute_select = $('<select class="js-example-basic-single label"></select>')
    .on('change', function () {
        let last_institute_id = $(this).val();
        let query_string = window.location.search;
        let params = new URLSearchParams(query_string);
        params.set("last_institute_id", last_institute_id);
        window.location.search = params.toString();
    });

    $.ajax( {
        url: 'institute_data.php',
        type: "POST",
        dataType: 'json',
        success: function ( json ) {
            let options = json.options.INSTITUTE;
            for (let i = 0; i < options.length; i++) {
                last_institute_select.append('<option value="' + options[i].value + '">' + options[i].label + '</option>');
            }
            last_institute_select.val(last_institute_id);
        }
    });

    // Add selectors based on view
    if (year != null) {
        year_select.val(year);
        year_select.appendTo($("#selectors"));

        activity_select.appendTo($("#selectors"));
        system_select.appendTo($("#selectors"));

        category_code_select.appendTo($("#selectors"));
        category_code_select.val(category_code);
    }

    if (task_id != null) {
        task_select.appendTo($("#selectors"));
    } else if (req_id != null) {
        task_select.appendTo($("#selectors"));
        req_select.appendTo($("#selectors"));
    }

    if (person_id != null) {
        person_select.appendTo($("#selectors"));
    }

    if (last_institute_id != null) {
        last_institute_select.appendTo($("#selectors"));
    }

    if ((year == null) && (system != null)) {
        activity_select.appendTo($("#selectors"));
        system_select.appendTo($("#selectors"));
    }

    let fixed_left_columns = (year != null ? debug ? 10 : 6 : 0) +                       // SELECT, NAME, INST, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             (task_id != null ? debug ? 12 : 6 : 0) +                    // SELECT, YEAR, NAME, INST, REQ_ID, REQUIREMENT
                             (req_id != null ? debug ? 10 : 4 : 0) +                     // SELECT, YEAR, NAME, INST
                             (person_id != null ? debug ? 10 : 5 : 0) +                  // SELECT, YEAR, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             (last_institute_id != null ? debug ? 10 : 6 : 0) +          // SELECT, YEAR, NAME, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM
                             ((system != null) && (year == null) ? debug ? 10 : 6 : 0);  // SELECT, YEAR, NAME, INST, TASK_ID_REQ_ID, TASK_REQUIREMENT,
    let fixed_right_columns = 1;

    let is_local = window.location.hostname == 'localhost' || window.location.hostname == 'otp-next.localhost';

    if (debug) {
        console.log(window.location);
        console.log("test = ", is_test);
        console.log("debug = ", debug);
        console.log("data_script = ", data_script);
        log_state("init");
    }




    editor = new DataTable.Editor({
        ajax: {
            url: data_script,
            data: function ( d ) {
                if ((d.action == 'create') || (d.action == 'remove')) {
                    return;
                }

                // NOTE below breaks double editing as 'table' changes
                // related test: test_edit_inline_system_twice
                // Send also initial values before change in 'init_data'
                // [row, value] = Object.entries(d.data)[0];
                // [table, value] = Object.entries(value)[0];
                // [field, value] = Object.entries(value)[0];

                // // FIXME check for row, for multi-submit
                // d.init_data = {}
                // d.init_data[row] = {}
                // d.init_data[row][table] = {}
                // d.init_data[row][table][field] = editor.init_data[table][field]

                if (debug) console.log("Data populated for EDIT (currently disabled)");
                if (debug) console.log(d);
            }
        },
        table: "#table",
        template: '#customForm',
        fields: [
            // all Editable fields
            {
                label: "Name:",
                name: "PERSON_ID",
                type: "datatable",
                config: {
                    paging:         true,
                    scrollY:        150,
                    scrollCollapse: true,
                    deferRender:    true,
                    // See #156, cannot use scroller as selected person (beyond the first few) does not get shown (scrolled to)
                    // scroller can be true here as we do only allow inline edits, but seems to give a stacktrace
                    scroller:       false,
                    columns: [
                        {
                            title: '<b>Last name</b>, First name',
                            data: 'label'
                        }
                    ]
                }
            },
            {
                label: "Name:",
                name: "SELECTED_PERSON_ID",
                type: "display",
                // data: function ( row, type, val, meta ) {
                //     // Combine the first and last names into a single table field
                //     return `<b>${row.LNAME}</b>, ${row.FNAME}`;
                // },
            },
            {
                label: "Selected:",
                name: "LNAME",
                type: "hidden"
            },
            {
                label: "Selected:",
                name: "FNAME",
                type: "hidden"
            },
            {
                label: "Photo:",
                name: "PICTURE_URL",
                type: "image"
            },

            {
                label: "Year:",
                name: "YEAR",
                type: "select",
                def: year,
                options: [ 2025, 2024, 2023 ]
            },
            {
                label: "Year:",
                name: "SELECTED_YEAR",
                type: "display",
            },

            {
                label: "Task:",
                name: "TASK_ID",
                type: "datatable",
                config: {
                    paging:         true,
                    scrollY:        150,
                    scrollCollapse: true,
                    deferRender:    true,
                    scroller:       false,
                    columns: [
                        {
                            title: '<b>Task ID</b>: Name',
                            data: 'label'
                        }
                    ],
                    order: 0
                }
            },
            {
                label: "Task:",
                name: "SELECTED_TASK_ID",
                type: "display",
            },

            {
                label: "Requirement:",
                name: "REQUIREMENT_ID",
                type: "select",
                def: req_id,
                placeholder: "Select a Requirement",
                placeholderValue: ""
            },
            {
                label: "Requirement:",
                name: "SELECTED_REQUIREMENT_ID",
                type: "display",
            },

            {
                label: "Jan:",
                name: "JAN",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Feb:",
                name: "FEB",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Mar:",
                name: "MAR",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Apr:",
                name: "APR",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "May:",
                name: "MAY",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Jun:",
                name: "JUN",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Jul:",
                name: "JUL",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Aug:",
                name: "AUG",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Sep:",
                name: "SEP",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Oct:",
                name: "OCT",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Nov:",
                name: "NOV",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Dec:",
                name: "DEC",
                def: "0",
                setFormatter: function(val, _field) { return parseFloat(val).toFixed(2); },
                className: 'text-right',
            },
            {
                label: "Total:",
                name: "TOTAL",
                def: function() {
                    let total = 0.0;
                    for (const month of months) {
                        total += parseFloat(editor.field(month).get());
                    }
                    return (total/12).toFixed(2);
                },
                className: 'text-right',
            }
        ]
    } );

    // send also original data
    editor.on('initEdit', function (_e, _node, data, _items, type) {
        // Called just before editing (inline or normal) is done.
        if (debug) console.log('initEdit', type, JSON.stringify(data));

        // TBD Store initial data before change (as prepared in ajax.data)
        // to enable to detect (and possibly resolve) conflicts (a la git)
        editor.init_data = data;
    });

    editor.on('preOpen', function(e, mode, action) {
        // Called before a form (inline or normal) is opened. Called after initEdit.
        if (debug) console.log('preOpen', mode, action, e);

        let modifier = editor.modifier();
        let data = table.row( modifier ).data();

        // No edits/deletes if CAN_EDIT field is false
        if (data && (typeof data !== "undefined") && !data.CAN_EDIT) {
            if (debug) console.log("No edits if CAN_EDIT field is false");
            return false;
        }

        // For edit window
        let inline = mode !== 'main';

        console.log(data);

        log_state("preOpen");

        // set_show(editor.field('PERSON_ID'), person_id == null && !inline);
        // set_show(editor.field('YEAR'), year == null && !inline);
        // set_show(editor.field('TASK_ID'), task_id == null && !inline);
        // set_show(editor.field('REQUIREMENT_ID'), req_id == null && !inline);
        // editor.field('TASK_ID').val(task_id);
        // editor.field('REQUIREMENT_ID').val(req_id);
        set_field(db, editor, 'PERSON_ID', person_id, 'SELECTED_PERSON_ID', inline);
        set_field(db, editor, 'YEAR', year, 'SELECTED_YEAR', inline);
        set_field(db, editor, 'TASK_ID', task_id, 'SELECTED_TASK_ID', inline);
        set_field(db, editor, 'REQUIREMENT_ID', req_id, 'SELECTED_REQUIREMENT_ID', inline);

        set_enabled(editor.field('JAN'), inline);
        set_enabled(editor.field('FEB'), inline);
        set_enabled(editor.field('MAR'), inline);
        set_enabled(editor.field('APR'), inline);
        set_enabled(editor.field('MAY'), inline);
        set_enabled(editor.field('JUN'), inline);
        set_enabled(editor.field('JUL'), inline);
        set_enabled(editor.field('AUG'), inline);
        set_enabled(editor.field('SEP'), inline);
        set_enabled(editor.field('OCT'), inline);
        set_enabled(editor.field('NOV'), inline);
        set_enabled(editor.field('DEC'), inline);
        set_enabled(editor.field('TOTAL'), inline);

        validated = false;

        // set_change_handler(editor, 'PERSON_ID', mode, debug);
        // set_change_handler(editor, 'REQUIREMENT_ID', mode, debug);
    } );

    // re-align columns
    editor.on('open', function (_e, mode, _action) {
        if (mode === 'inline') {
            table.columns.adjust();

            // Select editable text rather than insert. Click again to insert.
            $('#table div.DTE input').select();
        }
    });

    let validated = false;

    function validate() {
        // FIXME, works for year but not for person_id
        if (validated) {
            console.log("Re-validate");

            let person_id = editor.field('PERSON_ID');
            console.log("  pid:", person_id.val());
            person_id.error(person_id.val() == "" ? "Select a Person": "");

            if (req_id === null) {
                let requirement_id = editor.field('REQUIREMENT_ID');
                console.log("  rid:", requirement_id.val());
                requirement_id.error(requirement_id.val() == "" ? "Select a Requirement": "");
            }

            for (const month of months) {
                let value = editor.field(month);
                let value_value = parseFloat(value.val());
                value.error(isNaN(value_value) ? "Value must be a number" : value_value < 0 ? "Value must be larger or equal to 0": "");
            }
        }
    }

    editor.on('preSubmit', function(_e, data, action) {
        if (debug) console.log('preSubmit', action, data, editor.display());

        // if TOTAL is set
        let changeAllMonths = false;
        let row = data.data[Object.keys(data.data)[0]];

        if (("TOTAL" in row) &&
            !("JAN" in row) && !("FEB" in row) && !("MAR" in row) && !("APR" in row) && !("MAY" in row) && !("JUN" in row) &&
            !("JUL" in row) && !("AUG" in row) && !("SEP" in row) && !("OCT" in row) && !("NOV" in row) && !("DEC" in row)) {
            let total = row.TOTAL;

            // Check if all values are equal to eachother
            changeAllMonths = true;
            let set = new Set();
            for (const month of months) {
                set.add(editor.field(month).get());
            }
            if (set.size != 1) {
                if (!confirm( 'You are about to change all Month values to ' + total + ". Please confirm?" ) ) {
                    changeAllMonths = false;
                }
            }

            if (changeAllMonths) {
                for (const month of months) {
                    row[month] = total;
                }
            }
        }

        // TOTAL is not stored
        if (changeAllMonths) {
            delete row.TOTAL;
        }

        // REMOVE all entries that do not change inline
        let mode = editor.display();
        if (mode == 'inline') {
            delete row.YEAR;
            delete row.PICTURE_URL;
            delete row.PERSON_ID;
            delete row.REQUIREMENT_ID;
            delete row.TASK_ID;
            delete row.TOTAL;
        }

        // only needed to fake users in a local environment\
        if (params.get("user")) {
            data.user = params.get("user");
        }

        validated = true;
        // Validate fields before submitting to server

        // DELETE is ok
        if (action == "remove") {
            if (debug) console.log("DELETE is ok");
            return true;
        }

        validate();

        // Any missing fields, abort submit
        if ( this.inError() ) {
            console.log("Some error prevented submit");
            if (debug) {
                console.log(editor.field('PERSON_ID').error());
                console.log(editor.field('REQUIREMENT_ID').error());
            }
            return false;
        }

        return true;
    });

    // handle server errors
    editor.on('postSubmit', function (_e, json, data, action, xhr) {
        if (debug) console.log("postSubmit", action, json, data, xhr);

        if (json === null) {
            console.log(data);
            console.log('responseText:', xhr.responseText);
            alert("Server error in script: " + data_script + ", look at console log.");
        }

        update_roles(json.data, current_user, user_roles, table);

        //   if ( json.FieldErrors ) {
        //      .... take a look at json.fieldErrors
        //   }

        // console.log(json.data[0].COMMITMENTS.ID)

        // table.page.jumpToData( 22676, 3 );
    });

    // jump to new or edited record
    editor.on( 'submitComplete', function ( e, json, data, action ) {
        if (debug) console.log("submitComplete", data, action);
        if (( action === 'create') || ( action === 'edit' )) {
            let row_id_object = $(`#${data.DT_RowId}`);

            if (is_test) {
                row_id_object.addClass("test_allocations_record");
            }

            let row = table.row(row_id_object);
            row.scrollTo();

            if ( action === 'create') {
                inline_edit(row);
            }
        }
    });

    // Sets the list of options for requirements
    editor.dependent( 'TASK_ID', 'requirement_data.php');

    // Sets the selected user and displays his/her picture
    editor.dependent( 'PERSON_ID', function ( val, data, callback, e) {
        if (debug) console.log("Dependency called for PERSON_ID -> PERSON_DATA, PICTURE_URL", val, data, e);
        $.ajax( {
            url: 'person_data.php',
            type: "POST",
            data: data,
            dataType: 'json',
            success: function ( json ) {
                let picture_url = json.options.PICTURE_URL;
                let picture_url_field = $('#DTE_Field_PICTURE_URL');
                // Set my own picture if running locally
                picture_url =  !picture_url ? '../img/no_photo.png' : is_local ? '../img/duns.jpg' : picture_url;
                if (debug) console.log("picture_url: ", picture_url);
                $('img', picture_url_field).attr( 'src', picture_url);
                callback(json);
            }
        } );

        return {};
    });

    // Switched the create button color
    editor.dependent(['REQUIREMENT_ID', 'PERSON_ID', 'YEAR'],
        function ( val, data, _callback, e) {
            if (debug) console.log("Dependency called for any ALLOCATION field update ", val, data, e);
            console.log("REQUIREMENT_ID", data.values.REQUIREMENT_ID);
            console.log("PERSON_ID", data.values.PERSON_ID);
            console.log("YEAR", data.values.YEAR);

            if (data.values.REQUIREMENT_ID == '' ||
                data.values.PERSON_ID == '' ||
                data.values.YEAR == '') {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-green");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-red");
                $('div.DTE_Form_Buttons button.submit').attr("disabled", true);
            } else {
                $('div.DTE_Form_Buttons button.submit').removeClass("gradient-red");
                $('div.DTE_Form_Buttons button.submit').addClass("gradient-green");
                $('div.DTE_Form_Buttons button.submit').attr("disabled", false);
            }

            validate();

            return {};
        }
    );

    // Activate an inline edit on click of a table cell
    $('#table').on( 'click', 'tbody td:not(.no-inline-edit)', function (e) {
        if (debug) console.log("Clicked any cell except no-inline-edit", e);
        // Focus on the input in the cell that was clicked when Editor opens
        editor.one( 'open', () => {
            $('input', this).focus();
        });

        inline_edit(this.parentNode);
    } );

    let hide = [CAN_EDIT, CAN_EDIT_BECAUSE, SELECT,
                PERSON_ID, LAST_INSTITUTE_ID, LAST_INSTITUTE,
                TASK_ID_REQ_ID, TASK, TASK_REQUIREMENT, REQUIREMENT,
                ACTIVITY_SYSTEM, ACTIVITY, SYSTEM,
                JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, TOTAL];
    let show = [REQUIREMENT_ID];
    if (year == null) {
        show.push(YEAR);
    } else {
        hide.push(YEAR);
    }
    if (task_id == null) {
        show.push(TASK_ID);
    } else {
        hide.push(TASK_ID);
    }
    if (person_id == null) {
        show.push(NAME);
    } else {
        hide.push(NAME);
    }

    // Setup DataTable
    table = new DataTable('#table', {
        // autoFill: {
        //     columns: [JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, TOTAL],
        //     editor: editor
        // },
        // keys: {
        //     columns: [JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, TOTAL],
        //     editor: editor
        // },
        lengthChange: false,

        layout: {
            top2Start: 'buttons',
            top2End: 'search',
            top: {
                // searchPanes: {
                //     cascadePanes: true
                // }
            },
            topStart: null,
            topEnd: null,
            bottom: null,
            bottomStart: null,
            bottomEnd: null
        },
        ajax: {
            url: data_script + query_string,
            type: "POST",
            dataSrc: function ( json ) {
                current_user = json.user;

                // Update User Roles
                user_roles = decode_user_roles(json.options.TASK);
                console.log("User Roles", user_roles);

                // Add authorization fields
                set_user(json.user, json.has_role_admin, json.has_role_role_manager, user_roles, table);
                update_roles(json.data, current_user, user_roles, table);

                db = json.db;
                let dbobject = database(json.db);
                $('#table_db').text(dbobject.text);
                $('#table_db').addClass(dbobject.clazz);

                allow_new = json.options.TASK_ID.length > 0;
                allow_edit = allow_new;

                return json.data;
            }
        },
        fixedHeader: {
            header: true,
            footer: true
        },
        fixedColumns: {
            leftColumns: fixed_left_columns,
            rightColumns: fixed_right_columns
        },
        scrollX:        true,
        scrollY:        '50vh',
        scrollCollapse: true,

        deferRender:    true,

        // switch off scroller and paging in case of a test to make sure it loads all records
        paging:         !is_test,
        scroller:       !is_test,

        // serverSide:     true,

        // FIXME, seems to introduce problems with paging and rendering
        // stateSave:      true,  // NOTE: delete local storage for searchpanes to work first time...

        columnDefs: [
            // {
            //     searchPanes: {
            //         show: false,
            //     },
            //     targets: hide,
            // },
            // {
            //     searchPanes: {
            //         initCollapsed: true,
            //         show: true,
            //         orthogonal: 'sp'
            //     },
            //     targets: show,
            // },

            {
                targets: [JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC],
                render: function ( data, _type, _row, _meta ) {
                    return data.toFixed(2);
                },
            },
            {
                targets: [TOTAL],
                render: function ( _data, _type, row, _meta ) {
                    let total = 0;
                    for (const month of months) {
                        total += row[month];
                    }
                    return (total/12).toFixed(2);
                },
            },
        ],

        // serverSide: true,

        columns: [
            {
                data: "CAN_EDIT",
                className: "can_edit",
                visible: false
            },
            {
                data: "CAN_EDIT_BECAUSE",
                className: "can_edit_because",
                visible: false
            },
            {
                data: "RO",
                className: 'ro',
                visible: false
            },
            {
                // SELECT CHECKBOX
				data: null,
				defaultContent: '',
				className: 'select no-inline-edit',
				orderable: true,
                searchable: false,
                // editField: "ID",
                render: function ( data, type, row ) {
                    // if (debug) console.log(type+" "+data+" "+row.CAN_EDIT);
                    return type == 'sort' ? row.CAN_EDIT : data;
                },
			},

            {
                data: "YEAR",
                render: function ( data, type, row ) {
                    return type == 'display' ? `<a href="?year=${row.YEAR}${extra_param}">${data}</a>` : data;
                },
                className: 'year no-inline-edit dt-body-right dt-foot-right',
                defaultContent: '',
                visible: false
            },

           {
                data: "PERSON_ID",
                className: 'personid no-inline-edit dt-body-right dt-foot-right',
                defaultContent: '',
                visible: false
            },
            {
                // NAME
                data: function ( row, _type, _set ) {
                    // Combine the first and last names into a single table field
                    return `<B>${row.LNAME}</B>, ${row.FNAME}`;
                },
                render: function ( data, type, row ) {
                    return type == 'display' ? `<a href="?person_id=${row.PERSON_ID}${extra_param}"><b>${row.LNAME}</b>, ${row.FNAME}</a>` : data;
                },
                className: 'person_id no-inline-edit',
                visible: false
            },

            {
                data: "LAST_INSTITUTE_ID",
                className: 'last_institute_id',
                defaultContent: '',
                visible: false
            },

            {
                data: "LAST_INSTITUTE",
                render: function ( data, type, row ) {
                    return type == 'display' ? `<a href="?last_institute_id=${row.LAST_INSTITUTE_ID}${extra_param}">${data}</a>` : data;
                },
                className: 'last_institute',
                defaultContent: '',
                visible: false
            },

            {
                // TASK_ID_REQ_ID
                data: function ( row, type, _set ) {
                    return type == 'sort' ? String(row.TASK_ID).padStart(7, '0') + '-' + String(row.REQUIREMENT_ID).padStart(7, '0') : `${row.TASK_ID} // ${row.REQUIREMENT_ID}`;
                },
                render: function ( data, type, row ) {
                    return type == 'display' ? `<a href="?task_id=${row.TASK_ID}${extra_param}">${row.TASK_ID}</a> // <a href="?req_id=${row.REQUIREMENT_ID}${extra_param}">${row.REQUIREMENT_ID}</a>` : data;
                },
                className: 'task_id_reqid no-inline-edit',
                visible: false
            },

            {
                // TASK_REQUIREMENT
                data: function ( row, _type, _set ) {
                    return `${row.TASK} // ${row.REQUIREMENT}`;
                },
                className: 'task_requirement',
                visible: false
            },

            {
                data: "TASK_ID",
                className: 'taskid no-inline-edit dt-body-right dt-foot-right',
                defaultContent: '',
                render: {
                    _: function ( data, type, row, _meta ) {
                        return type == 'display' ? `<a href="?task_id=${row.TASK_ID}${extra_param}">${data}</a>` : data;
                    },
                    sp: function ( _data, _type, row, _meta ) {
                        return `<b>${row.TASK_ID}</b>: ${row.TASK}`;
                    }
                },
                visible: false
            },
            {
                data: "TASK",
                editField: "TASK_ID",
                className: 'task no-inline-edit dt-foot-right',
                defaultContent: '',
                visible: false
            },

            {
                data: "REQUIREMENT_ID",
                className: 'reqid dt-body-right dt-foot-right',
                defaultContent: '',
                render: {
                    _: function ( data, type, row, _meta ) {
                        return type == 'display' ? `<a href="?req_id=${row.REQUIREMENT_ID}${extra_param}">${data}</a>` : data;
                    },
                    sp: function ( _data, _type, row, _meta ) {
                        return `<b>${row.REQUIREMENT_ID}</b>:` +  (row.REQUIREMENT != null ? ` ${row.REQUIREMENT}` : '');
                    }
                },
                visible: false
            },
            {
                data: "REQUIREMENT",
                editField: "REQUIREMENT_ID",
                className: 'requirement dt-foot-right',
                defaultContent: '',
                visible: false
            },

            {
                // ACTIVITY_SYSTEM
                data: function ( row, type, _set ) {
                    return type == 'sort' ? row.ACTIVITY + '-' + row.SYSTEM : `${row.ACTIVITY} // ${row.SYSTEM}`;
                },
                render: function ( data, type, row ) {
                    return type == 'display' ? `<a href="?activity=${row.ACTIVITY}&system=${row.SYSTEM}${extra_param}">${data}</a>` : data;
                },
                className: 'activity_system no-inline-edit',
                visible: false
            },

            {
                data: "ACTIVITY",
                editField: "ACTIVITY_ID",
                className: 'activity dt-foot-right',
                defaultContent: '',
                visible: false
            },

            {
                data: "SYSTEM",
                editField: "SYSTEM_ID",
                className: 'system dt-foot-right',
                defaultContent: '',
                visible: false
            },

            {
                data: "JAN",
                className: 'jan value editable dt-body-right dt-foot-right'
            },
            {
                data: "FEB",
                className: 'feb value editable dt-body-right dt-foot-right'
            },
            {
                data: "MAR",
                className: 'mar value editable dt-body-right dt-foot-right'
            },
            {
                data: "APR",
                className: 'apr value editable dt-body-right dt-foot-right'
            },
            {
                data: "MAY",
                className: 'may value editable dt-body-right dt-foot-right'
            },
            {
                data: "JUN",
                className: 'jun value editable dt-body-right dt-foot-right'
            },
            {
                data: "JUL",
                className: 'jul value editable dt-body-right dt-foot-right'
            },
            {
                data: "AUG",
                className: 'aug value editable dt-body-right dt-foot-right'
            },
            {
                data: "SEP",
                className: 'sep value editable dt-body-right dt-foot-right'
            },
            {
                data: "OCT",
                className: 'oct value editable dt-body-right dt-foot-right'
            },
            {
                data: "NOV",
                className: 'nov value editable dt-body-right dt-foot-right'
            },
            {
                data: "DEC",
                className: 'dec value editable dt-body-right dt-foot-right'
            },

            {
                data: null,
                editField: "TOTAL",
                className: 'total value editable dt-body-right dt-foot-right'
            },
        ],
        order: [[ YEAR, 'desc'], [NAME, 'asc' ], [TASK_ID_REQ_ID, 'asc' ]],
        select: {
            style:    'os',
            selector: '.select-enabled',
            blurable: true
        },
        buttons: [
            // {
            //     extend: "createInline",
            //     editor: editor,
            //     // formOptions: {
            //     //     submitTrigger: -2,
            //     //     submitHtml: '<i class="fa fa-play"/>'
            //     // }
            // },
            {
                extend: "create",
                editor: editor,
                formTitle: "Create New Allocation",
                formButtons: [
                    { text: 'Create', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            // { extend: "createInline", editor: editor, text: 'New (Inline)' },
            // {   // No need to edit as the ID = YEAR, REQ_ID, PID
            //     extend: "edit",
            //     editor: editor,
            //     formTitle: "Edit Allocation",
            //     formButtons: [
            //         { text: 'Edit', className: 'submit', action: function () { this.submit(); } },
            //         { text: 'Cancel', className: 'close', action: function () { this.close(); } }
            //     ]
            // },
            // {   // No need to duplicate as the ID = YEAR, REQ_ID, PID
            //     extend: "selected",
            //     text: 'Duplicate',
            //     action: function ( _e, _dt, _node, _config ) {
            //         // Start in edit mode, and then change to create
            //         editor
            //             .edit( table.rows( {selected: true} ).indexes(), {
            //                 title: 'Duplicate Allocation',
            //                 buttons: [
            //                     { text: 'Create from existing', className: 'submit', action: function () { this.submit(); } },
            //                     { text: 'Cancel', className: 'close', action: function () { this.close(); } }
            //                 ]
            //             } )
            //             .mode( 'create' );
            //     }
            // },
            {
                extend: "remove",
                editor: editor,
                formTitle: "Delete Allocation",
                formButtons: [
                    { text: 'Delete', className: 'submit', action: function () { this.submit(); } },
                    { text: 'Cancel', className: 'close', action: function () { this.close(); } }
                ]
            },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    // 'copy',
                    'excel',
                    'csv',
                    // 'pdf',
                    'print'
                ]
            }
        ],
        rowCallback: function( row, data ) {
            if ( data.CAN_EDIT ) {
                $('td.select', row).addClass( 'select-checkbox' );
                $('td.select', row).addClass( 'select-enabled' );
            } else {
                $('td.select', row).removeClass( 'select-checkbox' );
                $('td.select', row).removeClass( 'select-enabled' );
            }
        },
        footerCallback: function ( _row, _data, _start, _end, _display ) {
            let api = this.api();

            count_unique(api, YEAR);
            count_unique(api, NAME);
            count_unique(api, LAST_INSTITUTE_ID);
            count_unique(api, LAST_INSTITUTE);
            count_unique(api, TASK_ID_REQ_ID);
            count_unique(api, TASK_ID);
            count_unique(api, REQUIREMENT_ID);
            count_unique(api, ACTIVITY_SYSTEM);
            count_unique(api, ACTIVITY);
            count_unique(api, SYSTEM);

            let total = 0;
            total += sum(api, JAN);
            total += sum(api, FEB);
            total += sum(api, MAR);
            total += sum(api, APR);
            total += sum(api, MAY);
            total += sum(api, JUN);
            total += sum(api, JUL);
            total += sum(api, AUG);
            total += sum(api, SEP);
            total += sum(api, OCT);
            total += sum(api, NOV);
            total += sum(api, DEC);
            $( api.column( TOTAL ).footer() ).html((total/12).toFixed(2));
        }
    } );

    table.on( 'draw select deselect', function() {
        let enable_new = allow_new;
        let enable_edit = allow_edit;

        let selectedRows = table.rows( { search: 'applied', selected: true } );

        enable_new = enable_new && (selectedRows.count() == 0);
        enable_edit = enable_edit && (selectedRows.count() > 0);

        enable_new = enable_new && (
            // only enable new when task_id and/or req_id is set
            ((task_id !== null) || (req_id !== null)) ||
            // only enable new when year is set
            (year !== null) ||
            // only enable new when user is set
            (person_id !== null)
        );

        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> new enabled", enable_new);
        if (debug) console.log("(De)Select", selectedRows.count(), "rows -> edit enabled", enable_edit);

        count_selected(table, selectedRows.count(), SELECT);

        table.buttons( ['.buttons-create'] ).enable(enable_new);
        table.buttons( ['.buttons-edit', '.buttons-remove'] ).enable(enable_edit);
    });

    table.paging = !is_test;
    table.scroller = !is_test;

    if (year != null) {
        table.columns( [NAME, LAST_INSTITUTE, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM] ).visible(true);
    }
    if (task_id != null) {
        table.columns( [YEAR, NAME, LAST_INSTITUTE, REQUIREMENT_ID, REQUIREMENT] ).visible(true);
    } else if (req_id != null) {
        table.columns( [YEAR, NAME, LAST_INSTITUTE] ).visible(true);
    }
    if (person_id != null) {
        table.columns( [YEAR, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM] ).visible(true);
    }
    if (last_institute_id != null) {
        table.columns( [YEAR, NAME, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM] ).visible(true);
    }
    if ((year == null) && (system != null)) {
        table.columns( [YEAR, NAME, LAST_INSTITUTE, TASK_ID_REQ_ID, TASK_REQUIREMENT] ).visible(true);
    }

    if (debug) {
        table.columns( [CAN_EDIT, CAN_EDIT_BECAUSE, RO, YEAR, NAME, LAST_INSTITUTE, TASK_ID_REQ_ID, TASK_REQUIREMENT, ACTIVITY_SYSTEM] ).visible( true );
    }
} );
