#!/bin/sh
# set -e

rm *.js
rm *.css
rm *.map
wget -nv -i assets-bs5.lst
mv datatables.css datatables-bs5.css
mv datatables.js datatables-bs5.js
mv datatables.min.css datatables-bs5.min.css
mv datatables.min.js datatables-bs5.min.js
wget -nv -i assets.lst
